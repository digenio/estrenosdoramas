import { LayoutGrid } from '../../layouts/LayoutGrid'
import { LayoutPage } from '../../layouts/LayoutPage'
import { MoviesSlug } from '../../containers/Movies/MoviesSlug'
import { getMoviesPaths, getMoviesData } from '../../services/movies'
import { MovieError } from '../../containers/Movies/MovieError'
import { Head } from '../../components/Head/Head'
import config from '../../utils/config'
import { URL_IMAGE_POSTER } from '../../utils/urls'

export const Peliculas = ({ loading, movie, error, slug }) => {
  if (error) {
    return (
      <LayoutPage>
        <LayoutGrid>
          <MovieError slug={slug} />
        </LayoutGrid>
      </LayoutPage>
    )
  }

  if (loading) {
    return (
      <LayoutPage>
        <LoaderAsync />
      </LayoutPage>
    )
  }
  const { name, name_es, release_date, poster, poster_path } = movie || {}

  return (
    <LayoutPage>
      <LayoutGrid>
        <Head
          title={`Ver pelicula ${name} en estrenos doramas ► ${config.name}`}
          description={`Ver pelicula ${name} ${release_date &&
            release_date.split('-')[0]}  🎦 Pelicula ${name_es ||
            name} sub español online 🎦 Pelicula asiática ${name_es ||
            name} en estrenos doramas`}
          image={poster ? poster : URL_IMAGE_POSTER + poster_path}
        />
        <MoviesSlug movie={movie} />
      </LayoutGrid>
    </LayoutPage>
  )
}

export async function getStaticPaths () {
  const paths = await getMoviesPaths()
  return { paths, fallback: true }
}

export async function getStaticProps ({ params }) {
  const props = await getMoviesData(params.slug)
  return { props }
}

export default Peliculas
