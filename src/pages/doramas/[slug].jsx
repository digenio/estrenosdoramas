import config from '../../utils/config'

import { Head } from '../../components/Head/Head'
import { LayoutPage } from '../../layouts/LayoutPage'
import { LayoutGrid } from '../../layouts/LayoutGrid'
import { DoramasSlug } from '../../containers/Doramas/DoramasSlug'
import { LoaderAsync } from '../../components/Base/LoaderAsync'
import { DoramaError } from '../../containers/Doramas/DoramasError'
import { URL_IMAGE_POSTER } from '../../utils/urls'
import { getDoramaData, getDoramasPaths } from '../../services/doramas'

const Dorama = ({ loading, dorama, error, slug, seasons }) => {
  if (error) {
    return (
      <LayoutPage>
        <LayoutGrid>
          <DoramaError slug={slug} />
        </LayoutGrid>
      </LayoutPage>
    )
  }

  if (loading) {
    return (
      <LayoutPage>
        <LoaderAsync />
      </LayoutPage>
    )
  }

  const { name, name_es, first_air_date, poster, poster_path } = dorama || {}

  return (
    <LayoutPage>
      <LayoutGrid>
        {dorama && (
          <Head
            title={`Ver ${name} sub español en estreno ► ${config.name}`}
            description={`Ver dorama ${name} ${first_air_date &&
              first_air_date.split('-')[0]} 🎦 Dorama ${name_es ||
              name} sub español online 🎦 Capitulos completos del dorama ${name} en estrenos doramas`}
            image={poster ? poster : URL_IMAGE_POSTER + poster_path}
          />
        )}
        <DoramasSlug {...dorama} seasons={seasons} />
      </LayoutGrid>
    </LayoutPage>
  )
}
export async function getStaticPaths () {
  const paths = await getDoramasPaths()
  return { paths, fallback: true }
}

export async function getStaticProps ({ params }) {
  const props = await getDoramaData(params.slug)
  return { props }
}

export default Dorama
