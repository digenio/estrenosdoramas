import { useRouter } from 'next/router'
import { useContext, useEffect } from 'react'

import { UserContext } from '../../contexts/UserContext'
import { LayoutPage } from '../../layouts/LayoutPage'
import { LoaderAsync } from '../../components/Base/LoaderAsync'
import { UserSlug } from '../../containers/usuario/UserSlug'
import { Head } from '../../components/Head/Head'

export const User = () => {
  const { loading, error, loggedUser, logout } = useContext(UserContext)

  const router = useRouter()

  // useEffect(() => {
  //   if (!loggedUser) {
  //     router.push('/')
  //   }
  // }, [])

  // if (!loggedUser) {
  //   return null
  // }
  if (loading) {
    return (
      <LayoutPage>
        <LoaderAsync />
      </LayoutPage>
    )
  }
  return (
    <LayoutPage>
      <Head noFolow />
      <UserSlug loggedUser={loggedUser} />
    </LayoutPage>
  )
}

export async function getStaticPaths() {
  return { paths: [], fallback: true }
}

export async function getStaticProps({ params }) {
  return { props: params }
}

export default User
