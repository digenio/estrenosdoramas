import config from '../../utils/config'
import { Head } from '../../components/Head/Head'
import { AuthForm } from '../../components/Auth/AuthForm'
import { LayoutPage } from '../../layouts/LayoutPage'

const Auth = () => {
  return (
    <LayoutPage>
      <Head title={`Autentificación 【 ▷ 】 ${config.name}`} noFolow />
      <AuthForm />
    </LayoutPage>
  )
}

export default Auth
