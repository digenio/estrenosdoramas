import { Head } from '../../components/Head/Head'
import { LayoutPage } from '../../layouts/LayoutPage'
import { Categories } from '../../ui/Base/Categories/Categories'
import { LayoutGrid } from '../../layouts/LayoutGrid'
import { AsideRating } from '../../containers/Home/AsideRating'
import { DoramasSeries } from '../../containers/Doramas/DoramaSeries'

const Doramas = () => {
  return (
    <LayoutPage>
      <LayoutGrid slug>
        <Head
          title={`ESTRENOS DORAMAS - Ver Doramas Online Gratis HD`}
          description={`Los mejores estrenos doramas asiáticos: k-dramas, j-dramas, c-dramas y t-dramas completas y gratis. Ver ⭐️ estrenosdoramas ⭐️ gratis online Full HD.`}
        />
        <DoramasSeries />
        <Categories slug />
        <AsideRating serie slug />
      </LayoutGrid>
    </LayoutPage>
  )
}

export default Doramas
