import config from '../../utils/config'

import { LayoutPage } from '../../layouts/LayoutPage'
// import { SeasonError } from '../../containers/Temporadas/SeasonError'
import { TemporadaSlug } from '../../containers/Seasons/TemporadaSlug'
import { getSeasonData, getSeasonPaths } from '../../services/seasons'

import { LoaderAsync } from '../../components/Base/LoaderAsync'
import { LayoutGrid } from '../../layouts/LayoutGrid'
import { SeasonError } from '../../containers/Seasons/SeasonError'
import { Head } from '../../components/Head/Head'
import { URL_IMAGE_POSTER } from '../../utils/urls'

const Temporadas = ({ season, loading, error, slug }) => {
  if (error) {
    return (
      <LayoutPage>
        <SeasonError slug={slug} />
      </LayoutPage>
    )
  }

  if (loading || !season) {
    return (
      <LayoutPage>
        <LoaderAsync />
      </LayoutPage>
    )
  }

  const { serie_name, serie_poster, season_number } = season || {}

  return (
    <LayoutPage>
      <LayoutGrid slugIverse>
        <Head
          title={`Ver ${serie_name} temporada ${season_number} online  ► ${config.name}`}
          description={`Ver ${serie_name} temporada ${season_number} online 📺 dorama ${serie_name} temporada ${season_number} sub español, latino en estrenos doramas.`}
          image={URL_IMAGE_POSTER + serie_poster}
        />

        <TemporadaSlug season={season} />
      </LayoutGrid>
    </LayoutPage>
  )
}

export async function getStaticPaths () {
  const paths = await getSeasonPaths()
  return { paths, fallback: true }
}

export async function getStaticProps ({ params }) {
  const props = await getSeasonData(params.slug)
  return { props }
}

export default Temporadas
