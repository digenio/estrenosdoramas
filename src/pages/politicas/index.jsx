import config from '../../utils/config'
import { Head } from '../../components/Head/Head'
import { LayoutPage } from '../../layouts/LayoutPage'
import { Cookiespolicy } from '../../components/Cookiespolicy/Cookiespolicy'

const Politicas = () => {
  return (
    <LayoutPage>
      <Head title={`Politicas de privacidad 【 ▷ 】 ${config.name}`} noFolow />
      <Cookiespolicy />
    </LayoutPage>
  )
}

export default Politicas
