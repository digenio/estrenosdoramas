import { generateSiteMap } from '../../utils/site-map'

export default async (req, res) => {
  await generateSiteMap()

  res.end(`Generate Site map`)
}
