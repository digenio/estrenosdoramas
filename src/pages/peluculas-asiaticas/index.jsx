import { LayoutPage } from '../../layouts/LayoutPage'
import { Categories } from '../../ui/Base/Categories/Categories'
import { LayoutGrid } from '../../layouts/LayoutGrid'
import { AsideRating } from '../../containers/Home/AsideRating'
import { Movies } from '../../containers/Movies/Movies'
import { Head } from '../../components/Head/Head'
import config from '../../utils/config'

const Movie = () => {
  return (
    <LayoutPage>
      <LayoutGrid slug>
        <Head
          title={`Ver Películas Asiaticas Online Gratis HD`}
          description={`Ver peliculas asiaticas completas y gratis full HD 720p y 1080p en Sub Español.`}
        />
        <Movies />
        <Categories slug />
        <AsideRating movie slug />
      </LayoutGrid>
    </LayoutPage>
  )
}

export default Movie
