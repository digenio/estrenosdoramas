import { LayoutPage } from '../../layouts/LayoutPage'
import { Categories } from '../../ui/Base/Categories/Categories'
import { LayoutGrid } from '../../layouts/LayoutGrid'
import { AsideRating } from '../../containers/Home/AsideRating'
import { VariedadesList } from '../../containers/Varieties/VariedadesList'
import { Head } from '../../components/Head/Head'
import config from '../../utils/config'

const Doramas = () => {
  return (
    <LayoutPage>
      <LayoutGrid slug>
        <Head
          title={`Ver show de variedades online gratis 📺 ${config.name}`}
          description={`En ♥ ${config.name} ♥ encontraras kshows subtitulados al español, latino 📌 online, gratis y en HD.`}
        />
        <VariedadesList />
        <Categories slug />
        <AsideRating serie slug />
      </LayoutGrid>
    </LayoutPage>
  )
}

export default Doramas
