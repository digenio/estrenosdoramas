import config from '../../utils/config'

import { LayoutPage } from '../../layouts/LayoutPage'
import { LayoutGrid } from '../../layouts/LayoutGrid'
import { VariedadesSlug } from '../../containers/Varieties/VariedadesSlug'
import { getDoramaData, getDoramasPaths } from '../../services/doramas'
import { LoaderAsync } from '../../components/Base/LoaderAsync'
import { DoramaError } from '../../containers/Doramas/DoramasError'
import { Head } from '../../components/Head/Head'
import { URL_IMAGE_POSTER } from '../../utils/urls'

const Variedad = ({ loading, dorama, error, slug, seasons }) => {
  if (error) {
    return (
      <LayoutPage>
        <DoramaError slug={slug} />
      </LayoutPage>
    )
  }

  if (loading) {
    return (
      <LayoutPage>
        <LoaderAsync />
      </LayoutPage>
    )
  }
  const { name, name_es, first_air_date, poster, poster_path } = dorama || {}

  return (
    <LayoutPage>
      <LayoutGrid>
        <Head
          title={`Ver ${name} show de variedades online sub español ► ${config.name}`}
          description={`Ver show de variedades ${name} ${first_air_date &&
            first_air_date.split('-')[0]}  🎦 show de variedades ${name_es ||
            name} sub español online 🎦 capitulos completos del Kshow ${name_es ||
            name}`}
          image={poster ? poster : URL_IMAGE_POSTER + poster_path}
        />
        <VariedadesSlug {...dorama} seasons={seasons} />
      </LayoutGrid>
    </LayoutPage>
  )
}

export async function getStaticPaths () {
  const paths = await getDoramasPaths()
  return { paths, fallback: true }
}

export async function getStaticProps ({ params }) {
  const props = await getDoramaData(params.slug)
  return { props }
}

export default Variedad
