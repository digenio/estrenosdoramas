import Router from 'next/router'
import { ThemeProvider } from 'styled-components'
import { ApolloProvider } from '@apollo/client'

import * as theme from '../styles/theme'
import { pageview } from '../lib/gtag'
import { useApollo } from '../lib/apolloClient'
import { GlobalStyle } from '../styles/Global'
import { ContextsProviders } from '../contexts/Provider'

Router.events.on('routeChangeComplete', url => pageview(url))

function MyApp ({ Component, pageProps }) {
  const apolloClient = useApollo(pageProps.initialApolloState)

  return (
    <>
      <ApolloProvider client={apolloClient}>
        <ContextsProviders>
          <ThemeProvider theme={theme}>
            <GlobalStyle />
            <Component {...pageProps} />
          </ThemeProvider>
        </ContextsProviders>
      </ApolloProvider>
    </>
  )
}

export default MyApp
