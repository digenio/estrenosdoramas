import { EpisodesList } from '../../containers/Chapters/ChapterList'
import { AsideRating } from '../../containers/Home/AsideRating'
import { LayoutGrid } from '../../layouts/LayoutGrid'
import { LayoutPage } from '../../layouts/LayoutPage'
import { Categories } from '../../ui/Base/Categories/Categories'

export const Capitulos = () => {
  return (
    <LayoutPage>
      <LayoutGrid slugIverse>
        <EpisodesList />
        <Categories slug slugIverse />
        <AsideRating serie slugIverse />
      </LayoutGrid>
    </LayoutPage>
  )
}
export default Capitulos
