import config from '../../utils/config'

import { Head } from '../../components/Head/Head'
import { URL_IMAGE } from '../../utils/urls'
import { LayoutGrid } from '../../layouts/LayoutGrid'
import { LayoutPage } from '../../layouts/LayoutPage'
import { ChapterSlug } from '../../containers/Chapters/ChapterSlug'
import { LoaderAsync } from '../../components/Base/LoaderAsync'
import { ChapterError } from '../../containers/Chapters/ChapterError'
import { getCapituloData, getCapitulosPaths } from '../../services/capitulos'

const Capitulos = ({ extra, loading, episode, error, slug }) => {
  if (error) {
    return (
      <LayoutPage>
        <LayoutGrid>
          <ChapterError slug={slug} />
        </LayoutGrid>
      </LayoutPage>
    )
  }
  if (loading) {
    return (
      <LayoutPage>
        <LoaderAsync />
      </LayoutPage>
    )
  }

  const {
    serie_name,
    season_number,
    episode_number,
    still_path,
    poster,
    backdrop,
    serie_poster,
    season_poster
  } = episode || {}

  return (
    <LayoutPage>
      <LayoutGrid>
        {episode && (
          <Head
            title={`${serie_name} ${
              season_number > 1 ? `temporada ${season_number} ` : ''
            }capitulo ${episode_number} subtitulado ► ${config.name}`}
            description={`Ver ${serie_name}${
              season_number > 1 ? ` temporada ${season_number}` : ''
            } capitulo ${episode_number} online ❇ dorama ${serie_name} ${season_number}X${episode_number} sub español ❇ ${serie_name} cap ${episode_number} gratis en estrenos doramas.`}
            image={
              poster ||
              backdrop ||
              URL_IMAGE + (serie_poster || season_poster || still_path)
            }
          />
        )}
        <ChapterSlug chapter={episode} extra={extra} />
      </LayoutGrid>
    </LayoutPage>
  )
}
export async function getStaticPaths () {
  const paths = await getCapitulosPaths()
  return { paths, fallback: true }
}

export async function getStaticProps ({ params }) {
  const props = await getCapituloData(params.slug)
  return { props }
}
export default Capitulos
