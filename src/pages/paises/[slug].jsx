import config from '../../utils/config'

import { CountriesSlug } from '../../components/Countries/CountriesSlug'
import { LayoutPage } from '../../layouts/LayoutPage'
import { LayoutGrid } from '../../layouts/LayoutGrid'
import { Head } from '../../components/Head/Head'

const Countries = ({ slug }) => {
  return (
    <LayoutPage>
      <LayoutGrid slugIverse>
        <Head
          title={`Ver doramas de ${slug} online sub español gratis en estrenos doramas ▷ ${config.name}`}
          description={`Ver doramas y películas de ${slug} online ➤ Dramas de ${slug} sub español o latino en HD gratis ➤ Ver dramas de ${slug} en estrenos doramas.`}
        />
        <CountriesSlug slug={slug} />
      </LayoutGrid>
    </LayoutPage>
  )
}

export async function getStaticPaths () {
  return { paths: [], fallback: true }
}

export async function getStaticProps ({ params }) {
  // const props = await getCountrielData(params.slug)
  return { props: params }
}

export default Countries
