import { Head } from '../components/Head/Head'
import { Alphabet } from '../ui/Base/Alphabet'
import { LayoutPage } from '../layouts/LayoutPage'
import { LayoutGrid } from '../layouts/LayoutGrid'
import { Categories } from '../ui/Base/Categories/Categories'
import { AsideRating } from '../containers/Home/AsideRating'
import { HomeCategories } from '../components/Home/HomeCategories'
import { HomeCarouselPostels } from '../containers/Home/HomeCarouselPostels'
import { HomeCarouselDetails } from '../containers/Home/HomeCarouselDetails'

const Home = () => {
  return (
    <LayoutPage>
      <Alphabet />
      <Head />
      <LayoutGrid>
        <HomeCarouselPostels />
        <HomeCarouselDetails />
        <HomeCategories />
        <Categories />
        <AsideRating />
      </LayoutGrid>
      <Alphabet />
    </LayoutPage>
  )
}

export default Home
