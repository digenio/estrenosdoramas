import { LayoutGrid } from '../../layouts/LayoutGrid'
import { LayoutPage } from '../../layouts/LayoutPage'
import { LetterSlug } from '../../containers/Letters/LetterSlug'
import { letters } from '../../utils/constans'
import { Alphabet } from '../../ui/Base/Alphabet'
import { Head } from '../../components/Head/Head'
import config from '../../utils/config'

export const Letras = props => {
  return (
    <LayoutPage>
      <Alphabet />
      <LayoutGrid slugIverse>
        <Head
          title={`Ver doramas de inicial ${props.slug} online sub español gratis en estrenos doramas ▷ ${config.name}`}
          description={`Ver doramas y películas de inicial ${props.slug} online ➤ Dramas de la letra ${props.slug} sub español o latino en HD gratis ➤ Ver dramas del género ${props.slug} en estrenos doramas.`}
        />

        <LetterSlug {...props} />
      </LayoutGrid>
      <Alphabet />
    </LayoutPage>
  )
}

export const getStaticProps = async ({ params: { slug } }) => {
  return {
    props: { letter: slug }
  }
}

export async function getStaticPaths () {
  return {
    paths: letters.map(letter => ({ params: { slug: letter } })),
    fallback: false // See the "fallback" section below
  }
}

export default Letras
