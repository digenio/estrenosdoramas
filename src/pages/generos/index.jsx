import config from '../../utils/config'

import { LayoutPage } from '../../layouts/LayoutPage'
import { GenerosList } from '../../components/Generos/GenerosList'
import { Head } from '../../components/Head/Head'

const Generos = () => {
  return (
    <LayoutPage>
      <Head
        title={`Genéros de estrenos doramas 📺 ${config.name}`}
        description={`En ♥ ${config.name} ♥ encontraras doramas de diferentes genéros 📌 subtitulados al español, latino, online, gratis y en HD.`}
      />
      <GenerosList />
    </LayoutPage>
  )
}

export default Generos
