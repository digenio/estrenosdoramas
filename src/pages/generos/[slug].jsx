import config from '../../utils/config'

import { GeneroSlug } from '../../containers/Generos/GeneroSlug'
import { LayoutPage } from '../../layouts/LayoutPage'
import { LayoutGrid } from '../../layouts/LayoutGrid'
import { getKindlData, getKindPaths } from '../../services/Kind'
import { LoaderAsync } from '../../components/Base/LoaderAsync'
import { KindlError } from '../../containers/Generos/KindError'
import { Head } from '../../components/Head/Head'

const Generos = ({ loading, kind, error, slug }) => {
  if (error) {
    return (
      <LayoutPage>
        <LayoutGrid slugIverse>
          <KindlError slug={slug} />
        </LayoutGrid>
      </LayoutPage>
    )
  }

  if (loading) {
    return <LoaderAsync />
  }

  return (
    <LayoutPage>
      <LayoutGrid slugIverse>
        <Head
          title={`Ver doramas de ${kind?.name} online sub español gratis en estrenos doramas ▷ ${config.name}`}
          description={`Ver doramas y películas de  ${kind?.name} online ➤ Dramas del género  ${kind?.name} sub español o latino en HD gratis ➤ Ver dramas del género ${kind?.name} en estrenos doramas.`}
        />
        <GeneroSlug kind={kind} />
      </LayoutGrid>
    </LayoutPage>
  )
}

export async function getStaticPaths() {
  const paths = await getKindPaths()

  return { paths, fallback: paths.length === 0 }
}

export async function getStaticProps({ params }) {
  const props = await getKindlData(params.slug)
  return { props }
}

export default Generos
