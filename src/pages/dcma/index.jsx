import config from '../../utils/config'
import { Head } from '../../components/Head/Head'
import { LayoutPage } from '../../layouts/LayoutPage'
import { LegalWarning } from '../../components/Dcma/LegalWarning'

const Dcma = () => {
  return (
    <LayoutPage>
      <Head title={`DCMA 【 ▷ 】 ${config.name}`} noFolow />
      <LegalWarning />
    </LayoutPage>
  )
}

export default Dcma
