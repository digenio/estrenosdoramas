import config from '../../utils/config'

import { LayoutPage } from '../../layouts/LayoutPage'
import { ProducersList } from '../../components/Producers/ProducersList'
import { Head } from '../../components/Head/Head'

const Producers = () => {
  return (
    <LayoutPage>
      <Head
        title={`Productoras de estrenos doramas 📺 ${config.name}`}
        description={`En ♥ ${config.name} ♥ encontraras doramas de diferentes productoras 📌 subtitulados al español, latino, online, gratis y en HD.`}
      />
      <ProducersList />
    </LayoutPage>
  )
}

export default Producers
