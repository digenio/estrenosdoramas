import config from '../../utils/config'

import { ProcucerSlug } from '../../components/Producers/ProcucerSlug'
import { LayoutPage } from '../../layouts/LayoutPage'
import { LayoutGrid } from '../../layouts/LayoutGrid'
import {
  getProductorasData,
  getProductorasPaths
} from '../../services/producer'
import { KindlError } from '../../containers/Generos/KindError'
import { LoaderAsync } from '../../components/Base/LoaderAsync'
import { Head } from '../../components/Head/Head'

const Producer = ({ loading, network, error, slug }) => {
  if (error) {
    return (
      <LayoutPage>
        <KindlError slug={slug} />
      </LayoutPage>
    )
  }

  if (loading || !network) {
    return <LoaderAsync />
  }

  return (
    <LayoutPage>
      <LayoutGrid slugIverse>
        <Head
          title={`Ver doramas de ${network.name} online sub español gratis en estrenos doramas ▷ ${config.name}`}
          description={`Ver doramas y películas de  ${network.name} online ➤ Dramas de la productora  ${network.name} sub español o latino en HD gratis ➤ Ver dramas de la productora ${network.name} en estrenos doramas.`}
        />
        <ProcucerSlug network={network} />
      </LayoutGrid>
    </LayoutPage>
  )
}

export async function getStaticPaths () {
  const paths = await getProductorasPaths()

  return { paths, fallback: paths.length === 0 }
}

export async function getStaticProps ({ params }) {
  const props = await getProductorasData(params.slug)
  return { props }
}

export default Producer
