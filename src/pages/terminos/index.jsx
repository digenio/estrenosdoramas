import config from '../../utils/config'
import { Head } from '../../components/Head/Head'
import { LayoutPage } from '../../layouts/LayoutPage'
import { TermsAndConditions } from '../../components/Terms/TermsAndConditions'

const QuestionsAndAnswers = () => {
  return (
    <LayoutPage>
      <Head title={`Terminos y condiciones 【 ▷ 】 ${config.name}`} noFolow />
      <TermsAndConditions />
    </LayoutPage>
  )
}

export default QuestionsAndAnswers
