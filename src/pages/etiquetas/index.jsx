import config from '../../utils/config'

import { Head } from '../../components/Head/Head'
import { LabelList } from '../../containers/Etiquetas/LabelList'
import { LayoutPage } from '../../layouts/LayoutPage'

const Etiquetas = () => {
  return (
    <LayoutPage>
      <Head
        title={`Ver categorias de estrenos doramas 📺 ${config.name}`}
        description={`En ♥ ${config.name} ♥ encontraras doramas en estreno de diferentes categorias subtitulados al español, latino 📌 online, gratis y en HD.`}
      />
      <LabelList />
    </LayoutPage>
  )
}

export default Etiquetas
