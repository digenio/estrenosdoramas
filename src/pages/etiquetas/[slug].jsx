import config from '../../utils/config'

import { Head } from '../../components/Head/Head'
import { LabelSlug } from '../../containers/Etiquetas/LabelSlug'
import { LayoutPage } from '../../layouts/LayoutPage'
import { LayoutGrid } from '../../layouts/LayoutGrid'
import { getLabelData, getLabelsPaths } from '../../services/labels'
import { LabelError } from '../../containers/Etiquetas/LabelError'

const Etiquetas = ({ loading, label, error, slug }) => {
  if (error) {
    return (
      <LayoutPage>
        <LayoutGrid slugIverse>
          <LabelError slug={slug} />
        </LayoutGrid>
      </LayoutPage>
    )
  }

  if (loading) {
    return <LoaderAsync />
  }

  return (
    <LayoutPage>
      <LayoutGrid slugIverse>
        <Head
          title={`Ver doramas de ${label?.name} online sub español gratis en estrenos doramas ▷ ${config.name}`}
          description={`Ver doramas y películas de  ${label?.name} online ➤ Dramas del género  ${label?.name} sub español o latino en HD gratis ➤ Ver dramas del género ${label?.name} en estrenos doramas.`}
        />
        <LabelSlug label={label} />
      </LayoutGrid>
    </LayoutPage>
  )
}

export async function getStaticPaths() {
  const paths = await getLabelsPaths()
  return { paths, fallback: paths.length === 0 }
}

export async function getStaticProps({ params }) {
  const props = await getLabelData(params.slug)
  return { props }
}

export default Etiquetas
