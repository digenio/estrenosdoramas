import config from '../../utils/config'
import { Faq } from '../../components/Faq/Faq'
import { Head } from '../../components/Head/Head'
import { LayoutPage } from '../../layouts/LayoutPage'

const QuestionsAndAnswers = () => {
  return (
    <LayoutPage>
      <Head title={`Preguntas frecuentes 【 ▷ 】 ${config.name}`} noFolow />
      <Faq />
    </LayoutPage>
  )
}

export default QuestionsAndAnswers
