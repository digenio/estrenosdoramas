import { useRouter } from 'next/router'
import { useContext, useEffect } from 'react'

import config from '../../utils/config'
import { Head } from '../../components/Head/Head'
import { LayoutPage } from '../../layouts/LayoutPage'
import { FormAccount } from '../../components/Account/FormAccount'
import { UserContext } from '../../contexts/UserContext'

export const Cuenta = () => {
  const { loggedUser, logout } = useContext(UserContext)
  const router = useRouter()

  useEffect(() => {
    if (!loggedUser) {
      router.push('/')
    }
  }, [])

  if (!loggedUser) {
    return null
  }
  return (
    <LayoutPage>
      <Head title={`Perfil 【 ▷ 】 ${config.name}`} noFolow />
      <FormAccount />
    </LayoutPage>
  )
}

export default Cuenta
