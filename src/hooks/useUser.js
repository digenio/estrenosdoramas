import { useEffect } from 'react'
import { usePersistStorage } from '../hooks/usePersistStorage'
import { useMutation, useLazyQuery } from '@apollo/client'

import { UPDATE_USER, GET_LOGGED_USER } from '../gql/user'

export function useUser ({ jwt } = {}) {
  const [user, changeUser, restored] = usePersistStorage('user', null)

  const [updateUser, { loading: ldUpdate, error: errUpdate }] = useMutation(
    UPDATE_USER,
    {
      update (
        cache,
        {
          data: {
            updateUser: { record }
          }
        }
      ) {
        changeUser(record)
      }
    }
  )

  const [
    fetchUser,
    { loading, error, data: { loggedUser } = {} }
  ] = useLazyQuery(GET_LOGGED_USER, {
    fetchPolicy: 'network-only'
  })

  useEffect(() => {
    if (loggedUser) {
      changeUser(loggedUser)
    }
  }, [loggedUser])

  useEffect(() => {
    if (restored && jwt) {
      fetchUser()
    } else if (!jwt) {
      changeUser()
    }
  }, [restored, jwt])

  const refetch = () => {
    fetchUser()
  }

  return {
    loading,
    error,
    refetch,
    loggedUser: user,
    updateUser,
    changeUser,
    ldUpdate,
    errUpdate,
    ldUser: !restored || loading
  }
}
