import { useQuery } from '@apollo/client'
import { useContext, useState, useEffect } from 'react'

import { ServerContext } from '../contexts/ServerContext'
import { LIST_PROBLEM } from '../gql/report'

export function useOptions ({
  _id,
  type,
  links_online,
  changeLang,
  changeServer,
  lang
}) {
  const [options, changeOptions] = useState([])
  const [langs, changeLanguages] = useState([])
  const [loading, changeLoading] = useState(false)

  const { servers = [], loading: ld2 } = useContext(ServerContext)

  const { data: { listProblems = [] } = {}, loading: ldReport } = useQuery(
    LIST_PROBLEM,
    {
      variables: { problem_type: type, problem_id: _id }
    }
  )

  const init = () => {
    if (ldReport) {
      return
    }
    if (servers.length > 0) {
      let opts = []
      let lngs = []

      let sortProblems = []

      listProblems.forEach(it => {
        if (it.server) {
          const index = sortProblems.findIndex(p => it.server.link === p.link)
          if (index === -1) {
            sortProblems.push({ link: it.server.link, reports: 1 })
          } else {
            sortProblems[index].reports += 1
          }
        }
      })

      sortProblems = sortProblems.sort((a, b) => a.reports - b.reports)

      servers.forEach(server => {
        opts = [
          ...opts,
          ...links_online?.filter(l => l.server == server.code_flix)
        ]
      })

      sortProblems.forEach(p => {
        if (p.reports > 10) {
          const index = opts.findIndex(op => op.link === p.link)
          if (index !== -1) {
            opts.push(opts.splice(index, 1)[0])
          }
        }
      })

      opts.forEach(op => {
        const index = lngs.findIndex(it => it === op.lang)
        if (index === -1) {
          lngs.push(op.lang)
        }
      })

      changeOptions(opts)
      changeLanguages(lngs)

      // const lng = defaultAudio
      //   ? lngs.find(lg =>
      //       defaultAudio === 'Latino' ? lg == '38' : lg != '38'
      //     ) || lngs[0]
      //   : lngs[0]

      changeLang(lngs[0])
      changeLoading(false)
    }
  }

  useEffect(() => {
    init()
  }, [links_online?.length, servers.length, listProblems.length, _id, ldReport])

  useEffect(() => {
    if (lang) {
      //const server_fav = loggedUser?.preferences?.fav_server_page
      let opt = options.find(s => s.lang == lang)
      if (opt) {
        changeServer(opt)
      } else {
        opt = options.find(s => s.lang == lang)
        changeServer(opt)
      }
    }
  }, [lang])

  return { options, langs, loading: loading || ld2 || ldReport }
}
