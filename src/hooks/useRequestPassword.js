import { useMutation } from '@apollo/client'

import { REQUEST_PASS } from '../gql/auth'
import config from '../utils/config'

export const useRequestPassword = ({ onSuccess }) => {
  const [requestPass, { loading, error }] = useMutation(REQUEST_PASS)

  const handleRequest = async data => {
    requestPass({
      variables: {
        ...data,
        platform: config.platform
      }
    }).then(({ errors }) => {
      if (errors) {
        alert('Se presento un error al requerir una nueva contraseña.')
      } else {
        alert('Se envió un email con una contraseña temporal.')
        onSuccess && onSuccess()
      }
    })
  }

  return {
    handleRequest,
    ldRequest: loading,
    errRequest: error
  }
}
