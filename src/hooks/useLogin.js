import { useMutation } from '@apollo/client'
import { useContext } from 'react'

import { LOGIN } from '../gql/auth'
import { UserContext } from '../contexts/UserContext'

export const useLogin = ({ onSuccess } = {}) => {
  const { setAuth } = useContext(UserContext)

  const [login, { loading, error }] = useMutation(LOGIN)

  const handleLogin = async variables => {
    login({ variables }).then(({ data = {}, errors }) => {
      if (errors) {
        alert('Se presento un error al iniciar sesión.')
      } else {
        const {
          login: { token, ...props }
        } = data
        setAuth(token)
        onSuccess && onSuccess()
      }
    })
  }

  return {
    handleLogin,
    ldLogin: loading,
    errLogin: error
  }
}
