import { initializeApollo } from '../lib/apolloClient'

import { SEASON_DETAIL } from '../gql/season'
import { LIST_DORAMAS_SLUG } from '../gql/dorama'

export const getSeasonPaths = async () => {
  try {
    return []
    const apolloClient = initializeApollo()
    const data = await apolloClient.query({
      query: LIST_DORAMAS_SLUG
    })

    const paths = data.data.listDoramas.map(dorama => ({
      params: { slug: dorama.slug }
    }))

    return paths
  } catch (error) {
    console.error('Error al traer los doramas', error)
    return []
  }
}

export const getSeasonData = async slug => {
  try {
    const apolloClient = initializeApollo()
    const data = await apolloClient.query({
      query: SEASON_DETAIL,
      variables: { slug }
    })

    const season = data.data.detailSeason

    return {
      slug,
      season,
      loading: !data?.data?.detailSeason
    }
  } catch (error) {
    console.error('Error get dorama --->', slug)
    return { error: true, slug, dorama: null }
  }
}
