import { LIST_COUNTRYS } from '../gql/country'
import { initializeApollo } from '../lib/apolloClient'
import { useLazyQuery } from '@apollo/client'

export const getCountriePaths = async () => {
  // if (process.env.NODE_ENV !== 'production') {
  //   return []
  // }

  try {
    const apolloClient = initializeApollo()
    const data = await apolloClient.query({
      query: LIST_COUNTRYS
    })

    const paths = data.data.listCountries
      .filter(it => it.slug)
      .map(contries => ({
        params: { slug: contries.slug }
      }))

    return paths
  } catch (error) {
    console.error('Error al traer los paises')
    return []
  }
}

export const getCountrielData = async slug => {
  try {
    const apolloClient = initializeApollo()
    const data = await apolloClient.query({
      query: LIST_COUNTRYS,
      variables: { slug }
    })
    const countries = data?.data?.listCountries

    return {
      slug,
      countries,
      loading: !data?.data?.listCountries
    }
  } catch (error) {
    console.error('Error al tarer la etiqueta', slug)
    return {
      props: { slug, error: true }
    }
  }
}

export const getCountrielDataLazy = async slug => {
  try {
    const [
      getDetailCountry,
      { data: { listCountries } = {}, loading }
    ] = useLazyQuery(LIST_COUNTRYS, { variables: { slug } })

    return {
      listCountries,
      getDetailCountry,
      loading: !listCountries
    }
  } catch (error) {
    console.error('Error get dorama --->', slug)
    return { error: true, slug, dorama: null }
  }
}
