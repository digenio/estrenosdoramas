import { DETAIL_GENRE, LIST_GENRE_SLUG } from '../gql/genre'
import { initializeApollo } from '../lib/apolloClient'
import config from '../utils/config'

export const getKindPaths = async () => {
  try {
    const apolloClient = initializeApollo()
    const data = await apolloClient.query({
      query: LIST_GENRE_SLUG,
      variables: { platform: config.app }
    })

    const paths = data.data.listGenres
      .filter(it => it.slug)
      .map(generos => ({
        params: { slug: generos.slug }
      }))

    return paths
  } catch (error) {
    console.error('Error al traer los generos')
    return []
  }
}

export const getKindlData = async slug => {
  try {
    const apolloClient = initializeApollo()
    const data = await apolloClient.query({
      query: DETAIL_GENRE,
      variables: { slug }
    })
    const kind = data?.data?.detailGenre
    return {
      slug,
      kind,
      loading: !data?.data?.detailGenre
    }
  } catch (error) {
    console.error('Error al tarer la etiqueta', slug, error)
    return {
      props: { slug, error: true }
    }
  }
}
