import { initializeApollo } from '../lib/apolloClient'
import { LIST_NETWORKS_SLUG, DETAIL_NETWORKS } from '../gql/network'

export const getProductorasPaths = async () => {
  return []
  // if (process.env.NODE_ENV !== 'production') {
  //   return {
  //     paths: [],
  //     fallback: true
  //   }
  // }
  try {
    const apolloClient = initializeApollo()
    const data = await apolloClient.query({
      query: LIST_NETWORKS_SLUG
    })

    const paths = data.data.listNetworks
      .filter(it => it.slug)
      .map(genre => ({
        params: { slug: genre.slug }
      }))

    return paths
  } catch (error) {
    console.error('Error traer todas la productoras')
    return []
  }
}

export const getProductorasData = async slug => {
  try {
    const apolloClient = initializeApollo()
    const data = await apolloClient.query({
      query: DETAIL_NETWORKS,
      variables: { slug }
    })
    const network = data?.data?.detailNetwork
    return {
      slug,
      network,
      loading: !data?.data?.detailNetwork
    }
  } catch (error) {
    console.error('Error Productora --->', slug)
    return { error: true }
  }
}

export const getProductorasDataLazy = async slug => {
  try {
    const [
      getDetailNetwork,
      { data: { detailNetwork } = {}, loading2 }
    ] = useLazyQuery(DETAIL_NETWORKS, { variables: { slug } })

    return {
      detailNetwork,
      getDetailNetwork,
      loading: !detailNetwork
    }
  } catch (error) {
    console.error('Error get dorama --->', slug)
    return { error: true, slug, dorama: null }
  }
}
