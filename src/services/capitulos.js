import { initializeApollo } from '../lib/apolloClient'
import {
  LIST_EPISODES_SLUG,
  EPISODE_DETAIL,
  EPISODE_DETAIL_EXTRA
} from '../gql/episode'

export const getCapitulosPaths = async () => {
  //return []
  // if (process.env.NODE_ENV !== 'production') {
  return []
  // }

  try {
    const apolloClient = initializeApollo()
    const data = await apolloClient.query({
      query: LIST_EPISODES_SLUG
    })

    const paths = data.data.listEpisodes.map(item => ({
      params: { slug: item.slug }
    }))
    return paths
  } catch (error) {
    console.error('Error al traer los capitulos', error)
    return []
  }
}

export const getCapituloData = async slug => {
  try {
    const apolloClient = initializeApollo()
    const data = await apolloClient.query({
      query: EPISODE_DETAIL,
      variables: { slug }
    })

    const episode = data.data.detailEpisode

    const dataExtra = await apolloClient.query({
      query: EPISODE_DETAIL_EXTRA,
      variables: { episode_id: episode._id, slug: episode.serie_slug }
    })

    const extra = dataExtra.data

    return {
      slug,
      extra,
      episode,
      loading: !episode || !extra
    }
  } catch (error) {
    console.error('Error get dorama --->', slug)
    return { error: true, slug }
  }
}
