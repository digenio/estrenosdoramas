import { initializeApollo } from '../lib/apolloClient'
import { useLazyQuery } from '@apollo/client'
import { LIST_SEASON } from '../gql/season'
import { LIST_DORAMAS_SLUG, DETAIL_DORAMA } from '../gql/dorama'

export const getDoramasPaths = async () => {
  return []
  // if (process.env.NODE_ENV !== 'production') {
  // return {
  //   paths: [],
  //   fallback: true
  //}
  // }

  try {
    const apolloClient = initializeApollo()
    const data = await apolloClient.query({
      query: LIST_DORAMAS_SLUG
    })

    const paths = data.data.listDoramas.map(dorama => ({
      params: { slug: dorama.slug }
    }))

    return paths
  } catch (error) {
    console.error('Error al traer los doramas', error)
    return []
  }
}

export const getDoramaData = async slug => {
  try {
    const apolloClient = initializeApollo()
    const data = await apolloClient.query({
      query: DETAIL_DORAMA,
      variables: { slug }
    })

    const dorama = data.data.detailDorama

    const dataSeason = await apolloClient.query({
      query: LIST_SEASON,
      variables: { serie_id: dorama._id }
    })

    const seasons = dataSeason.data.listSeasons

    return {
      slug,
      dorama,
      seasons,
      loading: !data?.data?.detailDorama || !dataSeason?.data?.listSeasons
    }
  } catch (error) {
    console.error('Error get dorama --->', slug)
    return { error: true, slug, dorama: null }
  }
}
