import { DETAIL_LABEL, LIST_LABELS } from '../gql/label'
import { initializeApollo } from '../lib/apolloClient'

export const getLabelsPaths = async () => {
  // if (process.env.NODE_ENV !== 'production') {
  //   return []
  // }

  try {
    const apolloClient = initializeApollo()
    const data = await apolloClient.query({
      query: LIST_LABELS
    })

    const paths = data.data.listLabels
      .filter(it => it.slug)
      .map(label => ({
        params: { slug: label.slug }
      }))

    return paths
  } catch (error) {
    console.error('error al traer las etiquetas')
    return []
  }
}

export const getLabelData = async slug => {
  try {
    const apolloClient = initializeApollo()
    const data = await apolloClient.query({
      query: DETAIL_LABEL,
      variables: { slug }
    })
    const label = data?.data?.detailLabel
    return {
      slug,
      label,
      loading: !data?.data?.detailLabel
    }
  } catch (error) {
    console.error('Error al tarer la etiqueta', slug)
    return {
      props: { slug, error: true }
    }
  }
}
