import { initializeApollo } from '../lib/apolloClient'
import { LIST_MOVIES_SLUG, DETAIL_MOVIES } from '../gql/movie'

export const getMoviesPaths = async () => {
  // if (process.env.NODE_ENV !== 'production') {
  return []
  // }

  try {
    const apolloClient = initializeApollo()
    const data = await apolloClient.query({
      query: LIST_MOVIES_SLUG
    })

    const paths = data.data.listMovies.map(movie => ({
      params: { slug: movie.slug }
    }))

    return paths
  } catch (error) {
    console.error('Error al traer las peliculas')
    return []
  }
}

export const getMoviesData = async slug => {
  try {
    const apolloClient = initializeApollo()
    const data = await apolloClient.query({
      query: DETAIL_MOVIES,
      variables: { slug }
    })

    const movie = data?.data?.detailMovie

    return {
      movie,
      loading: !data?.data?.detailMovie
    }
  } catch (error) {
    console.error('error al traer la pelicula', slug)
    return {
      props: { error: true, slug, loading: false }
    }
  }
}
