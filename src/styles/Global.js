import { createGlobalStyle } from 'styled-components'
import config from '../utils/config'

export const GlobalStyle = createGlobalStyle`
*{
  padding: 0;
  box-sizing:border-box;
  margin: 0;
}
@font-face {
  font-family: 'Montserrat';
  src: url('/fonts/Montserrat-Light.ttf') format('truetype');
  font-style: normal;
  font-weight: 300;
  font-display: fallback; /* <- this can be added to each @font-face definition */
}
@font-face {
  font-family: 'Montserrat';
  src: url('/fonts/Montserrat-Regular.ttf') format('truetype');
  font-style: normal;
  font-weight: 400;
  font-display: fallback; /* <- this can be added to each @font-face definition */
}

@font-face {
  font-family: 'Montserrat';
  src: url('/fonts/Montserrat-Bold.ttf') format('truetype');
  font-style: normal;
  font-weight: 700;
  font-display: fallback; /* <- this can be added to each @font-face definition */
}
@font-face {
  font-family: 'Montserrat';
  src: url('/fonts/Montserrat-Black.ttf') format('truetype');
  font-style: normal;
  font-weight: 900;
  font-display: fallback; /* <- this can be added to each @font-face definition */
}




body{
  /* background-color: #15171C; */
  font-family: 'Montserrat', sans-serif;
  font-weight: 400;

  
}
*::-webkit-scrollbar {
width: 5px;
border-radius: 5px;
  background-color: #262a36;
}

*::-webkit-scrollbar-thumb {
  background-color: blue;
  border-radius: 20px;
  border: 3px solid ${config.color};
}

* {
  scrollbar-width: thin;
  scrollbar-color: ${config.color} #262a36;
}
.carouselItem{
  
  flex-grow: 1 !important;;
  flex-shrink: 0 !important;
  border-radius: 1rem;
  /* margin-left: 16px; */
  /* margin-right: 16px; */

  /* @media  screen and (min-width: 768px) {
   width:   calc(50vw - 16px) !important;
} */

.carousel-button-group{
  position: absolute !important;


}
}

.carouselposter{ flex-grow: 1 !important;;
  flex-shrink: 0 !important;
  border-radius: 1rem;
  width:300px;
  }

.carousel-container{height:100%;
position: relative;
  @media ${({ theme }) => theme.devices.laptop} {
    max-height:400px;
  }
margin-top:10px}

.carousel-container-postel{
  flex-grow: 1 !important;;
  flex-shrink: 0 !important;
  border-radius: 1rem;
}
/* .carousel-postel{} */
.containercarousel ul{
  box-sizing: content-box;
  transition-duration: 0ms;
  margin-left: auto;
    margin-right: auto;
}

.hamburger {
  padding: 5PX;
  display: inline-block;
  cursor: pointer;
  transition-property: opacity, filter;
  transition-duration: 0.15s;
  transition-timing-function: linear;
  font: inherit;
  color: inherit;
  text-transform: none;
  background-color: transparent;
  border: 0;
  margin: 0;
  overflow: visible; }
  .hamburger:hover {
    opacity: 0.7; }
  .hamburger.is-active:hover {
    opacity: 0.7; }
  .hamburger.is-active .hamburger-inner,
  .hamburger.is-active .hamburger-inner::before,
  .hamburger.is-active .hamburger-inner::after {
    background-color: #fff; }

.hamburger-box {
  width: 40px;
  height: 24px;
  display: inline-block;
  position: relative; }

.hamburger-inner {
  display: block;
  top: 50%;
  margin-top: -2px; }
  .hamburger-inner, .hamburger-inner::before, .hamburger-inner::after {
    width: 30px;
    height: 3px;
    background-color: #FFF;
    border-radius: 4px;
    position: absolute;
    transition-property: transform;
    transition-duration: 0.15s;
    transition-timing-function: ease; }
  .hamburger-inner::before, .hamburger-inner::after {
    content: "";
    display: block; }
  .hamburger-inner::before {
    top: -10px; }
  .hamburger-inner::after {
    bottom: -10px; }

      /*
     * Slider
     */
  .hamburger--slider .hamburger-inner {
    top: 2px; }
    .hamburger--slider .hamburger-inner::before {
      top: 10px;
      transition-property: transform, opacity;
      transition-timing-function: ease;
      transition-duration: 0.15s; }
    .hamburger--slider .hamburger-inner::after {
      top: 20px; }
  
  .hamburger--slider.is-active .hamburger-inner {
    transform: translate3d(0, 10px, 0) rotate(45deg); }
    .hamburger--slider.is-active .hamburger-inner::before {
      transform: rotate(-45deg) translate3d(-5.71429px, -6px, 0);
      opacity: 0; }
    .hamburger--slider.is-active .hamburger-inner::after {
      transform: translate3d(0, -20px, 0) rotate(-90deg); }
  
  /*
     * Slider Reverse
     */
  .hamburger--slider-r .hamburger-inner {
    top: 2px; }
    .hamburger--slider-r .hamburger-inner::before {
      top: 10px;
      transition-property: transform, opacity;
      transition-timing-function: ease;
      transition-duration: 0.15s; }
    .hamburger--slider-r .hamburger-inner::after {
      top: 20px; }
  
  .hamburger--slider-r.is-active .hamburger-inner {
    transform: translate3d(0, 10px, 0) rotate(-45deg); }
    .hamburger--slider-r.is-active .hamburger-inner::before {
      transform: rotate(45deg) translate3d(5.71429px, -6px, 0);
      opacity: 0; }
    .hamburger--slider-r.is-active .hamburger-inner::after {
      transform: translate3d(0, -20px, 0) rotate(90deg); }
  

/*Spacing*/
ul,
ol,
dl,
dt,
dd,
h1,
h2,
h3,
h4,
h5,
h6,
p,
pre,
blockquote,
table,
fieldset {
  margin: 0;
}

figure,
ul,
ol {
  margin: 0;
  padding: 0;
  list-style-type: none
}

/*Forms*/
button,

select,
textarea {
  font: inherit;
  width: 100%;
  vertical-align: middle;
  line-height: normal;
  text-transform: none;
  overflow: visible
}

fieldset {
  border: 1px solid transparent;
  padding: 1rem
}

legend {
  color: inherit;
  display: table;
  max-width: 100%;
  white-space: normal;
  border: 0;
  padding: var(--size)
}

label {
  display: inline-block;
  padding: .875rem 0
}

label[for] {
  cursor: pointer
}

[type="search"] {
  -webkit-appearance: textfield;
  outline-offset: -2px
}

[type="search"]::-webkit-search-decoration {
  -webkit-appearance: none
}

::-webkit-file-upload-button {
  -webkit-appearance: button;
  font: inherit
}

::placeholder {
  opacity: .7;
  color: currentColor
}


*,
:before,
:after {
  box-sizing: border-box;
  min-width: 0
}

html {
  -ms-text-size-adjust: 100%;
  -webkit-text-size-adjust: 100%;
  scroll-behavior: smooth;
  overflow-x: hidden;
  font-size: 100%
}



hr {
  border: 0;
  margin: calc(2 * 1rem) 0;
  clear: both;
  overflow: hidden;
  height: 1px;
  background-color: var(--gray)
}

[hidden],
.dn {
  display: none !important
}

img,
svg {
  vertical-align: middle;
  max-width: 100%;
  border: 0;
  pointer-events: none;
}

:focus {
  outline: 0
}

a {
  text-decoration: none;
  color: var(--link)
}

a:hover {
  color: var(--link-hover)
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0
}


    
`
