import gql from 'graphql-tag'
import config from '../utils/config'

export const LIST_LISTS = gql`
  query listList($user_id: MongoID!) {
    listList(filter: { user_id: $user_id, platform: "${config.app}" }) {
      _id
      serie_id
      name
      name_es
      poster_path
      poster
      type
      status
      slug
    }
  }
`

export const CREATE_LIST = gql`
  mutation createList($record: CreateOneListInput!) {
    createList(record: $record) {
      record {
        _id
        name
        serie_id
        name_es
        poster_path
        poster
        type
        status
      }
    }
  }
`

export const UPDATE_LIST = gql`
  mutation updateList($record: UpdateByIdListInput!) {
    updateList(record: $record) {
      record {
        _id
        name
        name_es
        poster_path
        poster
        type
        serie_id
        status
      }
    }
  }
`

export const REMOVE_LIST = gql`
  mutation deleteList($_id: MongoID!) {
    deleteList(_id: $_id) {
      record {
        _id
        serie_id
        name
        name_es
        poster_path
        poster
        type
        status
      }
    }
  }
`
