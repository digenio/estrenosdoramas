import gql from 'graphql-tag'
import config from '../utils/config'

export const GENRE_LIST_IMAGE = gql`
  query listGenres {
    listGenres(filter: { platform: "doramasgo" }, sort: NUMBER_DESC) {
      name
      _id
      slug
      platforms {
        _id
        platform
        number
        image_default
        image_tmdb
        image_custom
      }
    }
  }
`

export const LIST_GENRE_SLUG = gql`
  query listGenresSlug {
    listGenres(sort: UPDATEDAT_ASC, filter: { platform: "doramasgo" }) {
      slug
      updatedAt
    }
  }
`

export const DETAIL_GENRE = gql`
  query detailGenre($slug: String!) {
    detailGenre(filter: { slug: $slug }) {
      _id
      name
      slug
    }
  }
`
