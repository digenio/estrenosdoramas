import gql from 'graphql-tag'

export const SEARCH = gql`
  query searchAll($input: String!) {
    searchDorama(input: $input, limit: 10) {
      _id
      slug
      name
      name_es
      names
    }
    searchMovie(input: $input, limit: 10) {
      _id
      name
      name_es
      names
      slug
    }
  }
`

export const LIST = gql`
  {
    listDoramas(filter: { in_carrousel: true }) {
      _id
      name
      name_es
      slug
      cast
      names
      overview
      created_by
      popularity
      poster_path
      backdrop_path
      first_air_date
      episode_run_time
      isTVShow
      poster
      backdrop
      genres {
        name
        slug
      }
      networks {
        name
        slug
      }
    }
    listMovies(filter: { in_carrousel: true }) {
      _id
      name
      name_es
      slug
      cast
      names
      overview
      popularity
      poster_path
      backdrop_path
      release_date
      runtime
      genres {
        name
        slug
      }
      networks {
        name
        slug
      }
    }
  }
`

export const LIST_POPULARS = gql`
  query listPopulars($limit: Int, $skip: Int) {
    listDoramas(limit: $limit, skip: $skip, sort: POPULARITY_DESC) {
      name
      name_es
      slug
      poster_path
      poster
    }
    listMovies(limit: $limit, skip: $skip, sort: POPULARITY_DESC) {
      name
      name_es
      slug
      poster_path
      poster
    }
  }
`
