import gql from 'graphql-tag'
import config from '../utils/config'

export const LIST_LABELS = gql`
  query listLabels {
    listLabels(filter: { platform: "doramasgo" }, sort: NUMBER_DESC) {
      _id
      name
      slug
      platforms {
        _id
        platform
        number
        image_default
        image_tmdb
        image_custom
      }
    }
  }
`

export const DETAIL_LABEL = gql`
  query detailLabel($slug: String!) {
    detailLabel(filter: { slug: $slug }) {
      _id
      name
      slug
    }
  }
`

export const LABEL_PELICULAS_LIST = gql`
  query listMoviesLabel($labelId: MongoID!) {
    listMovies(filter: { labelId: $labelId }) {
      _id
      name
      name_es
      slug
      overview
      release_date
      runtime
      poster_path
    }
  }
`

export const LABEL_DORAMAS_LIST = gql`
  query listDoramasLabel($labelId: MongoID!) {
    listDoramas(filter: { labelId: $labelId }) {
      _id
      name
      name_es
      isTVShow
      slug
      overview
      first_air_date
      episode_run_time
      poster_path
    }
  }
`

export const LANG_SERIES_LIST = gql`
  query listDoramasLang($filter: FilterFindManyDoramaInput!) {
    listDoramas(filter: $filter) {
      _id
      name
      name_es
      poster_path
    }
  }
`
