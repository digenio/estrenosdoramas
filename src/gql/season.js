import gql from 'graphql-tag'

export const LIST_SEASON = gql`
  query listSeasons($serie_id: MongoID!) {
    listSeasons(sort: NUMBER_ASC, filter: { serie_id: $serie_id }) {
      slug
      season_number
      poster_path
      air_date
      serie_name
      poster
      backdrop
    }
  }
`

export const LIST_SEASON_SLUG = gql`
  query listSeasonsSlug {
    listSeasons(sort: UPDATEDAT_ASC, filter: { type_serie: "dorama" }) {
      slug
      updatedAt
    }
  }
`

export const SEASON_DETAIL = gql`
  query detailSeason($slug: String!) {
    detailSeason(filter: { slug: $slug, type_serie: "dorama" }) {
      _id
      name
      slug
      serie_name
      serie_slug
      serie_id
      serie_poster
      season_number
      poster
      backdrop
    }
  }
`

export const SEASON_DETAIL_EXTRA = gql`
  query detailSeasonExtra($slug: String!, $season_number: Float!) {
    listSeasons(sort: NUMBER_ASC, filter: { serie_slug: $slug }) {
      slug
      season_number
      poster_path
      air_date
      serie_name
      poster
      backdrop
    }
    listEpisodes(
      sort: NUMBER_ASC
      filter: {
        type_serie: "dorama"
        serie_slug: $slug
        season_number: $season_number
      }
    ) {
      _id
      name
      slug
      serie_name
      serie_id
      still_path
      air_date
      season_number
      episode_number
      languages
      poster
      backdrop
    }
  }
`
