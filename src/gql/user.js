import gql from 'graphql-tag'

export const GET_LOGGED_USER = gql`
  query getUser {
    loggedUser {
      _id
      names
      email
      username
      preferences {
        name_lang
        fav_server_app
        default_audio
        fav_server_page
      }
    }
  }
`

export const GET_USER_COLLECTION = gql`
  query detailUser($username: String!) {
    detailUser(filter: { username: $username }) {
      _id
      names
      email
      username
    }
  }
`

export const UPDATE_USER = gql`
  mutation editUser($record: UpdateByIdUserInput!) {
    updateUser(record: $record) {
      record {
        _id
        names
        email
        username
        preferences {
          name_lang
          fav_server_app
          default_audio
          fav_server_page
        }
      }
    }
  }
`

export const TREND_SERIES = gql`
  {
    trendsDoramas(limit: 15) {
      _id
      name
      name_es
      slug
      poster_path
    }
  }
`
