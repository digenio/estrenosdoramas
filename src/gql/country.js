import gql from 'graphql-tag'

export const LIST_COUNTRYS = gql`
  query listCountries($_ids: [MongoID!]!) {
    listCountriesByIds(_ids: $_ids) {
      _id
      name
      slug
      flag
      code
      code_flix
    }
  }
`
