import gql from 'graphql-tag'
import config from '../utils/config'

export const NETWORK_LIST_IMAGES = gql`
  query listNetworks {
    listNetworks(
      filter: { platform: "doramasgo" }
      sort: NUMBER_DESC
      limit: 30
    ) {
      _id
      name
      slug
      platforms {
        _id
        platform
        number
        image_default
        image_tmdb
        image_custom
      }
    }
  }
`

export const LIST_NETWORKS_SLUG = gql`
  query listNetworksSlug {
    listNetworks(sort: UPDATEDAT_ASC, filter: { platform: "doramasgo" }) {
      slug
      updatedAt
    }
  }
`
export const DETAIL_NETWORKS = gql`
  query detailNetwork($slug: String!) {
    detailNetwork(filter: { slug: $slug }) {
      _id
      name
      slug
    }
  }
`
