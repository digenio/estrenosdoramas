import gql from 'graphql-tag'

export const CREATE_PROBLEM = gql`
  mutation reportProblem($record: CreateOneProblemInput!) {
    createProblem(record: $record) {
      record {
        _id
      }
    }
  }
`

export const LIST_PROBLEM = gql`
  query listProblemsItem(
    $problem_type: EnumProblemProblem_type
    $problem_id: MongoID!
  ) {
    listProblems(
      filter: {
        problem_type: $problem_type
        problem_id: $problem_id
        WithServer: true
      }
      sort: _ID_DESC
    ) {
      server
    }
  }
`
