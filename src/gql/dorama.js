import gql from 'graphql-tag'

export const LIST_DORAMAS = gql`
  query listDoramas(
    $limit: Int
    $skip: Int
    $sort: SortFindManyDoramaInput
    $filter: FilterFindManyDoramaInput
  ) {
    listDoramas(limit: $limit, skip: $skip, sort: $sort, filter: $filter) {
      _id
      name
      name_es
      slug
      names
      overview
      languages
      country
      premiere
      poster_path
      backdrop_path
      first_air_date
      episode_run_time
      isTVShow
      poster
      backdrop
      genres {
        name
        slug
      }
      networks {
        name
        slug
      }
      labels {
        name
        slug
      }
    }
  }
`

export const TREND_DORAMAS = gql`
  query trendsDoramas($limit: Float, $days: Float) {
    trendsDoramas(limit: $limit, days: $days) {
      _id
      first_air_date
      name
      name_es
      slug
      poster_path
      isTVShow
      poster
      count
    }
  }
`

export const DORAMAS_SIMILARS = gql`
  query similarsDoramas($limit: Float, $dorama_id: MongoID) {
    similarsDoramas(limit: $limit, dorama_id: $dorama_id) {
      _id
      name
      name_es
      slug
      poster_path
      isTVShow
      poster
    }
  }
`

export const TREND_DORAMAS_TODAY = gql`
  query trendsDoramasToday($limit: Float, $days: Float) {
    trendsDoramas(limit: $limit, days: $days) {
      _id
      country
      name
      name_es
      slug
      names
      overview
      languages
      poster_path
      backdrop_path
      first_air_date
      episode_run_time
      isTVShow
      poster
      premiere
      count
      backdrop
      genres {
        name
        slug
      }
      networks {
        name
        slug
      }
      labels {
        name
        slug
      }
    }
  }
`

export const LIST_DORAMAS_CARROUSEL = gql`
  query listDoramasCarrousel($limit: Float) {
    carrouselDoramas(limit: $limit) {
      _id
      name
      name_es
      slug
      names
      overview
      languages
      country
      premiere
      poster_path
      backdrop_path
      first_air_date
      episode_run_time
      isTVShow
      poster
      cast
      backdrop
      genres {
        name
        slug
      }
      networks {
        name
        slug
      }
      labels {
        name
        slug
      }
    }
  }
`

export const LIST_DORAMAS_CALENDAR = gql`
  query listDoramasCalendar {
    listDoramas(filter: { premiere: true }) {
      _id
      name
      name_es
      slug
      poster_path
      backdrop_path
      poster
      backdrop
      overview
      schedule {
        startEmision
        endEmision
        days
        hour
        season
        episode
      }
    }
  }
`

export const LIST_DORAMAS_BY_IDS = gql`
  query listDoramasByIds($_ids: [MongoID!]!) {
    listDoramasByIds(_ids: $_ids, limit: 200) {
      _id
      name
      slug
      cast
      names
      name_es
      overview
      languages
      created_by
      popularity
      poster_path
      poster
      backdrop
      vote_average
      backdrop_path
      first_air_date
      episode_run_time
      isTVShow
      genres {
        name
        slug
      }
      networks {
        name
        slug
      }
    }
  }
`

export const LIST_DORAMAS_SLUG = gql`
  query lisDoramasSlug {
    listDoramas(sort: UPDATEDAT_ASC) {
      slug
      cast
      updatedAt
      isTVShow
    }
  }
`

export const PAGINATION_DORAMAS = gql`
  query paginationDorama(
    $page: Int
    $perPage: Int
    $sort: SortFindManyDoramaInput
    $filter: FilterFindManyDoramaInput
  ) {
    paginationDorama(
      page: $page
      perPage: $perPage
      sort: $sort
      filter: $filter
    ) {
      count
      pageInfo {
        currentPage
        hasNextPage
        hasPreviousPage
      }
      items {
        number_of_seasons
        country
        _id
        name
        name_es
        slug
        cast
        names
        overview
        languages
        created_by
        popularity
        poster_path
        vote_average
        backdrop_path
        first_air_date
        episode_run_time
        isTVShow
        poster
        backdrop
        genres {
          name
          slug
        }
        networks {
          name
          slug
        }
      }
    }
  }
`

export const DETAIL_DORAMA = gql`
  query detailDorama($slug: String!) {
    detailDorama(filter: { slug: $slug }) {
      _id
      name
      slug
      cast
      country
      names
      name_es
      overview
      languages
      poster_path
      backdrop_path
      first_air_date
      episode_run_time
      isTVShow
      premiere
      poster
      trailer
      videos
      backdrop
      genres {
        name
        slug
      }
      labels {
        name
        slug
      }
    }
  }
`

export const RANDOM_DORAMA = gql`
  query randomDorama {
    randomDoramas(limit: 1) {
      _id
      name
      slug
      names
      name_es
      overview
      languages
      poster_path
      backdrop_path
      first_air_date
      episode_run_time
      isTVShow
      poster
      trailer
      videos
      backdrop
      genres {
        name
        slug
      }
      labels {
        name
        slug
      }
    }
  }
`

export const DETAIL_REPLACE = gql`
  query detailDoramaExtra($slug: String!) {
    detailDorama(filter: { slug: $slug }) {
      name
      name_es
      premiere
      trailer
      backdrop
      backdrop_path
    }
  }
`
