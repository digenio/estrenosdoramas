import gql from 'graphql-tag'
import config from '../utils/config'

export const DELETE_SEEN = gql`
  mutation deleteSeen($_id: MongoID!) {
    deleteSeen(_id: $_id) {
      recordId
    }
  }
`

export const ADD_SEEN = gql`
  mutation createSeen($record: CreateOneSeenInput!) {
    createSeen(record: $record) {
      record {
        _id
        seen_id
      }
    }
  }
`

export const LIST_SEEN = gql`
  query listSeen($group_id: MongoID!, $user_id: MongoID!) {
    listSeen(
      filter: { group_id: $group_id, user_id: $user_id, platform: "${config.app}" }
    ) {
      _id
      seen_id
    }
  }
`

export const LAST_SEEN = gql`
  query seenDoramas($limit: Float!) {
    listSeenDoramas(limit: $limit) {
      _id
      serie_name
      episode_number
      season_number
      still_path
      serie_poster
      backdrop
      slug
    }
  }
`
