import gql from 'graphql-tag'
import config from '../utils/config'

export const LIST_FAVS = gql`
  query listfavs($user_id: MongoID!) {
    listFav(filter: { user_id: $user_id, platform: "${config.app}" }) {
      _id
      serie_id
      name
      name_es
      poster_path
      poster
      type
      slug
    }
  }
`

export const CREATE_FAV = gql`
  mutation createFav($record: CreateOneFavInput!) {
    createFav(record: $record) {
      record {
        _id
        serie_id
        name
        name_es
        poster_path
        poster
        type
      }
    }
  }
`

export const REMOVE_FAV = gql`
  mutation deleteFav($_id: MongoID!) {
    deleteFav(_id: $_id) {
      record {
        _id
        serie_id
        name
        name_es
        poster_path
        poster
        type
      }
    }
  }
`
