import React, { useContext, useEffect } from 'react'
import { useLazyQuery } from '@apollo/client'

import { LIST_COUNTRYS } from '../gql/country'
import { ConfigContext } from './ConfigContext'

export const CountryContext = React.createContext()

const Provider = ({ children }) => {
  const { config, loading } = useContext(ConfigContext)

  const [
    getCountries,
    { data: { listCountriesByIds = [] } = {}, loading: ld }
  ] = useLazyQuery(LIST_COUNTRYS)

  useEffect(() => {
    if (config && config.countries && !loading) {
      const ids = config.countries.map(it => it.ref)
      getCountries({ variables: { _ids: ids } })
    }
  }, [config, loading])

  const value = {
    loading: loading || ld,
    countries: listCountriesByIds
  }

  return (
    <CountryContext.Provider value={value}>{children}</CountryContext.Provider>
  )
}

const Consumer = CountryContext.Consumer

export { Provider, Consumer }
export default { Provider, Consumer }
