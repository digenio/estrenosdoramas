import React, { useContext, useEffect } from 'react'
import { useLazyQuery, useMutation } from '@apollo/client'
import { CREATE_LIST, LIST_LISTS, REMOVE_LIST, UPDATE_LIST } from '../gql/list'
import { UserContext } from '../contexts/UserContext'
import config from '../utils/config'

export const ListContext = React.createContext()

const Provider = ({ children }) => {
  const { loggedUser } = useContext(UserContext)

  const [
    fetchList,
    { loading, error, data: { listList: list = [] } = {}, refetch }
  ] = useLazyQuery(LIST_LISTS)

  const [addList] = useMutation(CREATE_LIST)
  const [updateList] = useMutation(UPDATE_LIST)
  const [deleteList] = useMutation(REMOVE_LIST)

  useEffect(() => {
    if (loggedUser) {
      fetchList({ variables: { user_id: loggedUser._id } })
    }
  }, [loggedUser])

  const toggleList = (serie, status) => {
    if (status === 'remove') {
      deleteList({ variables: { _id: serie._id } })
        .then(() => refetch({ variables: { user_id: loggedUser._id } }))
        .catch(() => alert('Error al agregar la lista.'))
    } else {
      const index = list.findIndex(x => x?.serie_id === serie.serie_id)

      if (index !== -1) {
        updateList({ variables: { record: { ...serie, status } } })
          .then(() => refetch({ variables: { user_id: loggedUser._id } }))
          .catch(() => alert('Error al actualizar la lista.'))
      } else {
        addList({
          variables: {
            record: {
              ...serie,
              status,
              platform: config.app,
              user_id: loggedUser._id
            }
          }
        })
          .then(() => refetch({ variables: { user_id: loggedUser._id } }))
          .catch(() => Alert.alert('Error al agregar a la lista.'))
      }
    }
  }

  const value = {
    list,
    error,
    loading,
    toggleList
  }

  return <ListContext.Provider value={value}>{children}</ListContext.Provider>
}

const Consumer = ListContext.Consumer

export { Provider, Consumer }
export default { Provider, Consumer }
