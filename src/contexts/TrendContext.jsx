import React from 'react'
import { useQuery } from '@apollo/client'
import { TREND_DORAMAS_TODAY } from '../gql/dorama'

export const TrendContext = React.createContext()

const Provider = ({ children }) => {
  const { data: { trendsDoramas = [] } = {}, loading } = useQuery(
    TREND_DORAMAS_TODAY,
    {
      variables: { limit: 5, days: 0 },
      pollInterval: 30 * 60 * 1000
    }
  )

  const value = {
    trendsDoramas: trendsDoramas.slice().sort((a, b) => {
      return b.count - a.count
    }),
    loading
  }

  return <TrendContext.Provider value={value}>{children}</TrendContext.Provider>
}

const Consumer = TrendContext.Consumer

export { Provider, Consumer }
export default { Provider, Consumer }
