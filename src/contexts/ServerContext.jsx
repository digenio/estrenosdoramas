import React, { useContext, useEffect } from 'react'
import { useLazyQuery } from '@apollo/client'

import { LIST_SERVERS } from '../gql/server'
import { ConfigContext } from './ConfigContext'

export const ServerContext = React.createContext()

const Provider = ({ children }) => {
  const { config, loading } = useContext(ConfigContext)

  const [
    getServers,
    { data: { listServeresByIds = [] } = {}, loading: ld }
  ] = useLazyQuery(LIST_SERVERS)

  useEffect(() => {
    if (config && config.servers && !loading) {
      const ids = config.servers.map(it => it.ref)
      getServers({ variables: { _ids: ids } })
    }
  }, [config, loading])

  let servers = []

  if (config && config.servers && listServeresByIds.length > 0) {
    servers = config.servers.map(it =>
      listServeresByIds.find(s => s._id === it.ref)
    )
  }

  const value = {
    loading: loading || ld,
    servers
  }

  return (
    <ServerContext.Provider value={value}>{children}</ServerContext.Provider>
  )
}

const Consumer = ServerContext.Consumer

export { Provider, Consumer }
export default { Provider, Consumer }
