import React from 'react'
import { useQuery } from '@apollo/client'
import { NETWORK_LIST_IMAGES } from '../gql/network'

export const NetworkContext = React.createContext()

const Provider = ({ children }) => {
  const { data: { listNetworks = [] } = {}, loading } = useQuery(
    NETWORK_LIST_IMAGES
  )

  const value = { networks: listNetworks, loading }

  return (
    <NetworkContext.Provider value={value}>{children}</NetworkContext.Provider>
  )
}

const Consumer = NetworkContext.Consumer

export { Provider, Consumer }
export default { Provider, Consumer }
