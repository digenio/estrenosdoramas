import React from 'react'
import { useQuery } from '@apollo/client'
import { GET_CONFIG } from '../gql/config'

export const ConfigContext = React.createContext()

const Provider = ({ children }) => {
  const { data: { detailConfig } = {}, loading } = useQuery(GET_CONFIG)

  const value = { config: detailConfig, loading }

  return (
    <ConfigContext.Provider value={value}>{children}</ConfigContext.Provider>
  )
}

const Consumer = ConfigContext.Consumer

export { Provider, Consumer }
export default { Provider, Consumer }
