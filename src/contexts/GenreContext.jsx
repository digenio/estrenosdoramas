import React from 'react'
import { useQuery } from '@apollo/client'
import { GENRE_LIST_IMAGE } from '../gql/genre'

export const GenreContext = React.createContext()

const Provider = ({ children }) => {
  const { data: { listGenres = [] } = {}, loading } = useQuery(GENRE_LIST_IMAGE)

  const value = { genres: listGenres, loading }

  return <GenreContext.Provider value={value}>{children}</GenreContext.Provider>
}

const Consumer = GenreContext.Consumer

export { Provider, Consumer }
export default { Provider, Consumer }
