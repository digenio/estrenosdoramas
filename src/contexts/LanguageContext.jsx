import React, { useContext, useEffect } from 'react'
import { useLazyQuery } from '@apollo/client'

import { LIST_LANGUAGES } from '../gql/language'
import { ConfigContext } from './ConfigContext'

export const LanguageContext = React.createContext()

const Provider = ({ children }) => {
  const { config, loading } = useContext(ConfigContext)

  const [
    getLanguages,
    { data: { listLanguageesByIds = [] } = {}, loading: ld }
  ] = useLazyQuery(LIST_LANGUAGES)

  useEffect(() => {
    if (config && config.languages && !loading) {
      const ids = config.languages.map(it => it.ref)
      getLanguages({ variables: { _ids: ids } })
    }
  }, [config, loading])

  const value = {
    loading: loading || ld,
    languages: listLanguageesByIds
  }

  return (
    <LanguageContext.Provider value={value}>
      {children}
    </LanguageContext.Provider>
  )
}

const Consumer = LanguageContext.Consumer

export { Provider, Consumer }
export default { Provider, Consumer }
