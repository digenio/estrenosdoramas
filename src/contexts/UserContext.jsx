import React from 'react'
import { useUser } from '../hooks/useUser'
import { usePersistStorage } from '../hooks/usePersistStorage'

export const UserContext = React.createContext()

const Provider = ({ children }) => {
  const [jwt, setJWT, restored] = usePersistStorage('jwt', null)
  const { refetch, ...dataUser } = useUser({ jwt })

  const setAuth = async token => {
    setJWT(token)
    document.cookie = `jwt=${token}; path=/`
    setTimeout(() => {
      refetch()
    }, 200)
  }

  const logout = async () => {
    await setJWT()
    document.cookie = `jwt=; path=/`
    dataUser.changeUser()
    setTimeout(() => {
      refetch()
    }, 200)
  }

  const value = {
    jwt,
    restored,
    ...dataUser,
    logout,
    setAuth,
    reload: refetch
  }

  return <UserContext.Provider value={value}>{children}</UserContext.Provider>
}

const Consumer = UserContext.Consumer

export { Provider, Consumer }
export default { Provider, Consumer }
