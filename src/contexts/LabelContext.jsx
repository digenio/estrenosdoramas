import React from 'react'
import { useQuery } from '@apollo/client'

import { LIST_LABELS } from '../gql/label'

export const LabelContext = React.createContext()

const Provider = ({ children }) => {
  const { data: { listLabels = [] } = {}, loading } = useQuery(LIST_LABELS, {
    nextFetchPolicy: 'cache-first'
  })
  const value = { labels: listLabels, loading }
  return <LabelContext.Provider value={value}>{children}</LabelContext.Provider>
}

const Consumer = LabelContext.Consumer

export { Provider, Consumer }
export default { Provider, Consumer }
