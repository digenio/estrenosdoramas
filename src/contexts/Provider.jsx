import FavsContext from './FavsContext'
import ListContext from './ListContext'
import UserContext from './UserContext'
import LabelContext from './LabelContext'
import GenreContext from './GenreContext'
import TrendContext from './TrendContext'
import ConfigContext from './ConfigContext'
import ServerContext from './ServerContext'
import CountryContext from './CountryContext'
import NetworkContext from './NetworkContext'
import LanguageContext from './LanguageContext'

export function ContextsProviders ({ children }) {
  return (
    <ConfigContext.Provider>
      <LabelContext.Provider>
        <UserContext.Provider>
          <FavsContext.Provider>
            <ListContext.Provider>
              <NetworkContext.Provider>
                <GenreContext.Provider>
                  <CountryContext.Provider>
                    <LanguageContext.Provider>
                      <ServerContext.Provider>
                        <TrendContext.Provider>
                          {children}
                        </TrendContext.Provider>
                      </ServerContext.Provider>
                    </LanguageContext.Provider>
                  </CountryContext.Provider>
                </GenreContext.Provider>
              </NetworkContext.Provider>
            </ListContext.Provider>
          </FavsContext.Provider>
        </UserContext.Provider>
      </LabelContext.Provider>
    </ConfigContext.Provider>
  )
}
