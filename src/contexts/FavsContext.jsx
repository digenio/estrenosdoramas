import React, { useContext, useEffect } from 'react'
import { useLazyQuery, useMutation } from '@apollo/client'
import { CREATE_FAV, LIST_FAVS, REMOVE_FAV } from '../gql/favs'
import { UserContext } from './UserContext'
import config from '../utils/config'

export const FavsContext = React.createContext()

const Provider = ({ children }) => {
  const { loggedUser } = useContext(UserContext) || {}

  const [
    fetchFavs,
    { loading, error, data: { listFav: favs = [] } = {}, refetch }
  ] = useLazyQuery(LIST_FAVS)

  const [addFav] = useMutation(CREATE_FAV)
  const [deleteFav] = useMutation(REMOVE_FAV)

  useEffect(() => {
    if (loggedUser) {
      fetchFavs({ variables: { user_id: loggedUser._id } })
    }
  }, [loggedUser])

  const toggleFav = serie => {
    if (loggedUser) {
      const index = favs.findIndex(x => x?.serie_id === serie.serie_id)
      if (index !== -1) {
        deleteFav({ variables: { _id: favs[index]._id } })
          .then(() => refetch({ variables: { user_id: loggedUser._id } }))
          .catch(() => alert('Error al agregar a favoritos'))
      } else {
        addFav({
          variables: {
            record: {
              ...serie,
              platform: config.app,
              user_id: loggedUser._id
            }
          }
        })
          .then(() => refetch({ variables: { user_id: loggedUser._id } }))
          .catch(() => alert('Error al eliminar de favoritos'))
      }
    }
  }

  const value = {
    favs,
    error,
    loading,
    toggleFav
  }

  return <FavsContext.Provider value={value}>{children}</FavsContext.Provider>
}

const Consumer = FavsContext.Consumer

export { Provider, Consumer }
export default { Provider, Consumer }
