import { encode } from "js-base64";
import { useMemo } from "react";
import { ApolloClient, HttpLink, InMemoryCache } from "@apollo/client";
import { persistCache, LocalStorageWrapper } from "apollo3-cache-persist";

import { makeid } from "../utils/functions";
import { URL_API } from "../utils/urls";
import config from "../utils/config";

let apolloClient;
const cache = new InMemoryCache();

function createApolloClient() {
  const token =
    typeof window !== "undefined"
      ? localStorage.getItem("jwt") && localStorage.getItem("jwt") != "undefined"
        ? JSON.parse(localStorage.getItem("jwt"))
        : ""
      : "";

  const init = makeid(10) + "_" + makeid(12);
  const token_lifetime = 60 * 60 * 12;
  const expiration = Math.round(Date.now() / 1000 + token_lifetime);
  const base_64 = encode(expiration.toString());
  const send = init + "_" + makeid(5) + base_64;

  // if (typeof window !== 'undefined') {
  //   persistCache({
  //     cache,
  //     storage: new LocalStorageWrapper(window.localStorage)
  //   })
  // }

  return new ApolloClient({
    ssrMode: typeof window !== "undefined",
    link: new HttpLink({
      uri: `${URL_API}/gql`,
      headers: {
        authorization: `Bear ${token}`,
        "x-access-jwt-token": token,
        "x-access-platform": send,
        referer: config.url + "/",
        platform: config.platform,
      },
    }),
    cache: cache,
  });
}

export function initializeApollo(initialState = null) {
  const _apolloClient = apolloClient ?? createApolloClient();

  if (initialState) {
    const existingCache = _apolloClient.extract();

    _apolloClient.cache.restore({ ...existingCache, ...initialState });
  }

  if (typeof window === "undefined") return _apolloClient;

  if (!apolloClient) apolloClient = _apolloClient;
  return _apolloClient;
}

export function useApollo(initialState) {
  const store = useMemo(() => initializeApollo(initialState), [initialState]);
  return store;
}
