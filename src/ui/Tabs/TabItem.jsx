import { useEffect } from 'react'
import styled, { css } from 'styled-components'

export const TabItem = ({
  id,
  label,
  value,
  onChange,
  onClick,
  sideBar,
  children,
  button,
  season
}) => {
  const handleSeason = () => {
    onChange(id)
    onClick ? onClick(id) : null
  }
  useEffect(() => {
    handleSeason()
  }, [])
  return (
    <Tab
      onClick={handleSeason}
      active={id === value}
      sideBar={sideBar}
      button={button}
      season={season}
    >
      {!sideBar && <Figure active={id === value}></Figure>}
      {sideBar ? children : label}
    </Tab>
  )
}

const Tab = styled.li`
  display: flex;
  display: inline-block;
  margin-right: 1.5rem;
  position: relative;
  cursor: pointer;
  vertical-align: top;
  text-align: center;
  width: 50px;
  padding: 10px 0 10px 0;
  font-size: 14px;

  user-select: none;
  border-radius: 5px;
  margin-bottom: 20px;

  ${({ sideBar }) =>
    sideBar &&
    css`
      color: ${({ theme }) => theme.colors.black};
      background-color: ${({ theme }) => theme.colors.body};
    `}
  @media ${({ theme }) => theme.devices.laptop} {
    font-size: 16px;
  }

  :before {
    content: '';
    position: absolute;
    max-width: 0;
    bottom: -0.7rem;
    height: 4px;
    left: 0;
    right: 0;
    transition: 0.3s;
    margin: auto;
  }
  ${({ button }) =>
    button &&
    css`
      background: ${({ theme }) => theme.colors.body};
      padding: 12px;
      border-radius: 5px;
      margin-bottom: 20px;
      font-size: 0.8rem;
      ${({ season }) =>
        season &&
        css`
          margin-bottom: 0;
        `}
      @media ${({ theme }) => theme.devices.laptop} {
        font-size: 1rem;
        padding: 14px;
      }
    `}
  ${({ active }) =>
    active &&
    css`
      color: ${({ theme }) => theme.colors.secondary};
      max-width: 100%;

      ::before {
        max-width: 100%;
        background-color: ${({ theme }) => theme.colors.secondary};
      }

      ${({ sideBar }) =>
        sideBar &&
        css`
          color: white;
          background-color: ${({ theme }) => theme.colors.primary};
          ::before {
            max-width: 0;
          }
        `}
    `}

  ${({ season }) =>
    season &&
    css`
      width: 40%;
      margin-right: 0;
      @media ${({ theme }) => theme.devices.laptop} {
        width: auto;
        margin-right: 1.5rem;
      }
    `}
`

const Figure = styled.div`
  width: 0;
  height: 0;
  border-left: 5px solid #ffc107;
  border-top: 8px solid transparent;
  border-bottom: 8px solid transparent;
  position: absolute;
  transform: rotate(90deg);
  bottom: -20px;
  left: calc(50% - 2px);
  z-index: 10;
  opacity: 0;

  ${({ active }) =>
    active &&
    css`
      opacity: 1;
    `}
`
