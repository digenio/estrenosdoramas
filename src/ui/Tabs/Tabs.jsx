import styled, { css } from 'styled-components'

export const Tabs = ({ children, sideBar, season }) => {
  return (
    <TabNav sideBar={sideBar} season={season}>
      {children}
    </TabNav>
  )
}

const TabNav = styled.ul`
  width: 100%;
  gap: 10px;
  justify-items: center;
  flex-direction: row;
  white-space: nowrap;
  position: relative;
  /* overflow-x: scroll; */
  margin-bottom: 30px;
  padding: 10px;
  border-bottom: 1px solid rgba(255, 255, 255, 0.2);
  ::-webkit-scrollbar {
    display: none;
  }
  @media ${({ theme }) => theme.devices.mobileL} {
    display: flex;
  }

  ${({ sideBar }) =>
    sideBar &&
    css`
      border-bottom: none;
      margin-bottom: 0;
      padding: 0;
    `}

  ${({ season }) =>
    season &&
    css`
      justify-content: center;
      gap: 10px;
      margin-bottom: 30px;

      @media ${({ theme }) => theme.devices.laptop} {
        gap: 0;
        justify-content: flex-start;
      }
    `}
`
