import styled, { css } from 'styled-components'
import { FaPlay, FaRegClock, FaRegHourglass } from 'react-icons/fa'
import { IoFlagSharp } from 'react-icons/io5'
import { minutesToHours } from '../../utils/functions'
export const DetailRating = ({
  ratingList,
  year = '2020',
  country = 'Corea',
  premiere = true,
  runtime,
  category,
  aside
}) => {
  return (
    <Details ratingList={ratingList} aside={aside}>
      <Year>
        <FaRegClock />
        {'  '}
        {year}
      </Year>

      <Country>
        <IoFlagSharp /> {country}
      </Country>

      {category == 'Dorama' && (
        <Status premiere={premiere}>
          <FaPlay /> <span>{premiere ? 'En emision' : 'Finalizado'}</span>
        </Status>
      )}

      {category == 'Movie' && (
        <Status premiere={premiere} category={category}>
          <FaRegHourglass /> {minutesToHours(runtime)}
        </Status>
      )}
    </Details>
  )
}
const Details = styled.div`
  display: flex;
  margin-top: 5px;
  font-size: 0.8rem;
  ${({ ratingList }) =>
    ratingList &&
    css`
      color: ${({ theme }) => theme.colors.gray};
    `}

  ${({ aside }) =>
    aside &&
    css`
      flex-direction: column;
      gap: 5px;
    `}
`

const Year = styled.span`
  margin-right: 0.2rem;
  padding: 0 3px;
`

const Country = styled.span`
  margin-right: 0.2rem;
  padding: 0 3px;
  text-transform: capitalize;
`

const Status = styled.span`
  margin-right: 0.2rem;
  padding: 0 3px;
  text-transform: capitalize;
  color: #72d072;
  ${({ premiere }) =>
    premiere &&
    css`
      color: #f17f7f;
    `}

  ${({ premiere, category }) =>
    premiere &&
    category === 'Movie' &&
    css`
      color: ${({ theme }) => theme.colors.gray};
    `}
`
