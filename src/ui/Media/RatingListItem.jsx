import Img from 'react-cool-img'
import Link from 'next/link'
import styled, { css } from 'styled-components'
import { DetailRating } from '../Media/DetailRating'
import { FaPlay } from 'react-icons/fa'

export const RatingListItem = ({
  carousel,
  image,
  name,
  airDate,
  count,
  page,
  link,
  country,
  year,
  premiere,
  runtime,
  category,
  aside
}) => {
  return (
    <Link href={page} as={link}>
      <a>
        <Item>
          <Figure carousel={carousel}>
            <Img src={image}></Img>
            <Over></Over>
            <PlayIcon>
              <FaPlay />
            </PlayIcon>
          </Figure>

          <Description>
            <p>{name}</p>

            <Details></Details>
            <DetailRating
              aside={aside}
              ratingList
              year={year}
              country={country}
              premiere={premiere}
              runtime={runtime}
              category={category}
            />
          </Description>
        </Item>
      </a>
    </Link>
  )
}

const Figure = styled.figure`
  position: relative;
  width: 55px;
  height: 85px;
  margin-bottom: 10px;

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    border-radius: 5px;
    position: relative;
  }

  ${({ carousel }) =>
    carousel &&
    css`
      display: flex;
      justify-content: center;
      :before {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        object-fit: cover;

        background: linear-gradient(rgba(0, 0, 0, 0) 0%, #000 100%);
        height: 50%;
        top: auto;
        bottom: 0;
        z-index: 1;
        content: ' ';
        border-radius: 5px;
      }
    `}
`
const PlayIcon = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(0, 0, 0, 0.4);
  width: 10px;
  height: 10px;
  border-radius: 100%;
  font-size: 5px;
  box-shadow: 0 0 0 2px #fff, 0 0 10px rgb(0 0 0 / 50%);
  color: ${({ theme }) => theme.colors.secondary};
  position: absolute;
  top: calc(50% - 5px);
  left: calc(50% - 5px);
  z-index: 10;
  transition: 0.2s;
  opacity: 0;
  transform: scale(0);
`
const Over = styled.div`
  opacity: 0;
  :after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-color: ${({ theme }) => theme.colors.primary};
    opacity: 0.5;
    z-index: 2;
    border-radius: 5px;
  }
`

const Description = styled.div`
  p {
    font-weight: 300;
  }
`
const Item = styled.div`
  position: relative;
  display: flex;
  min-width: 0;
  padding: 5px;
  gap: 10px;
  width: 100%;
  cursor: pointer;
  :hover ${Over} {
    opacity: 1;
  }

  :hover ${PlayIcon} {
    opacity: 1;
    transform: scale(1.2);
  }
  @media ${({ theme }) => theme.devices.laptop} {
  }
`

const Details = styled.div``
