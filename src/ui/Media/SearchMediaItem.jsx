import styled, { css } from 'styled-components'
import Link from 'next/link'
import Img from 'react-cool-img'

export const SearchMediaItem = ({ item = [], close, calendar }) => {
  const { name, nameEs, image, slug, _id, category, page, link } = item

  return (
    <Link href={page} as={link}>
      <a>
        <SearchItem calendar={calendar} onClick={close}>
          <Card>
            <Image>
              <Img src={image} alt={name} />
            </Image>
            <Description calendar={calendar}>
              <h3>{name}</h3>
              <h4>{nameEs}</h4>
              <Categorie>{category}</Categorie>
            </Description>
          </Card>
        </SearchItem>
      </a>
    </Link>
  )
}

const Card = styled.div`
  display: flex;
  padding: 5px;
`
const Image = styled.figure`
  width: 50px;
  height: 70px;
  overflow: hidden;
  border-radius: 5px;
  img {
    vertical-align: middle;
    display: inline-block;
    object-fit: cover;
  }
`

const Description = styled.div`
  font-size: 14px;
  margin-left: 10px;
  width: 80%;
  h3 {
    margin-bottom: 5px;
    color: ${({ theme }) => theme.colors.gray};
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
  }
  h4 {
    margin-bottom: 7px;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    color: ${({ theme }) => theme.colors.lighGray};
  }
  ${({ calendar }) =>
    calendar &&
    css`
      h4 {
        color: ${({ theme }) => theme.colors.lighGray};
      }
    `}
`

const Categorie = styled.span`
  display: block;
  width: 45px;
  height: 16px;
  padding: 5px;
  text-align: center;
  line-height: 5px;
  top: 0.7rem;
  left: 0.7rem;
  text-transform: uppercase;
  font-size: 0.6rem;
  color: ${({ theme }) => theme.colors.primary};
  background-color: ${({ theme }) => theme.colors.details};
`

const SearchItem = styled.article`
  max-height: 80px;
  border-radius: 5px;
  cursor: pointer;
  margin-bottom: 5px;
  hr {
    width: 100%;
    height: 2px;
    display: inline-block;

    background-color: ${({ theme }) => theme.colors.bodyH};
  }
  ${({ calendar }) =>
    calendar &&
    css`
      background-color: ${({ theme }) => theme.colors.secondary};
    `}
  :hover {
    background-color: ${({ theme }) => theme.colors.primaryHover};
  }
  :hover h3 {
    color: white;
  }
  :hover h4 {
    color: white;
  }
  :hover ${Categorie} {
    color: white;
  }
`
