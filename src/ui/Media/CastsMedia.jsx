import styled, { css } from 'styled-components'

export const CastsMedia = ({ carousel, genero, labels }) => {
  return (
    <Casts carousel={carousel}>
      <p>
        <span>Géneros :</span>
        {genero?.slice(0, 2).map((item, idex) => (
          <Item key={idex}>{item.name}</Item>
        ))}
      </p>
      <p>
        {labels && (
          <>
            <span>Etiquetas :</span>
            {labels?.slice(0, 2).map((item, idex) => (
              <Item key={idex}>{item.name}</Item>
            ))}
          </>
        )}
      </p>
    </Casts>
  )
}
const Casts = styled.div`
  font-size: 0.7rem;

  ${({ carousel }) =>
    carousel &&
    css`
      @media ${({ theme }) => theme.devices.laptop} {
        font-size: 1rem;
      }
    `}

  p {
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
  }
  span {
    color: ${({ theme }) => theme.colors.mediumGray};
    font-weight: 700;
    opacity: 0.7;
  }

  ${({ carousel }) =>
    carousel &&
    css`
      span {
        color: ${({ theme }) => theme.colors.gray};

        @media ${({ theme }) => theme.devices.laptop} {
          color: ${({ theme }) => theme.colors.white};
        }
      }
    `}
`
const Item = styled.div`
  margin: 0 2px;
  display: inline-block;
  font-weight: 400;
`
