import styled, { css } from 'styled-components'
import { RatingListItem } from '../Media/RatingListItem'

export const RatingList = ({ children, aside }) => {
  return <List aside={aside}>{children}</List>
}

const List = styled.div`
  position: relative;
  width: 100%;
  border-radius: 5px;
  padding: 10px;
  z-index: 0;
  background-color: ${({ theme }) => theme.colors.body};
  font-size: 0.875rem;
  margin-top: 10px;
  @media ${({ theme }) => theme.devices.laptop} {
    max-width: 340px;
  }

  ${({ aside }) =>
    aside &&
    css`
      display: flex;
    `}
`
