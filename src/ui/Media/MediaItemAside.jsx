import Img from 'react-cool-img'
import styled from 'styled-components'
import { FaPlay } from 'react-icons/fa'

export const MediaItemAside = () => {
  return (
    <Item>
      <Figure>
        <Img src='https://image.tmdb.org/t/p/w220_and_h330_face/rsdTehWrJPGZkggKm6Jvs4TxAkj.jpg'></Img>
        <Over></Over>
        <Title>BTS World Tour</Title>
      </Figure>
      <PlayIcon>
        <FaPlay />
      </PlayIcon>
    </Item>
  )
}

const Figure = styled.figure`
  position: relative;
  width: 100%;
  height: 144px;

  @media ${({ theme }) => theme.devices.tablet} {
    height: 144px;
  }
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    border-radius: 5px;
    position: relative;
  }

  display: flex;
  justify-content: center;
  :before {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    object-fit: cover;

    background: linear-gradient(rgba(0, 0, 0, 0) 0%, #000 100%);
    height: 50%;
    top: auto;
    bottom: 0;
    z-index: 1;
    content: ' ';
    border-radius: 5px;
  }
`
const Over = styled.div`
  opacity: 0;
  :after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-color: ${({ theme }) => theme.colors.primary};
    opacity: 0.5;
    z-index: 2;
    border-radius: 5px;
  }
`
const PlayIcon = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(0, 0, 0, 0.4);
  width: 35px;
  height: 35px;
  border-radius: 100%;
  font-size: 15px;
  box-shadow: 0 0 0 2px #fff, 0 0 10px rgb(0 0 0 / 50%);
  color: ${({ theme }) => theme.colors.secondary};
  position: absolute;
  top: calc(50% - 16px);
  left: calc(50% - 18px);
  z-index: 10;
  transition: 0.2s;
  opacity: 0;
  transform: scale(0);
`
const Title = styled.h2`
  white-space: normal;
  font-size: 0.875rem;
  text-overflow: ellipsis;
  overflow: hidden;
  text-align: center;
  line-height: 20px;
  font-weight: 400;
  text-transform: capitalize;
  top: 9;
  position: absolute;
  z-index: 5;
  bottom: 20px;
  color: white;
  font-size: 12px;
`

const Item = styled.div`
  position: relative;
  min-width: 0;

  width: 100%;

  cursor: pointer;

  :hover ${PlayIcon} {
    opacity: 1;
    transform: scale(1.2);
  }
  :hover ${Over} {
    opacity: 1;
  }

  @media ${({ theme }) => theme.devices.laptop} {
  }
`
