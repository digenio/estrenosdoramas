import Img from 'react-cool-img'
import Link from 'next/link'
import styled, { css } from 'styled-components'
import { FaPlay } from 'react-icons/fa'

import { useInView } from 'react-intersection-observer'
import { DetailRating } from './DetailRating'
import { CastsMedia } from './CastsMedia'
import { useNearScreen } from '../../hooks/useNearScreen'
import { shortOverviewDots } from '../../utils/functions'

export const MediaItem = ({
  carousel,
  name,
  image,
  type,
  page = '/',
  link,
  overview,
  genres,
  labels,
  chapter,
  season_number,
  episode_number,
  season,
  country,
  year,
  premiere,
  category,
  categorie,
  refContainer
}) => {
  const { isNearScreen, fromRef } = useNearScreen({
    root: refContainer?.current
  })

  // const overviewCut = overview?.slice(0, 110).padEnd(113, '.')
  const overviewCut = shortOverviewDots(overview)

  return (
    <Item carousel={carousel}>
      <Link href={page} as={link}>
        <a>
          <Figure carousel={carousel}>
            <Img src={image}></Img>
            <Over></Over>
            {carousel && <Title carousel={carousel}>{name}</Title>}
            {chapter && (
              <ChapterDescription>
                S{season_number}.E{episode_number}
              </ChapterDescription>
            )}
            {type == 'Dorama' && (
              <ChapterDescription>
                {season} <span>- Seasons</span>
              </ChapterDescription>
            )}
          </Figure>
        </a>
      </Link>
      <Categorie type={type}>{category || type}</Categorie>
      <PlayIcon>
        <FaPlay />
      </PlayIcon>

      {!carousel && <Title>{name}</Title>}

      {!carousel && categorie == 'viendo' && (
        <>
          <ContentModal chapter={chapter}>
            <Modal ref={fromRef} right={isNearScreen}>
              <ModalHeader>
                <p>{name}</p>
                <DetailRating
                  country={country}
                  year={year}
                  premiere={premiere}
                  category={category}
                />
              </ModalHeader>
              <ModalBody>
                <p>{overview}</p>
              </ModalBody>
              <ModalFooter>
                <CastsMedia genero={genres} labels={labels} />
              </ModalFooter>
            </Modal>
          </ContentModal>
        </>
      )}
      {!carousel && (
        <>
          <ContentModal chapter={chapter}>
            <Modal ref={fromRef} right={isNearScreen}>
              <ModalHeader>
                <p>{name}</p>
                <DetailRating
                  country={country}
                  year={year}
                  premiere={premiere}
                  category={category}
                />
              </ModalHeader>
              <ModalBody>
                <p>{overviewCut}</p>
              </ModalBody>
              <ModalFooter>
                <CastsMedia genero={genres} labels={labels} />
              </ModalFooter>
            </Modal>
          </ContentModal>
        </>
      )}
    </Item>
  )
}

const Figure = styled.figure`
  position: relative;
  width: 100%;
  height: 200px;
  margin-bottom: 10px;
  @media ${({ theme }) => theme.devices.tabletS} {
    height: 200px;
  }
  @media ${({ theme }) => theme.devices.tablet} {
    height: 240px;
  }
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    border-radius: 5px;
    position: relative;
  }

  ${({ carousel }) =>
    carousel &&
    css`
      height: 200px;
      width: 132px;
      display: flex;
      justify-content: center;
      :before {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        object-fit: cover;

        background: linear-gradient(rgba(0, 0, 0, 0) 0%, #000 100%);
        height: 50%;
        top: auto;
        bottom: 0;
        z-index: 1;
        content: ' ';
        border-radius: 5px;
      }
    `}
`
const Over = styled.div`
  opacity: 0;
  :after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-color: ${({ theme }) => theme.colors.primary};
    opacity: 0.5;
    z-index: 2;
    border-radius: 5px;
  }
`
const PlayIcon = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(0, 0, 0, 0.4);
  width: 35px;
  height: 35px;
  border-radius: 100%;
  font-size: 15px;
  box-shadow: 0 0 0 2px #fff, 0 0 10px rgb(0 0 0 / 50%);
  color: ${({ theme }) => theme.colors.secondary};
  position: absolute;
  top: calc(50% - 35px);
  left: calc(50% - 18px);
  z-index: 10;
  transition: 0.2s;
  opacity: 0;
  transform: scale(0);
`
const Title = styled.h2`
  white-space: normal;
  font-size: 0.875rem;
  text-overflow: ellipsis;
  overflow: hidden;
  text-align: center;
  line-height: 20px;
  font-weight: 400;

  top: 9;

  ${({ carousel }) =>
    carousel &&
    css`
      position: absolute;
      z-index: 5;
      bottom: 20px;
      color: white;
      font-size: 12px;
    `}
`

const ChapterDescription = styled.div`
  position: absolute;
  bottom: 0;
  width: 100%;
  background: black;
  opacity: 0.9;
  height: 30px;
  color: ${({ theme }) => theme.colors.secondary};
  font-size: 0.8rem;
  text-align: center;
  line-height: 30px;
  span {
    color: white;
  }
`
// Modal
const ContentModal = styled.div`
  z-index: 100;
  ${({ chapter }) =>
    chapter &&
    css`
      display: none;
      z-index: 9999;
    `}
`
const Modal = styled.div`
  z-index: 100;
  display: flex;
  pointer-events: none;
  flex-direction: column;
  position: absolute;
  top: calc(50% - 100px);
  right: -250px;
  display: none;
  padding: 25px;
  width: 170%;
  max-width: 270px;
  background-color: white;
  border-radius: 5px;
  box-shadow: 0px 8px 80px #6579a04d;
  z-index: 10;

  ${({ right }) =>
    right &&
    css`
      right: 160px;
    `}

 
    :before {
    content: '';
    position: absolute;
    left: -9px;
    top: 0;
    bottom: 0;
    margin: auto;
    width: 0;
    height: 0;
    border-top: 10px solid transparent;
    border-right: 10px solid white;
    border-bottom: 10px solid transparent;

    ${({ right }) =>
    right == 0.5 &&
    css`
        transform: rotate(180deg);
        left: initial;
        right: -9px;
      `}

  ${({ right }) =>
    right == 1 &&
    css`
      left: initial;
      transform: rotate(180deg);
      right: -9px;
    `}
    ${({ right }) =>
    right == 0.4 &&
    css`
        left: initial;
        transform: rotate(180deg);
        right: -9px;
      `}
      ${({ right }) =>
    right == 0.9 &&
    css`
          left: initial;
          transform: rotate(180deg);
          right: -9px;
        `}
  }
`
const ModalHeader = styled.div`
  font-size: 1rem;
`

const ModalBody = styled.div`
  margin: 10px 0;
  font-size: 0.7rem;
  color: ${({ theme }) => theme.colors.lighGray};
  max-width: 270px;

  p {
    height: 55px;
    white-space: normal;
    text-overflow: ellipsis;
    overflow: hidden;
  }
`

const Item = styled.div`
  position: relative;
  min-width: 0;

  width: 100%;
  /* height: 200px; */
  cursor: pointer;
  margin-bottom: 1.25rem;

  ${({ carousel }) =>
    carousel &&
    css`
      padding: 10px;
      margin-bottom: 0;
    `}
    :hover{
    ${PlayIcon} {
    opacity: 1;
    transform: scale(1.2);
  }
  :hover ${Over} {
    opacity: 1;
  }

  @media ${({ theme }) => theme.devices.laptop} {
    :hover ${Modal} {
      display: block;
    }
  }
`
const ModalFooter = styled.div``

const Categorie = styled.p`
  font-size: 12px;
  background-color: ${({ theme }) => theme.colors.primary};
  width: 80px;
  text-align: center;
  color: white;
  padding: 5px;
  border-radius: 20px;
  position: absolute;
  top: 5%;
  left: 5%;

  ${({ type }) =>
    type &&
    css`
      left: 10%;
      top: 8%;
    `}
`
