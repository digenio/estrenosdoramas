import { FaCheck } from 'react-icons/fa'
import styled, { css } from 'styled-components'
import { addOpacityToColor } from '../../styles/ultis'

export function CheckBox ({ checked, value, onChange }) {
  const handleChange = e => {
    const check = e.target.checked
    onChange(check, value)
  }
  return (
    <Checkbox checked={checked}>
      <Input
        type='checkbox'
        name={value}
        checked={checked}
        onChange={handleChange}
      />
      {/* <InputIcon onClick={() => onChange(false, value)}>
        <FaCheck />
      </InputIcon> */}
    </Checkbox>
  )
}

const Checkbox = styled.div`
  display: inline-block;
  position: relative;
  width: 20px;
  height: 20px;

  ${({ checked }) =>
    checked &&
    css`
      a {
        display: block;
      }
    `}
`

const Input = styled.input`
  background-color: ${({ theme }) => theme.colors.gray};
  display: block;
  width: 100%;
  height: 100%;
  cursor: pointer;
  border: 1px solid ${({ theme }) => addOpacityToColor(theme.colors.gray, 0.5)};

  &:checked {
    background-color: ${({ theme }) => theme.colors.primary};
    border: 1px solid
      ${({ theme }) => addOpacityToColor(theme.colors.primary, 0.5)};
  }

  &:not(:focus) {
    opacity: 1;
  }
`

const InputIcon = styled.a`
  display: none;
  position: absolute;
  top: 1px;
  left: 4px;

  svg {
    color: ${({ theme }) => theme.colors.white};
  }
`
