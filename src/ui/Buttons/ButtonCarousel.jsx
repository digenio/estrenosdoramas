import styled from 'styled-components'

import { MdKeyboardArrowRight, MdKeyboardArrowLeft } from 'react-icons/md'
const ButtonCarousel = ({ children, className, onClick }) => {
  return (
    <button className={className} onClick={onClick}>
      <i>{children}</i>
    </button>
  )
}

export const ButtonGroup = ({ next, previous, goToSlide, ...rest }) => {
  const {
    carouselState: { currentSlide }
  } = rest
  return (
    <div className='carousel-button-group'>
      <ArrowButtonLeft
        className={currentSlide === 0 ? 'disable' : ''}
        onClick={() => previous()}
      >
        <MdKeyboardArrowLeft size={28} />
      </ArrowButtonLeft>
      <ArrowButtonRight onClick={() => next()}>
        <MdKeyboardArrowRight size={28} />
      </ArrowButtonRight>
    </div>
  )
}

const ButtonCarouselStyle = styled(ButtonCarousel)`
  display: none;
  position: absolute;
  cursor: pointer;
  border: none;
  border-radius: 20px;
  color: white;
  /* box-shadow: 0 0 0 0.35rem rgb(10 10 10 / 40%); */
  background-color: ${({ theme }) => theme.colors.primaryHover};

  min-width: 3rem;
  height: 3rem;
  width: 3rem;
  top: 40;

  z-index: 1;
  :hover {
    transform: scale(1.07);
  }
  @media ${({ theme }) => theme.devices.laptop} {
    display: block;
  }
`

const ArrowButtonLeft = styled(ButtonCarouselStyle)`
  left: 5px;
`
const ArrowButtonRight = styled(ButtonCarouselStyle)`
  right: 10px;
`
