import { useState } from 'react'
import styled, { css } from 'styled-components'

export const HamburguerButton = ({ onClick, viewMenu }) => {
  const [open, setOpen] = useState(false)
  if (viewMenu) {
  }
  const handleOpen = () => {
    onClick()
    if (!open) {
      setOpen(true)
    } else {
      setOpen(false)
    }
  }

  return (
    <MenuBtn onClick={handleOpen} open={open} viewMenu={viewMenu}>
      <MenuBurger open={open}></MenuBurger>
    </MenuBtn>
  )
}

const PsudoElements = css`
  content: '';
  position: absolute;
  width: 25px;
  height: 3px;
  background-color: #fff;
  border-radius: 5px;
  box-shadow: 0 2px 5px rgba(255, 101, 47, 0.2);
  transition: all 0.5s ease-in-out;
`

const MenuBtn = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 60px;
  height: 40px;
  cursor: pointer;
  transition: all 0.5s ease-in-out;
  background-color: ${({ theme }) => theme.colors.primary};
  border-radius:5px;

    :before {
    position: fixed;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-color: ${({ theme }) => theme.colors.black};
    z-index:6;
    visibility: hidden;
    opacity: 0;
    transition: 0.3s;
    visibility: visible;
    opacity: 0.7;

  }
  ${({ viewMenu }) =>
    viewMenu &&
    css`
      :before {
        content: '';
        z-index: -1;
      }
    `}
    @media ${({ theme }) => theme.devices.laptop} {
    display: none;
    background-color: ${({ theme }) => theme.colors.primary};
  }

`

const MenuBurger = styled.div`
  width: 25px;
  height: 3px;
  background-color: #fff;
  border-radius: 5px;
  box-shadow: 0 2px 5px rgba(255, 101, 47, 0.2);
  transition: all 0.5s ease-in-out;
  @media ${({ theme }) => theme.devices.laptop} {
    display: none;
  }

  :before {
    ${PsudoElements}
    transform: translateY(-10px);
    ${({ open }) =>
      open &&
      css`
        transform: rotate(45deg) translate(35px, -35px);
      `}
  }
  :after {
    ${PsudoElements}
    transform: translateY(10px);
    ${({ open }) =>
      open &&
      css`
        transform: rotate(-45deg) translate(35px, 35px);
      `}
  }

  ${({ open }) =>
    open &&
    css`
      transform: translateX(-50px);
      background-color: transparent;
      box-shadow: none;
    `}
`
