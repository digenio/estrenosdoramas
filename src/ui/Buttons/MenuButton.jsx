import styled, { css } from 'styled-components'
import { GoThreeBars } from 'react-icons/go'
export const MenuButton = ({ onClick, viewMenu }) => {
  return (
    <WrapperButton viewMenu={viewMenu} onClick={() => viewMenu && onClick()}>
      <input type='checkbox' name='btn' id='btn' onClick={onClick} />
      <label htmlFor='btn'>
        <i>
          <GoThreeBars color={'white'} size={25} />
        </i>
      </label>
    </WrapperButton>
  )
}

const WrapperButton = styled.div`
  margin-right: 20px;
  @media ${({ theme }) => theme.devices.laptop} {
    display: none;
  }
  input {
    display: none;
  }
  :before {
    position: fixed;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-color: ${({ theme }) => theme.colors.black};
    z-index: 2;
    visibility: hidden;
    opacity: 0;
    transition: 0.3s;
    visibility: visible;
    opacity: 0.7;
  }
  ${({ viewMenu }) =>
    viewMenu &&
    css`
      :before {
        content: '';
      }
    `}
`
