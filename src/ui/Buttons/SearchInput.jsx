import { useRef, useEffect } from 'react'
import styled, { css } from 'styled-components'

import { colors } from '../../styles/theme'
import { FaSearch, FaTimesCircle } from 'react-icons/fa'
import { FadeIn } from '../../styles/animations'
import { useForm } from '../../hooks/useForm'
import { Search } from '../../components/Base/Search'

export const SearchInput = ({ viewInput, onClick, handleSearchInputView }) => {
  const inputRef = useRef()
  const [values, handleInputChange, reset] = useForm({
    search: ''
  })
  const { search } = values
  const searchStringLength = search.length

  useEffect(() => {
    viewInput ? inputRef.current.focus() : inputRef.current.blur()
  }, [viewInput])

  const handleCleanInput = () => {
    onClick()
    reset()
  }
  const handleSearchEsc = e => {
    if (e.keyCode === 27) {
      reset()
    }
  }

  const handleClose = () => {
    reset()
  }



  return (
    <>
      <FormSearch viewInput={viewInput}>
        <SearchIcon />
        <Input
          placeholder='Buscar'
          ref={inputRef}
          name='search'
          onChange={handleInputChange}
          value={search}
          onKeyUp={handleSearchEsc}
          autoComplete='off'
        />

        {viewInput ? (
          <CloseInputButton onClick={handleCleanInput}>
            <FaTimesCircle color={colors.red} size={30} />
          </CloseInputButton>
        ) : null}
      </FormSearch>

      {searchStringLength > 0 ? (
        <Search
          searchStringLength={searchStringLength}
          searchValue={search}
          close={handleClose}
        />
      ) : null}
    </>
  )
}

const Input = styled.input`
  width: 100%;
  background: transparent;
  border: none;
  padding-left: 10px;
  outline: none;
  color: white;
  ::placeholder {
    color: ${({ theme }) => theme.colors.lighGray};
  }
  :focus-within {
    color: black;
  }

  @media ${({ theme }) => theme.devices.laptop} {
    display: flex;
  }
`
const SearchIcon = styled(FaSearch)`
  color: ${({ theme }) => theme.colors.lighGray};
  @media ${({ theme }) => theme.devices.tabletS} {
    width: 30%;
  }
`
const CloseInputButton = styled.div`
  cursor: pointer;
`
const FormSearch = styled.form`
  display: none;
  margin-left: 20px;
  background: ${({ theme }) => theme.colors.body};
  position: absolute;
  right: 0;
  left: 0;
  top: 0;
  bottom: 0;
  margin: auto;
  margin-right:10px;
  height: 3.5rem;
  width: 97%;
  padding: 30px;
  border-radius: 0.25rem;
  align-items: center;
z-index: 10;
  animation: ${FadeIn} .2s;
  transition: all 1s;
  animation-fill-mode: both;

  :focus-within {
    background-color: white;
  }


  ${({ viewInput }) =>
    viewInput &&
    css`
      display: flex;
    `}

    

    @media ${({ theme }) => theme.devices.tabletS} {
      animation: ${FadeIn} 1s;
    transition: all 0.1s;
    animation-fill-mode: both;
   display: flex;
    position: relative;
    height: 38px;
    border-radius: 5px;
    padding:0;
    width: 50%;
    max-width: 200px;
    transition: max-width .5s ease-in;}
  }
  @media ${({ theme }) => theme.devices.laptopL} {
     :focus-within {
     max-width: 230px;
     transition: max-width .5s ease;}
     
   }
  
`
