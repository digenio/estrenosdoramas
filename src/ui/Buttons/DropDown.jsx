import { useEffect, useState } from 'react'
import styled, { css } from 'styled-components'
import { CheckBox } from './Checkbox'

export const DropDown = ({
  label = 'Agregar',
  position = 'relative',
  children,
  icon,
  onClick,
  value,
  navegation,
  width = 'auto',
  season,
  iconTwo,
  player,
  movie,
  languajeSelect,
  checked,
  changeIdiom,
  visible,
  languajeCurrent
}) => {
  const [open, setOpen] = useState(false)
  const handleOpen = () => {
    changeIdiom(value)
    // setOpen(!visible)
    setOpen(!open)
  }

  useEffect(() => {
    // setOpen(visible)
  }, [languajeCurrent])

  return (
    <DropContainer
      season={season}
      label={label}
      navegation={navegation}
      value={open}
      onClick={onClick}
      position={position}
      width={width}
      onClick={onClick ? handleOpen : null}
    >
      {onClick ? (
        <CheckBox
          type='checkbox'
          id='droplist'
          name={value}
          onClick={handleOpen}
          checked={checked}
        />
      ) : null}

      <DropTop
        position={position}
        onClick={onClick}
        navegation={navegation}
        width={width}
      >
        <TitleTag
          htmlFor='droplist'
          onClick={onClick}
          width={width}
          movie={movie}
        >
          <IconMenu> {iconTwo}</IconMenu>
          <LanguajeDrop>
            {player ? languajeSelect : label}
            {icon ? <i>{icon}</i> : null}
          </LanguajeDrop>

          {player && <OptionsName>Opciones</OptionsName>}
        </TitleTag>

        <DropBotton
          value={value}
          navegation={navegation}
          onClick={onClick}
          movie={movie}
          checked={checked}
          open={open}
        >
          <ul className='subMenu'>{children}</ul>
        </DropBotton>
      </DropTop>
    </DropContainer>
  )
}

const DropTop = styled.div`
  ${({ navegation }) =>
    navegation &&
    css`
      color: ${({ theme }) => theme.colors.white};
      font-weight: 400;
      transition: color 0.3s ease;
      :hover {
        color: white;
      }
    `}

  ${({ onClick }) =>
    onClick &&
    css`
    position relative`}
`
const TitleTag = styled.label`
  font-size: 13px;

  @media ${({ theme }) => theme.devices.laptopL} {
    font-size: 14px;
  }
  ${({ onClick }) =>
    onClick &&
    css`
      cursor: pointer;
      background-color: ${({ theme }) => theme.colors.secondary};
      border-radius: 5px;
      text-transform: uppercase;
      width: ${({ width }) => `${width}px`};
      text-align: center;
      font-size: 0.6rem;
      padding: 10px;
      display: flex;
      flex-direction: column;
      justify-content: flex-start;
      @media ${({ theme }) => theme.devices.laptop} {
        font-size: 15px;
        text-transform: capitalize;
      }
    `}

  ${({ movie }) =>
    movie &&
    css`
      background-color: ${({ theme }) => theme.colors.primary};
      color: white;
    `}
`

const DropBotton = styled.div`
  ${({ movie }) =>
    movie &&
    css`
      ul {
        background-color: ${({ theme }) => theme.colors.body};
      }
    `}

ul{
  width: 100%;

  
}

  ${({ navegation }) =>
    navegation &&
    css`
      display: none;
      top: 100%;
      left: -1.15rem;
      min-width: 10rem;
      max-width: calc(100vw - 4rem);
      z-index: 11;
      border: 0;
      border-radius: 0.25rem;
      padding: 0.5rem;
      list-style: none;
      max-height: 34rem;
      overflow: auto;
      box-shadow: 0 0 4rem rgb(0 0 0 / 5%);
      @media ${({ theme }) => theme.devices.laptop} {
        display: none;
      }
    `}

  @media ${({ theme }) => theme.devices.laptop} {
  ${({ navegation }) =>
    navegation &&
    css`
      display: none;
      position: absolute;
      background-color: ${({ theme }) => theme.colors.body};
    `}
    
  }
  ${({ onClick }) =>
    onClick &&
    css`
      display: none;
      position: absolute;
      top: 100%;
      left: 0;
      margin-top: 10px;
      min-width: 7rem;
      max-width: calc(100vw - 4rem);
      z-index: 2;
      border: 0;
      border-radius: 0.25rem;
      list-style: none;
      max-height: 34rem;
      overflow: auto;
      box-shadow: 0 0 4rem rgb(0 0 0 / 5%);
      background-color: ${({ theme }) => theme.colors.white};
      ${({ open }) =>
        open &&
        css`
          display: flex;
        `}
    `}
  
`

const DropContainer = styled.div`
  position: ${({ position }) => position};
  user-select: none;
  margin-right: 10px;

  ${({ label }) =>
    label === 'Productora' &&
    css`
      @media ${({ theme }) => theme.devices.laptop} {
        display: none;
      }
      @media ${({ theme }) => theme.devices.laptopL} {
        display: initial;
      }
    `}
  ${({ navegation }) =>
    navegation &&
    css`
      :hover ${DropBotton} {
        display: block;
      }
    `}
    input {
    display: none;
  }

  ${({ onClick }) =>
    onClick &&
    css`
      top: 10px;
      right: 20px;
      border-radius: 5px;
      padding: 0;
      width: 100px;
    `}

  ${({ season }) =>
    season &&
    css`
      display: none;
    `}
`

const IconMenu = styled.span`
  margin-right: 5px;
  @media ${({ theme }) => theme.devices.laptop} {
    display: none;
  }
`

const OptionsName = styled.span`
  display: inline-block;
  text-transform: capitalize;
  font-size: 12px;
  text-align: left;
  opacity: 0.7;
`

const LanguajeDrop = styled.span`
  text-align: left;
`
