import Link from 'next/link'
import styled from 'styled-components'

export const TextLink = ({ className, children, href = '/', as }) => {
  return (
    <Link href={href} as={as} passHref>
      <a>
        <LinkItem className={className}>{children}</LinkItem>
      </a>
    </Link>
  )
}

const LinkItem = styled.li`
  font-size: 13px;
  height: 45px;
  margin: 10px 7px;
  display: flex;
  align-items: center;
  color: white;

  @media ${({ theme }) => theme.devices.laptopL} {
    font-size: 14px;
    color: white;
  }
`
