import Link from 'next/link'
import { useContext, useEffect } from 'react'
import styled, { css } from 'styled-components'

import { ServerContext } from '../../contexts/ServerContext'
export const DropItem = ({
  children,
  icon,
  href,
  as,
  buttonList,
  navegationList,
  more,
  eventClick,
  liked,
  player,
  id,
  value,
  onChange,
  languaje,
  server,
  number,
  linkOnline,
  activeLink,
  color,
  setServer
}) => {
  const { servers } = useContext(ServerContext)

  const serverAvalible = servers.filter(serv => {
    if (serv.code_flix == server) {
      return serv
    }
  })

  const [{ name: nameServer } = {}] = serverAvalible || []

  const handleLanguaje = () => {
    if (eventClick) {
      eventClick()
    }

    if (setServer) {
      setServer(activeLink)
    }
  }

  useEffect(() => {
    if (onChange) {
      onChange(value)
    }
    if (setServer) {
      setServer(activeLink)
    }
  }, [])
  const ancor = (
    <a>
      {icon ? <Icon liked={liked}>{icon}</Icon> : null}
      <Text
        color={color}
        more={more}
        player={player}
        onClick={
          onChange
            ? () => {
              onChange(linkOnline)
            }
            : () => { }
        }
      >
        {player ? (
          <OptionItem active={activeLink == linkOnline}>
            <ServerName active={activeLink == linkOnline}>
              <NumberOption>
                <p>0{number + 1}</p>
              </NumberOption>
              {nameServer}
            </ServerName>
            <LanguajeOptions>
              <span>HD</span>
            </LanguajeOptions>
          </OptionItem>
        ) : (
          children
        )}
      </Text>
    </a>
  )

  return (
    <ItemDrop
      onClick={handleLanguaje}
      buttonList={buttonList}
      navegationList={navegationList}
      player={player}
    // activeurl={activeLink == linkOnline}
    >
      {href ? (
        <Link href={href} as={as}>
          {ancor}
        </Link>
      ) : (
        ancor
      )}
    </ItemDrop>
  )
}
const Icon = styled.i`
  display: inline-block;
  color: ${({ theme }) => theme.colors.black};
  ${({ liked }) =>
    liked &&
    css`
      color: black;
    `}
`
const Text = styled.div`
  height: 100%;
  z-index: 10;
  display: block;
  /* color: black; */

  ${({ more }) =>
    more &&
    css`
      @media ${({ theme }) => theme.devices.laptop} {
        color: ${({ theme }) => theme.colors.twitter};
        text-transform: uppercase;
      }
    `};

  ${({ color }) =>
    color &&
    css`
      @media ${({ theme }) => theme.devices.laptop} {
        color: black;
      }
    `};

  :hover {
    color: white;
  }

  ${({ player }) =>
    player &&
    css`
      padding: 10px;
    `}
`

const LanguajeOptions = styled.div`
  width: 20px;
  height: 100%;
  font-size: 12px;
  margin-left: 25px;
  margin-top: 5px;
  p {
    margin-bottom: 3px;
    font-weight: 700;
  }
  span {
    color: ${({ theme }) => theme.colors.gray};
  }
`
const OptionItem = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 5px;
  ${({ active }) =>
    active &&
    css`
      background-color: ${({ theme }) => theme.colors.bodyH};
      border-radius: 5px;
    `}
`
const ServerName = styled.span`
  display: flex;
  gap: 10px;
  color: ${({ theme }) => theme.colors.black};
  font-weight: 700;
  font-size: 12px;

  ${({ active }) =>
    active &&
    css`
      color: ${({ theme }) => theme.colors.primary};
    `}
`
const NumberOption = styled.div`
  font-size: 12px;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.gray};

  :hover {
    opacity: 0.4;
  }
`
const ItemDrop = styled.li`
  position: relative;
  cursor: pointer;
 

  a {
    padding: 0;
    width: 100%;
    height: 100%;
  }

  :hover ${Icon} {
    color: white;
  }
  :hover ${ServerName} {
    /* color: white; */
  }

  :hover {
    /* background-color: ${({ theme }) => theme.colors.bodyH}; */
    color: white;
    /* padding: 5px; */
  }

  
 
  ${({ navegationList }) =>
    navegationList &&
    css`
      font-size: 0.875rem;
      padding: 0px 1rem;
      text-align: left;
      border-radius: 0.25rem;
      line-height: 2rem;
      @media ${({ theme }) => theme.devices.laptop} {
        :hover {
          background: ${({ theme }) => theme.colors.bodyH};
        }
      }
    `}

  ${({ buttonList }) =>
    buttonList &&
    css`
      padding-bottom: 10px;
      margin: 6px 0;

      a {
        display: inline-block;
        text-align: left;
        padding: 0;
        font-size: 1rem;
      }
    `}

    ${({ player }) =>
    player &&
    css`
        height: 50px;
      `}
`
