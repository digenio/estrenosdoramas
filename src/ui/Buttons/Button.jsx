import styled, { css } from 'styled-components'

import { pxToRem } from '../../styles/pxToRem'

export const Button = ({
  className,
  children,
  onClick,
  mobile,
  desktop,
  width,
  radius,
  minW,
  height,
  detailOne,
  detailTwo,
  offBackground,
  center,
  bgGreen,
  flex
}) => {
  return (
    <ButtonStyled
      className={className}
      onClick={onClick}
      mobile={mobile}
      desktop={desktop}
      width={width}
      radius={radius}
      minW={minW}
      height={height}
      detailOne={detailOne}
      detailTwo={detailTwo}
      bgGreen={bgGreen}
      offBackground={offBackground}
      center={center}
      flex={flex}
    >
      {children}
    </ButtonStyled>
  )
}

export const ButtonStyled = styled.button`
  width: ${({ width }) => (width ? `${pxToRem(width)}` : '140px')};
  min-width: ${({ minW }) => (minW ? `${pxToRem(minW)}` : 'auto')};
  border-radius: ${({ radius }) => (radius ? `${pxToRem(radius)}` : '6px')};
  text-align: center;
  height: ${({ height }) => (height ? `${pxToRem(height)}` : '30px')};
  border: none;
  cursor: pointer;
  border-radius: 6px;
  margin: 0 10px;

/* font-family: 'Asap Condensed'; */

  &:hover {
  }

  ${({ mobile }) =>
    mobile &&
    css`
      @media ${({ theme }) => theme.devices.tabletS} {
        display: none;
      }
    `}

    ${({ desktop }) =>
      desktop &&
      css`
        display: none;

        @media ${({ theme }) => theme.devices.laptop} {
          display: block;
        }
      `}

  ${({ center }) =>
    center &&
    css`
      margin: 0 auto;
    `}

    ${({ flex }) =>
      flex &&
      css`
        display: flex;
        justify-content: center;
        align-items: center;
      `}
  ${({ desktop }) => desktop && css``}


  ${({ detailOne }) =>
    detailOne &&
    css`
      background-color: ${({ theme }) => theme.colors.orange};
      color: ${({ theme }) => theme.colors.white};
      font-size: 1rem;
      margin: 0;
    `}

    ${({ offBackground, detailOne }) =>
      offBackground &&
      detailOne &&
      css`
        background-color: transparent;
        color: ${({ theme }) => theme.colors.details};
        border: 2px solid ${({ theme }) => theme.colors.details};
        font-size: 1rem;
        margin: 0;
      `}

      ${({ detailTwo }) =>
        detailTwo &&
        css`
          background-color: ${({ theme }) => theme.colors.detailSecond};
          color: ${({ theme }) => theme.colors.white};
          font-size: 1rem;
          margin: 0;
        `}
        ${({ bgGreen }) =>
          bgGreen &&
          css`
            background-color: ${({ theme }) => theme.colors.greenApple};
            color: ${({ theme }) => theme.colors.white};
            font-size: 1rem;
            margin: 0;
          `}

        ${({ offBackground, detailTwo }) =>
          offBackground &&
          detailTwo &&
          css`
            background-color: transparent;
            color: ${({ theme }) => theme.colors.greenApple};
            border: 2px solid ${({ theme }) => theme.colors.greenApple};
            font-size: 1rem;
            margin: 0;
          `}
`
