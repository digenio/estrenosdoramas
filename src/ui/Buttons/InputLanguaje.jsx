import Select, { components } from 'react-select'
import styled, { css } from 'styled-components'
import { FaCaretDown } from 'react-icons/fa'
import { useState } from 'react'

export const CountryNumberList = () => {
  const [value, setValue] = useState()
  const options = [
    { value: 'peru', label: '+51', icon: 'bandera' },
    { value: 'chile', label: '+02', icon: 'bandera' },
    { value: 'venezuela', label: '+58', icon: 'bandera' }
  ]
  const onDrowDownChange = value => {
    setValue(value)
  }

  const Placeholder = props => {
    return <components.Placeholder {...props} />
  }
  const DropdownIndicator = props => {
    return (
      <components.DropdownIndicator {...props}>
        <FaCaretDown color={'black'} />
        {props.label}
      </components.DropdownIndicator>
    )
  }

  const IconOptions = props => {
    return (
      <components.Option {...props}>
        <Icon>
          <img src='/venezuela.png' alt='' />
        </Icon>
        <ValueContainerCustom> {props.label}</ValueContainerCustom>
      </components.Option>
    )
  }

  const Value = props => {
    return (
      <components.SingleValue {...props}>
        {' '}
        <Icon>
          <img src='/venezuela.png' alt='' />
        </Icon>
        {props.data.label}
      </components.SingleValue>
    )
  }

  return (
    <WrapperSelect>
      <span>Pais</span>
      <SelectList
        value={value}
        classNamePrefix='react-select'
        defaultValue={options[0]}
        components={{
          Placeholder,
          DropdownIndicator,
          Option: IconOptions,
          SingleValue: Value
        }}
        options={options}
        onChange={onDrowDownChange}
      />
    </WrapperSelect>
  )
}

const WrapperSelect = styled.div`
  width: 100%;
  max-width: 100px;
  color: ${({ theme }) => theme.colors.darkBlue};
  span {
    font-size: 12px;
    @media ${({ theme }) => theme.devices.tablet} {
      font-size: 1rem;
    }
  }
`
const SelectList = styled(Select)`
  .react-select {
  }

  .react-select__control {
    border: none;
    box-shadow: none;
    :hover {
      border: red;
      box-shadow: red;
    }
  }
  .react-select__value-container {
    outline: none;
  }
  .react-select__indicators {
    cursor: pointer;
  }
  .react-select__indicator-separator {
    display: none;
  }

  .react-select__menu {
  }
  .react-select__menu-list {
  }
  .react-select__option {
  }
`
// ${({ theme }) => theme.colors.lighGray};

const Icon = styled.figure`
  width: 20px;
  display: inline-block;
  margin-right: 3px;
`

const ValueContainerCustom = styled.span`
  display: inline-block;
`
