import styled from 'styled-components'

export const FilterNotFound = () => {
  return (
    <NotFound>
      <ImageContainer>
        <img src='/notFound.svg' alt='notFount' />
      </ImageContainer>
      <h2>No se encontraron coincidencias</h2>
    </NotFound>
  )
}

const NotFound = styled.article`
  background-color: ${({ theme }) => theme.colors.bodyH};
  border-radius: 5px;
  padding: 40px 40px;
  width: 100%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  h2 {
    color: ${({ theme }) => theme.colors.black};
    font-size: 1.5rem;
    margin-top: 20px;
  }
  a {
    color: ${({ theme }) => theme.colors.white};
    font-size: 1.2rem;
    margin-top: 20px;
    background-color: ${({ theme }) => theme.colors.details};
    padding: 20px;
    border-radius: 5px;
    cursor: pointer;
    :hover {
      background-color: #874ee4;
    }
  }
`

const ImageContainer = styled.figure`
  width: 300px;

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`
