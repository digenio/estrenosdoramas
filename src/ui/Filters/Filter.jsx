import styled, { css } from 'styled-components'
import { FilterNavLink } from '../Filters/FilterNavLink'
import { MdArrowDropDown } from 'react-icons/md'
import config from '../../utils/config'

export function Filter ({
  active,
  sort,
  name,
  icon,
  children,
  toggle,
  visible,
  changeSort
}) {
  return (
    <FilterItem>
      <FilterIcon>{icon}</FilterIcon>
      <FilterSpan active={active}>{name}</FilterSpan>
      <FilterChoise onClick={toggle}>
        <FilterIconDown>
          <MdArrowDropDown size={30} color={active ? config.color : null} />
        </FilterIconDown>
      </FilterChoise>
      <FilterNav visible={visible}>
        {sort ? (
          <SortList>
            <FilterNavLink
              active={sort === 'CREATEDAT_DESC'}
              onClick={() => changeSort('CREATEDAT_DESC')}
            >
              Últimas
            </FilterNavLink>
            <FilterNavLink
              active={sort === 'POPULARITY_DESC'}
              onClick={() => changeSort('POPULARITY_DESC')}
            >
              Populares
            </FilterNavLink>
            <FilterNavLink
              active={sort === 'VOTE_DESC'}
              onClick={() => changeSort('VOTE_DESC')}
            >
              Vistas
            </FilterNavLink>
          </SortList>
        ) : (
          <FilterNavList>{children}</FilterNavList>
        )}
      </FilterNav>
    </FilterItem>
  )
}

const FilterIcon = styled.div`
  position: relative;
  display: inline-block;
  color: ${({ theme }) => theme.colors.black};
`

const FilterSpan = styled.span`
  display: inline-block;
  vertical-align: top;
  position: relative;
  margin-left: 10px;
  z-index: 1;
  padding-right: 0.3rem;
  transition: color 0.3s ease;
  color: ${({ theme }) => theme.colors.black};
  ${({ active }) =>
    active &&
    css`
      color: ${({ theme }) => theme.colors.details};
    `}
`

const FilterChoise = styled.a`
  display: inline-block;
  cursor: pointer;
  color: ${({ theme }) => theme.colors.black};

  ${({ active }) =>
    active &&
    css`
      color: ${({ theme }) => theme.colors.details};
    `}
`

const FilterIconDown = styled.div`
  position: relative;
  display: inline-block;

  color: ${({ theme }) => theme.colors.black};
`

const FilterNav = styled.aside`
  position: absolute;
  top: 100%;
  left: 0;
  right: 0;
  z-index: -1;
  transition: 0.2s;
  max-height: 400px;
  overflow: hidden;
  opacity: 0;
  border-top: 2px solid ${({ theme }) => theme.colors.bodyH};
  box-shadow: 0 5px 25px black;
  background-color: ${({ theme }) => theme.colors.body};
  margin: 0;
  padding: 0;
  display: block;
  list-style-type: none;
  height: 0;
  border-radius: 5px;

  @media ${({ theme }) => theme.devices.tablet} {
    width: 15rem;
    overflow: hidden;
  }

  @media ${({ theme }) => theme.devices.tabletMax} {
    ${({ visible }) =>
      visible &&
      css`
        max-height: 300px;
        height: auto;
        z-index: 10;
        opacity: 1;
        padding-bottom: 1rem;
        overflow: auto;
      `}
  }
`

const FilterNavList = styled.ul`
  margin: 0;
  padding: 0;
  list-style-type: none;
  display: flex;
  flex-wrap: wrap;
  padding: 10px;
`
const FilterItem = styled.div`
  display: flex;
  align-items: center;
  text-align: left;
  position: relative;
  justify-content: space-between;
  /* padding: 0.3rem 1.6rem; */
  padding: 10px 10px;
  margin-right: 0.5rem;
  font-size: 14px;
  @media ${({ theme }) => theme.devices.mobileL} {
    justify-content: flex-start;
  }
  @media ${({ theme }) => theme.devices.laptop} {
    &:hover {
      cursor: pointer;
      aside {
        max-height: 300px;
        height: auto;
        z-index: 10;
        opacity: 1;
        /* padding-bottom: 1rem; */
        overflow-y: scroll;
      }
    }
  }
`

const SortList = styled.div`
  padding: 10px;
  a {
    margin: 10px 0;
  }
`
