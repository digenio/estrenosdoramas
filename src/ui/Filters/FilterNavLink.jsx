import styled, { css } from 'styled-components'

export const FilterNavLink = ({ children, onClick, active }) => {
  return (
    <Link onClick={onClick} active={active}>
      <FilterNavText active={active}>{children}</FilterNavText>
    </Link>
  )
}

const Link = styled.a`
  display: block;
  cursor: pointer;
`

const FilterNavText = styled.span`
  display: inline-block;
  font-size: 1rem;
  margin-left: 0.6rem;
  max-width: 7rem;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  max-height: 2rem;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
  color: ${({ theme }) => theme.colors.lighGray};
  ${({ active }) =>
    active &&
    css`
      color: ${({ theme }) => theme.colors.black};
    `}
`
