import styled from 'styled-components'
import { CheckBox } from '../../ui/Buttons/Checkbox'

export const FilterItem = ({ value, name, onChange, filterCheck = [] }) => {
  return (
    <FilterNavItem>
      <CheckBox
        value={value}
        checked={!!filterCheck.find(item => item === value)}
        onChange={onChange}
      />
      <FilterNavText>{name}</FilterNavText>
    </FilterNavItem>
  )
}

const FilterNavItem = styled.li`
  position: relative;
  margin-top: 10px;
  margin-bottom: 10px;

  /* padding: 0.5rem 1.2rem; */
  width: 100%;
  display: flex;
  align-items: center;
`

const FilterNavText = styled.span`
  display: inline-block;
  font-size: 1rem;
  margin-left: 0.6rem;
  max-width: 7rem;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  max-height: 2rem;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
  color: ${({ theme }) => theme.colors.lighGray};
`
