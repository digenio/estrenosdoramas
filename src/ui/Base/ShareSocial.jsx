import styled from 'styled-components'

import {
  FacebookIcon,
  FacebookShareButton,
  TelegramIcon,
  TelegramShareButton,
  TwitterIcon,
  TwitterShareButton,
  WhatsappIcon,
  WhatsappShareButton
} from 'react-share'

export const ShareSocial = ({ name, url }) => {
  const title = `Estoy viendo ${name} en DoramasFlix, ¿Te animás a verlo conmigo?`
  return (
    <ShareSocialWrapper>
      <FacebookShareButton size={25} url={url} quote={title}>
        <FacebookIcon size={25} round />
      </FacebookShareButton>
      <TwitterShareButton url={url} title={title}>
        <TwitterIcon size={25} round />
      </TwitterShareButton>
      <TelegramShareButton url={url} title={title}>
        <TelegramIcon size={25} round />
      </TelegramShareButton>
      <WhatsappShareButton url={url} title={title} separator=':: '>
        <WhatsappIcon size={25} round />
      </WhatsappShareButton>
    </ShareSocialWrapper>
  )
}
const ShareSocialWrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 10px;
`

const Facebook = styled(FacebookIcon)`
  overflow: hidden;
  :hover {
    background-color: ${({ theme }) => theme.colors.facebook};
  }

  padding: 5px;
  border-radius: 100%;
  cursor: pointer;
`
const Twitter = styled.div`
  cursor: pointer;
  :hover {
    background-color: ${({ theme }) => theme.colors.twitter};
  }

  padding: 5px;
  border-radius: 100%;
`
const Telegram = styled.div`
  cursor: pointer;
  :hover {
    background-color: ${({ theme }) => theme.colors.twitter};
  }

  padding: 5px;
  border-radius: 100%;
`
