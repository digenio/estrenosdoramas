import { useContext } from 'react'
import styled, { css } from 'styled-components'

import { GenreContext } from '../../../contexts/GenreContext'
import { Categorie } from './Categorie'
import { CategorieLoader } from '../../Loaders/CategorieLoader'

export const Categories = ({ slug, slugIverse, slugSlug }) => {
  const { genres = [] } = useContext(GenreContext)

  if (genres.length == 0) {
    return <CategorieLoader slug={slug} slugIverse={slugIverse} />
  }

  return (
    <CategorieWrapper slug={slug} slugIverse={slugIverse} slugSlug={slugSlug}>
      <CategorieTitle>
        <p>Categorias</p>
        <div></div>
      </CategorieTitle>
      <CategoriesGroup slug={slug} slugSlug={slugSlug}>
        <CategoriesList>
          {genres.map((caregorie, idx) => (
            <Categorie
              key={idx}
              caregorie={caregorie}
              as={`/generos/${caregorie.slug}`}
              href={`/generos/[slug]`}
            />
          ))}
        </CategoriesList>
      </CategoriesGroup>
    </CategorieWrapper>
  )
}

const CategorieWrapper = styled.div`
  position: relative;
  width: 100%;
  border-radius: 5px;
  margin-bottom: 1.25rem;
  padding: 1.25rem;

  /* z-index: -1; */
  background-color: ${({ theme }) => theme.colors.body};
  font-size: 0.875rem;
  margin-top: 10px;
  /* max-height: 390px; */
  @media ${({ theme }) => theme.devices.laptop} {
    max-width: 340px;
    grid-column: 1;
    grid-row: 2/3;
    ${({ slug }) =>
    slug &&
    css`
        max-height: 600px;
        grid-row: 1/2;
      `}

    ${({ slugIverse }) =>
    slugIverse &&
    css`
        grid-column: 2;
        grid-row: 1;
      `}

      ${({ slugSlug }) =>
    slugSlug &&
    css`
          grid-row: 2/3;
          max-height: 600px;
        `}

        ${({ slugSlug }) =>
    slugSlug &&
    css`
            max-height: 600px;
          `}
  }
`
const CategorieTitle = styled.div`
  div {
    margin-top: 15px;
    position: relative;
    :before {
      border-radius: 3px;
      content: '';
      background-color: gray;
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 1px;
      opacity: 0.2;
      z-index: -1;
    }

    :after {
      position: absolute;
      height: 3px;
      width: 40px;
      left: 0;
      bottom: 0;
      content: '';
      background-color: ${({ theme }) => theme.colors.secondary};
    }
  }
`

const CategoriesGroup = styled.div`
  @media ${({ theme }) => theme.devices.laptop} {
    max-height: 330px;
    overflow-y: scroll;
    padding-right: 20px;

    ${({ slug }) =>
    slug &&
    css`
        max-height: 400px;
      `}

    ${({ slugSlug }) =>
    slugSlug &&
    css`
        max-height: 500px;
      `}
  }
`

const CategoriesList = styled.ul`
  margin: 27px 0px;
  display: grid;
  grid-template-columns: 1fr 1fr;
  column-gap: 5px;

  @media ${({ theme }) => theme.devices.tablet} {
    grid-template-columns: 1fr 1fr 1fr;
  }

  @media ${({ theme }) => theme.devices.laptop} {
    display: block;
  }
`
