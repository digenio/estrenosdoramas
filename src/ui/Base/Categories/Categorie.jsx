import styled, { css } from 'styled-components'
import Link from 'next/link'
export const Categorie = ({ caregorie, href, as }) => {
  return (
    <Link href={href} as={as}>
      <a>
        <CategorieItem>{caregorie.name}</CategorieItem>
      </a>
    </Link>
  )
}

const CategorieItem = styled.li`
  background-color: ${({ theme }) => theme.colors.white};
  padding: 0 1rem;
  width: 100%;
  margin-right: 5px;
  margin-bottom: 0.5rem;
  height: 30px;
  font-size: 0.625rem;
  border-radius: 15px;
  line-height: 30px;
  transition: 0.2s;
  cursor: pointer;
  :hover {
    background-color: ${({ theme }) => theme.colors.primary};
    color: white;
  }
`
