import Img from 'react-cool-img'
import styled, { css } from 'styled-components'
import { URL_PEOPLE } from '../../utils/urls'
import { BiDotsHorizontalRounded } from 'react-icons/bi'

export const CastList = ({ cast }) => {
  return (
    <ListCast>
      {cast.slice(0, 4).map((item, idx) => (
        <Users key={idx}>
          <Img
            error={'./user.png'}
            src={`${URL_PEOPLE}${item.profile_path}`}
          ></Img>
        </Users>
      ))}

      <Detail>
        <BiDotsHorizontalRounded />
      </Detail>
    </ListCast>
  )
}

const ListCast = styled.div`
  /* width: 100px; */
  height: auto;
  display: flex;
  gap: 10px;
  /* justify-content: center; */
  align-items: center;
  margin: 20px 0;
`
const Users = styled.figure`
  cursor: pointer;
  width: 30px;
  height: 30px;
  img {
    width: 100%;
    height: 100%;
    border-radius: 100%;
  }
  @media ${({ theme }) => theme.devices.laptop} {
    width: 50px;
    height: 50px;
  }
`
const Detail = styled.div`
  font-size: 15px;
  border: 1px solid ${({ theme }) => theme.colors.secondary};
  border-radius: 100%;
  padding: 4px;
  color: ${({ theme }) => theme.colors.secondary};
  cursor: pointer;
  @media ${({ theme }) => theme.devices.laptop} {
    font-size: 20px;
    padding: 10px;
  }
`
