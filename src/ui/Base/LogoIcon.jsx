import Image from 'next/image'
import Link from 'next/link'
import styled, { css } from 'styled-components'

export const LogoIcon = ({ width = 200, height = 40, grayscale, mobile }) => {
  return (
    <Link href='/'>
      <a>
        <Logo mobile={mobile}>
          <ImageLogo
            width={width}
            height={height}
            src='/logoApp.png'
            alt={'logoicon'}
            grayscale={grayscale}
          />
        </Logo>
      </a>
    </Link>
  )
}

const Logo = styled.figure`
  cursor: pointer;
  max-width: 200px;
  min-width: 90px;
  ${({ mobile }) =>
    mobile &&
    css`
      @media ${({ theme }) => theme.devices.laptop} {
        display: none;
      }
    `}
`

const ImageLogo = styled(Image)`
  ${({ grayscale }) =>
    grayscale &&
    css`
      filter: grayscale(100%);
    `}
`
