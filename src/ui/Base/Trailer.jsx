import styled from 'styled-components'

export const Trailer = ({ link }) => {
  return (
    <TrailerView
      src={`https://www.youtube.com/embed/${link}`}
      allowFullScreen
    ></TrailerView>
  )
}
const TrailerView = styled.iframe`
  border: none;
  width: 100%;
  height: 500px;
  border-radius: 5px;
  margin-top: 10px;
`
