import styled, { css } from 'styled-components'

export const ErrorLoad = ({ recharge }) => {
  return (
    <ErrorLoader>
      <ErrorImageLoad>
        <img src='/bugError.svg' alt='error' />
      </ErrorImageLoad>
      <h2>Ocurrió un error</h2>
      <a onClick={recharge}>Recargar</a>
    </ErrorLoader>
  )
}

const ErrorLoader = styled.article`
  background-color: ${({ theme }) => theme.colors.bodyH};
  border-radius: 5px;
  padding: 40px 40px;
  width: 100%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 100px;
  @media ${({ theme }) => theme.devices.laptop} {
    grid-column: 1/-1;
  }

  h2 {
    color: ${({ theme }) => theme.colors.black};
    font-size: 1.5rem;
    margin-top: 20px;
  }
  a {
    color: ${({ theme }) => theme.colors.white};
    font-size: 1.2rem;
    margin-top: 20px;
    background-color: ${({ theme }) => theme.colors.primaryHover};
    padding: 20px;
    border-radius: 5px;
    cursor: pointer;
    :hover {
      background-color: ${({ theme }) => theme.colors.primary};
    }
  }
`

const ErrorImageLoad = styled.figure`
  width: 300px;

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`
