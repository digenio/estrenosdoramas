import Link from 'next/link'
import styled from 'styled-components'
import { Button } from '../Buttons/Button'

export const Alphabet = () => {
  let alphabet = String.fromCharCode(...Array(123).keys()).slice(97)
  let arrAlphabet = Array.from(alphabet)

  return (
    <AlphabetContainer>
      <ButtonAlphabet>#</ButtonAlphabet>
      {arrAlphabet.map((letter, key) => (
        <Link key={key} href='/letras/[letter]' as={`/letras/${letter}`}>
          <a>
            <ButtonAlphabet>{letter}</ButtonAlphabet>
          </a>
        </Link>
      ))}
    </AlphabetContainer>
  )
}

const AlphabetContainer = styled.div`
  width: 100%;
  margin-top: 10px;
  white-space: nowrap;
  position: relative;
  overflow-x: scroll;
  ::-webkit-scrollbar {
    display: none;
  }
  /* margin: 5px 0; */

  @media ${({ theme }) => theme.devices.laptop} {
    /* grid-column: span 1; */
  }
`

const ButtonAlphabet = styled(Button)`
  width: 10px;
  text-transform: uppercase;
  background-color: ${({ theme }) => theme.colors.body};
  color: ${({ theme }) => theme.colors.gray};
  opacity: 0.6;
  font-weight: 700;
  width: 40px;
  height: 30px;
  margin: 0 3px;
  font-size: 14px;
`
