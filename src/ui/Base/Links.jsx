import { useContext, useEffect, useState } from 'react'
import styled from 'styled-components'
import { LanguageContext } from '../../contexts/LanguageContext'
import { LinkItem } from '../../ui/Base/LinkItem'

export const Links = ({
  options = [],
  setShowPlayer,
  serverActive,
  languajeCurrent
}) => {
  const { languages: lang } = useContext(LanguageContext)
  const [viewLangueaje, setViewLanguaje] = useState()
  const optionOnline =
    options.filter(link => link.lang == languajeCurrent) || []

  useEffect(() => {
    lang.filter(lang => {
      if (lang.code_flix === languajeCurrent) {
        setViewLanguaje(lang.name)
      }
    })
  }, [languajeCurrent])

  return (
    <LinksContent>
      <CategorieTitle>
        <p>Links</p>
        <div></div>
      </CategorieTitle>

      {optionOnline.map((item, idx) => (
        <LinkItem
          id={idx}
          key={idx}
          {...item}
          nameLanguaje={viewLangueaje}
          setShowPlayer={setShowPlayer}
          serverActive={serverActive}
        />
      ))}
    </LinksContent>
  )
}

const LinksContent = styled.div`
  position: relative;
  width: 100%;
  border-radius: 5px;
  margin-bottom: 1.25rem;
  padding: 1.25rem;

  z-index: 0;
  background-color: ${({ theme }) => theme.colors.body};
  font-size: 0.875rem;
  margin-top: 10px;
  grid-column: 2/-1;
`
const CategorieTitle = styled.div`
  margin-bottom: 30px;
  div {
    margin-top: 15px;
    position: relative;
    :before {
      border-radius: 3px;
      content: '';
      background-color: gray;
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 1px;
      opacity: 0.2;
      z-index: -1;
    }

    :after {
      position: absolute;
      height: 3px;
      width: 40px;
      left: 0;
      bottom: 0;
      content: '';
      background-color: ${({ theme }) => theme.colors.secondary};
    }
  }
`
