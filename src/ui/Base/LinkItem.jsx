import styled, { css } from 'styled-components'
import { FaYoutube } from 'react-icons/fa'
import { useContext } from 'react'
import { ServerContext } from '../../contexts/ServerContext'
export const LinkItem = ({
  nameLanguaje,
  server,
  id,
  link,
  setShowPlayer,
  serverActive
}) => {
  const { servers } = useContext(ServerContext)

  const serverAvalible = servers.filter(serv => {
    if (serv.code_flix == server) {
      return serv
    }
  })

  const [{ name: nameServer } = {}] = serverAvalible || []

  return (
    <ItemLink
      onClick={() => {
        setShowPlayer(link, server)
      }}
      active={serverActive == server}
    >
      <LinkCount active={serverActive == server}>0{id + 1}</LinkCount>
      <LinkServer active={serverActive == server}>
        <ServerOption active={serverActive == server}>0{id + 1}</ServerOption>
        <Server>
          <FaYoutube size={20} />
          <ServerItem>{nameServer}</ServerItem>
        </Server>
        <span>HD</span>
        <span>{nameLanguaje}</span>
      </LinkServer>
    </ItemLink>
  )
}

const LinkCount = styled.p`
  font-weight: 700;
  font-size: 1rem;
  white-space: nowrap;
  margin-bottom: 15px;
  ${({ active }) =>
    active &&
    css`
      color: white;
    `}
  @media ${({ theme }) => theme.devices.laptop} {
    display: none;
  }
`

const LinkServer = styled.div`
  display: flex;

  @media ${({ theme }) => theme.devices.laptop} {
    display: grid;
    grid-template-columns: 30px 1fr 1fr 1fr;
  }
  gap: 20px;
  span {
    color: ${({ theme }) => theme.colors.gray};
  }
  ${({ active }) =>
    active &&
    css`
      span {
        color: white;
      }
    `}
`
const Server = styled.div``

const ServerOption = styled.p`
  font-weight: 700;
  font-size: 1rem;
  white-space: nowrap;
  display: none;

  color: ${({ theme }) => theme.colors.gray};

  @media ${({ theme }) => theme.devices.laptop} {
    display: inline-block;
    margin-right: 5px;
  }

  ${({ active }) =>
    active &&
    css`
      color: white;
    `}
`
const ServerItem = styled.span`
  margin-left: 5px;
  display: inline-block;
  color: ${({ theme }) => theme.colors.black};
`
const ItemLink = styled.div`
  display: flex;
  flex-direction: column;
  margin: 10px 0;
  background-color: ${({ theme }) => theme.colors.white};
  margin-bottom: 10px;
  border-radius: 3px 0 0 3px;
  padding: 10px;
  color: ${({ theme }) => theme.colors.gray};
  cursor: pointer;

  @media ${({ theme }) => theme.devices.laptop} {
    background-color: transparent;
    ${({ active }) =>
      active &&
      css`
        background-color: rgba(156, 39, 176, 0.5);
        color: white;
      `}
  }

  :hover {
    background-color: rgba(156, 39, 176, 0.5);
    color: white;
  }
  :hover ${LinkServer} span {
    color: white;
  }
  :hover ${ServerOption} {
    color: white;
  }
`
