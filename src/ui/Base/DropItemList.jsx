import styled from 'styled-components'
import Img from 'react-cool-img'
import Link from 'next/link'
import { URL_IMAGE_BACK_DROPS_1X } from '../../utils/urls'

export const DropItemList = ({ name, slug, imagen, path }) => {
  return (
    <Link href={`${path}/${slug}`}>
      <a>
        <Genero>
          <TitleGenero>{name}</TitleGenero>
          <Image>
            <Img src={`${URL_IMAGE_BACK_DROPS_1X}${imagen}`} />
          </Image>
          )
        </Genero>
      </a>
    </Link>
  )
}

const Genero = styled.article`
  position: relative;
  cursor: pointer;
  max-height: 135px;
  overflow: hidden;

  :before {
    content: '';
    background: linear-gradient(rgba(0, 0, 0, 0) 0%, #000 100%);
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    max-height: 140px;
  }
  :hover {
    box-shadow: inset 0 0 10rem rgb(10 10 10 / 80%),
      0 0 2rem rgb(10 10 10 / 50%);
  }
`

const TitleGenero = styled.h1`
  position: absolute;
  top: 70%;
  color: white;
  font-size: 0.9rem;

  margin-left: 10%;
  @media ${({ theme }) => theme.devices.tablet} {
    font-size: 1.2rem;
  }
  @media ${({ theme }) => theme.devices.laptop} {
    font-size: 1.5rem;
  }
`

const Image = styled.figure`
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    border-radius: 5px;
  }
`
