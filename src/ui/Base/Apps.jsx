import styled from 'styled-components'
import { DiAndroid } from 'react-icons/di'

export const Apps = () => {
  return (
    <AppMobile>
      <a
        target='_blank'
        rel='nofollow noopener noreferrer'
        href='https://play.google.com/store/apps/details?id=com.gratis.estrenosdoramas&hl=es'
      >
        <span>
          <DiAndroid />
        </span>
        <CallToAction>Descarga la App</CallToAction>
      </a>
    </AppMobile>
  )
}

const AppMobile = styled.div`
  display: none;
  a {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  @media ${({ theme }) => theme.devices.laptop} {
    display: flex;
    margin-left: 10px;
    padding-right: 10px;
    background-color: ${({ theme }) => theme.colors.orange};
    border-radius: 10px;
    width: 100%;
    margin-right: 20px;
    max-width: 170px;

    cursor: pointer;
    text-transform: uppercase;
    span {
      color: white;
      font-size: 20px;
      margin: 10px;
    }
  }
`

const CallToAction = styled.div`
  font-size: 12px;
  color: white;
`
