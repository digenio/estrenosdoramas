import styled, { css } from 'styled-components'
import { FaBirthdayCake } from 'react-icons/fa'
import { GiStumpRegrowth } from 'react-icons/gi'
import Img from 'react-cool-img'

import { colors } from '../../styles/theme'
import { URL_PEOPLE } from '../../utils/urls'

export const DetailActorView = ({
  actorView,
  name = 'Jesus Hernandez',
  location = 'Caracas,Venezuela',
  born = '09 Noviembre',
  img,
  profile_path
}) => {
  return (
    <Actor actorView={actorView}>
      <figure>
        <Img
          src={profile_path ? `${URL_PEOPLE}${profile_path}` : `/user.png`}
          alt={name}
        />
      </figure>
      <DetailDate actorView={actorView}>
        {actorView ? (
          <>
            <TitleActor>{name}</TitleActor>
            <span>
              <abbr title='Cumpleaños'>
                <i>
                  <FaBirthdayCake color={colors.lighGray} size={20} />
                </i>
              </abbr>

              {born}
            </span>
            <span>
              <i>
                <GiStumpRegrowth color={colors.lighGray} size={20} />
              </i>
              {location}
            </span>
          </>
        ) : (
          <span>{name}</span>
        )}
      </DetailDate>
    </Actor>
  )
}

const Actor = styled.div`
  flex-direction: column;
  width: 100px;
  cursor: pointer;
  justify-content: center;
  align-items: center;
  position: relative;
  line-height: 1.25rem;
  ${({ actorView }) =>
    actorView &&
    css`
      flex-direction: column;
    `}

  ${({ actorView }) =>
    actorView &&
    css`
      width: 90%;
      margin: 0 auto;
      @media ${({ theme }) => theme.devices.tablet} {
        width: 100%;
      }
      background-color: ${({ theme }) => theme.colors.secondary};
      padding: 20px;
      border-radius: 5px;
    `}

  figure {
    position: relative;
    border-radius: 0.25rem;

    width: 48px;
    height: 48px;
    overflow: hidden;
    margin: 7px auto;
    ${({ actorView }) =>
      actorView &&
      css`
        margin-right: 0;
        @media ${({ theme }) => theme.devices.tablet} {
          margin-right: 1rem;
        }
      `}

    ${({ actorView }) =>
      actorView &&
      css`
        width: 150px;
        height: 150px;
        border-radius: 100%;
      `}
  }
`

const DetailDate = styled.div`
  flex-grow: 1;
  flex-basis: 0;
  color: white;
  i {
    margin-right: 10px;
  }

  span {
    display: block;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    margin-bottom: 10px;

    ${({ actorView }) =>
      actorView &&
      css`
        white-space: initial;
      `}

    :after {
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      content: '';
      z-index: 2;
    }
  }
`

const TitleActor = styled.h1`
  font-size: 1.5rem;
  margin-top: 20px;
  margin-bottom: 10px;
  @media ${({ theme }) => theme.devices.tablet} {
    font-size: 2rem;
  }
`
