import styled from 'styled-components'
import Img from 'react-cool-img'
import Link from 'next/link'
import { getDateByMonthName } from '../../utils/functions'
import { getImageUrlEpisode } from '../../utils/functions'
import { SeenEpisode } from '../../components/Actions/SeenEpisode'

export const DetailSeriesChapter = ({
  _id,
  episode_number,
  href = '/',
  as,
  season_number,
  still_path,
  backdrop,
  air_date,
  listSeen,
  refetch,
  serie_id
}) => {
  const date = getDateByMonthName(air_date)

  const url = getImageUrlEpisode(still_path, backdrop)

  return (
    <Chapter>
      <Link href={href} as={as || href}>
        <LinkImage>
          <ChapterImage>
            <Img
              error={'/doramasError.jpg'}
              src={url ? url : null}
              alt={`Capitulo ${episode_number}`}
            />
          </ChapterImage>
        </LinkImage>
      </Link>

      <header>
        <Link href={href} as={as || href}>
          <a>
            <div>
              <ChapterTitle>Capitulo {episode_number}</ChapterTitle>
              <ChapterDescription>
                {season_number} - {episode_number}
              </ChapterDescription>
              <ChapterDate>{date}</ChapterDate>
            </div>
          </a>
        </Link>
        <EyeView>
          <SeenEpisode
            _id={_id}
            listSeen={listSeen}
            reload={refetch}
            serie_id={serie_id}
          />
        </EyeView>
      </header>
    </Chapter>
  )
}

const ChapterTitle = styled.h2`
  font-size: 0.8rem;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  margin-bottom: 0.5rem;
  @media ${({ theme }) => theme.devices.mobileL} {
    font-size: 1rem;
  }
`

const LinkImage = styled.a`
  position: relative;
  width: 50%;
  overflow: hidden;
  padding: 0.3rem;

  border-radius: 5px;

  @media ${({ theme }) => theme.devices.mobileM} {
    width: 40%;
  }
  @media ${({ theme }) => theme.devices.mobileL} {
    width: 30%;
  }
  @media ${({ theme }) => theme.devices.tabletS} {
    width: 24%;
  }
  @media ${({ theme }) => theme.devices.tablet} {
    width: 20%;
  }
  @media ${({ theme }) => theme.devices.laptop} {
    width: 30%;
  }
  @media ${({ theme }) => theme.devices.laptopL} {
    width: 25%;
  }
`

const ChapterImage = styled.figure`
  position: relative;
  align-self: flex-start;
  position: relative;
  margin-right: 1rem;
  height: 100%;
  width: 100%;
  :before {
    content: '';
    display: block;
    padding-top: 55%;
  }
  img {
    position: absolute;
    border-radius: 0.25rem;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`
const ChapterDescription = styled.span`
  display: inline-block;
  border-radius: 0.25rem;
  vertical-align: top;
  padding-right: 0.5rem;
  padding-left: 0.5rem;
  margin-right: 0.5rem;
  color: ${({ theme }) => theme.colors.white};
  background-color: ${({ theme }) => theme.colors.primary};
  line-height: 1.5rem;
  font-size: 0.7rem;
  @media ${({ theme }) => theme.devices.mobileL} {
    font-size: 0.9rem;
  }
`

const ChapterDate = styled.span`
  display: block;
  opacity: 0.5;
  font-size: 0.6rem;
  vertical-align: top;
  padding-right: 0.5rem;
  padding-left: 0.5rem;
  line-height: 1.5rem;
  @media ${({ theme }) => theme.devices.mobileL} {
    font-size: 0.8rem;
  }
`

const EyeView = styled.div`
  width: 60px;
`
const Chapter = styled.article`
  display: flex;
  border-radius: 0.25rem;
  margin-bottom: 0.5rem;
  max-width: 100%;
  background-color: ${({ theme }) => theme.colors.body};

  :hover {
    background-color: #f29805b3;
    color: white;
  }

  :hover ${ChapterDescription} {
    background-color: white;
    color: black;
  }

  a {
    display: flex;
    color: ${({ theme }) => theme.colors.black};
    align-items: center;
    cursor: pointer;
    :hover {
      color: white;
      z-index: 1;
    }

    figure {
      position: relative;
      align-self: flex-start;
      position: relative;
      margin-right: 1rem;

      :before {
        content: '';
        display: block;
        padding-top: 55%;
      }
      img {
        position: absolute;
        border-radius: 0.25rem;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
      }
    }
  }
  header {
    flex-grow: 1;
    flex-basis: 0;
    line-height: 1.5rem;
    display: flex;
    justify-content: space-between;
    align-items: center;
    a {
      padding: 0.5rem;
      width: 90%;
    }
  }
`
