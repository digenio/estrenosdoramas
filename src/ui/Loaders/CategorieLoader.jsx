import styled, { keyframes, css } from 'styled-components'

export const CategorieLoader = ({ slug, slugIverse }) => {
  return (
    <CategorieWrapper slug={slug} slugIverse={slugIverse}>
      <CategorieTitle>
        <p>Categorias</p>
        <div></div>
      </CategorieTitle>
      <CategoriesGroup slug={slug}>
        <CategoriesList>
          <CategorieItem />
          <CategorieItem />
          <CategorieItem />
          <CategorieItem />
          <CategorieItem />
          <CategorieItem />
        </CategoriesList>
      </CategoriesGroup>
    </CategorieWrapper>
  )
}
const shimmer = keyframes`
from{
background-position: -450px 0;
}

to{background-position: 450px 0;}
`
const CategorieItem = styled.li`
  position: relative;
  background-color: ${({ theme }) => theme.colors.white};
  padding: 0 1rem;
  width: 100%;
  margin-right: 5px;
  margin-bottom: 0.5rem;
  height: 30px;
  font-size: 0.625rem;
  border-radius: 15px;
  line-height: 30px;
  transition: 0.2s;
  cursor: pointer;

  :before {
    content: ' ';
    position: absolute;
    margin-right: 5px;
    margin-bottom: 0.5rem;
    width: 100%;
    height: 30px;
    padding: 0 1rem;
    top: 0;
    left: 0;

    object-fit: cover;
    border-radius: inherit;
    background-image: linear-gradient(
      90deg,
      rgba(0, 0, 0, 0) 0%,
      rgba(109, 110, 113, 0.2) 50%,
      rgba(93, 94, 96, 0.7) 100%
    );
    background-size: 450px 450px;
    background-repeat: no-repeat;
    z-index: 1;
    animation: ${shimmer} 1s linear infinite;
    animation-delay: 0.2s;
  }
`

const CategorieWrapper = styled.div`
  position: relative;
  width: 100%;
  border-radius: 5px;
  margin-bottom: 1.25rem;
  padding: 1.25rem;

  z-index: 0;
  background-color: ${({ theme }) => theme.colors.body};
  font-size: 0.875rem;
  margin-top: 10px;
  max-height: 390px;
  @media ${({ theme }) => theme.devices.laptop} {
    max-width: 340px;
    grid-column: 1;
    grid-row: 2/3;
    ${({ slug }) =>
      slug &&
      css`
        max-height: 600px;
        grid-row: 1/2;
      `}

    ${({ slugIverse }) =>
      slugIverse &&
      css`
        grid-column: 2;
        grid-row: 1;
      `}
  }
`
const CategorieTitle = styled.div`
  div {
    margin-top: 15px;
    position: relative;
    :before {
      border-radius: 3px;
      content: '';
      background-color: gray;
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 1px;
      opacity: 0.2;
      z-index: -1;
    }

    :after {
      position: absolute;
      height: 3px;
      width: 40px;
      left: 0;
      bottom: 0;
      content: '';
      background-color: ${({ theme }) => theme.colors.secondary};
    }
  }
`

const CategoriesGroup = styled.div`
  @media ${({ theme }) => theme.devices.laptop} {
    max-height: 330px;
    overflow-y: scroll;
    padding-right: 20px;

    ${({ slug }) =>
      slug &&
      css`
        max-height: 400px;
      `}
  }
`

const CategoriesList = styled.ul`
  margin: 27px 0px;
  display: grid;
  grid-template-columns: 1fr 1fr;
  column-gap: 5px;

  @media ${({ theme }) => theme.devices.tablet} {
    grid-template-columns: 1fr 1fr 1fr;
  }

  @media ${({ theme }) => theme.devices.laptop} {
    display: block;
  }
`
