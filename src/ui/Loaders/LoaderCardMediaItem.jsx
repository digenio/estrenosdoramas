import React from 'react'

import styled, { css, keyframes } from 'styled-components'

export const LoaderCardMediaItem = ({
  episode,
  carousel,
  postel,
  detail,
  list
}) => {
  return (
    <Item carousel={carousel} postel={postel} detail={detail} list={list}>
      <header>
        {!detail && <Name detail={detail}></Name>}

        {episode && <Episode detail={detail}> </Episode>}
      </header>

      <CarouselImage detail={detail} list={list}></CarouselImage>
      {detail && (
        <Description>
          <TextDetails></TextDetails>
          <TextDetails></TextDetails>
          <TextDetails></TextDetails>
          <TextDetails></TextDetails>
          <TextDetails></TextDetails>
        </Description>
      )}

      <Categorie detail={detail}></Categorie>
      {detail && <ButtonLoader></ButtonLoader>}
    </Item>
  )
}
const shimmer = keyframes`
from{
background-position: -450px 0;
}

to{background-position: 450px 0;}
`

const Item = styled.article`
  margin: 0 auto;
  position: relative;
  padding-right: 5px;
  width: 100%;
  height: 100%;
  cursor: pointer;

  @media ${({ theme }) => theme.devices.tablet} {
    max-width: calc(50vw - 16px);
  }
  ${({ carousel }) =>
    carousel &&
    css`
      :nth-child(2) {
        display: none;
      }
      @media ${({ theme }) => theme.devices.tablet} {
        ${({ postel }) =>
          postel &&
          css`
            height: 200px;
          `}
      }
      ${({ detail }) =>
        detail &&
        css`
          height: 400px;
          @media ${({ theme }) => theme.devices.laptop} {
            height: 300px;
          }
        `}
    `}
  header {
    position: absolute;
    padding: 1rem;
    left: 20px;
    right: 0;
    bottom: 0;
    z-index: 2;
  }
`
const CarouselImage = styled.figure`
  padding-top: 45%;
  height: 100%;
  min-height: 170px;
  background-color: ${({ theme }) => theme.colors.body};
  border-radius: 0.25rem;
  ${({ detail }) =>
    detail &&
    css`
      height: 400px;
      @media ${({ theme }) => theme.devices.laptop} {
        height: 300px;
      }
    `}

  ${({ list }) =>
    list &&
    css`
      height: 200px;
      @media ${({ theme }) => theme.devices.tablet} {
        height: 240px;
      }
    `}
`

const Categorie = styled.span`
  width: 70px;
  height: 24px;
  padding: 5px;
  position: absolute;
  top: 0.7rem;
  left: 1.5rem;
  background-color: white;
  border-radius: 5px;

  ${({ detail }) =>
      detail &&
      css`
        max-width: 200px;
        width: 100%;
        @media ${({ theme }) => theme.devices.laptop} {
          max-width: 300px;
          width: 100%;
        }
      `}
    :before {
    content: ' ';
    position: absolute;
    max-width: 300px;
    width: 100%;
    top: 0;
    left: 0;
    min-width: 50px;
    height: 24px;
    object-fit: cover;
    border-radius: inherit;
    background-image: linear-gradient(
      90deg,
      rgba(0, 0, 0, 0) 0%,
      rgba(109, 110, 113, 0.2) 50%,
      rgba(93, 94, 96, 0.7) 100%
    );
    background-size: 450px 450px;
    background-repeat: no-repeat;
    z-index: 1;
    animation: ${shimmer} 1s linear infinite;
    animation-delay: 0.2s;
  }
`

const Description = styled.div`
  min-width: 100px;
  max-width: 150px;
  width: 100%;
  height: 100px;
  position: absolute;
  top: 80px;
  @media ${({ theme }) => theme.devices.laptop} {
    min-width: 100px;
    max-width: 500px;
    width: 100%;
  }
`

const TextDetails = styled.div`
  max-width: 140px;
  width: 100%;
  height: 5px;
  padding: 5px;
  margin: 10px;
  position: relative;

  left: 1.5rem;
  background-color: white;
  border-radius: 5px;

  @media ${({ theme }) => theme.devices.laptop} {
    max-width: 400px;
    width: 100%;
    height: 5px;
    padding: 5px;
    margin: 10px;
    position: relative;

    left: 1.5rem;
    background-color: white;
    border-radius: 5px;
  }
  :nth-child(2n) {
    width: 100%;
    max-width: 100px;
    @media ${({ theme }) => theme.devices.laptop} {
      width: 100%;
      max-width: 300px;
    }
  }

  :before {
    content: ' ';
    position: absolute;
    max-width: 400px;
    width: 100%;
    top: 0;
    left: 0;

    min-width: 50px;
    height: 7px;
    object-fit: cover;
    border-radius: inherit;
    background-image: linear-gradient(
      90deg,
      rgba(0, 0, 0, 0) 0%,
      rgba(109, 110, 113, 0.2) 50%,
      rgba(93, 94, 96, 0.7) 100%
    );
    /* background-size: 450px 450px; */
    background-repeat: no-repeat;
    z-index: 1;
    animation: ${shimmer} 1s linear infinite;
    animation-delay: 0.2s;

    :nth-child(2n) {
      width: 300px;
    }
  }
`

const ButtonLoader = styled.div`
  min-width: 50px;
  max-width: 160px;
  width: 100%;
  height: 40px;
  border-radius: 5px;
  left: 1.5rem;
  position: absolute;
  bottom: 40px;
  background-color: white;
  @media ${({ theme }) => theme.devices.laptop} {
    bottom: 0px;
  }
  :before {
    content: ' ';
    position: absolute;
    width: 100%;
    min-width: 50px;
    max-width: 160px;
    height: 40px;
    object-fit: cover;
    border-radius: inherit;
    background-image: linear-gradient(
      90deg,
      rgba(0, 0, 0, 0) 0%,
      rgba(38, 42, 54, 0.2) 50%,
      rgba(38, 42, 54, 0.7) 100%
    );
    background-size: 50px 50px;
    background-repeat: no-repeat;
    z-index: 1;
    animation: ${shimmer} 1s linear infinite;
    animation-delay: 0.9s;
  }
`
const Name = styled.div`
  min-width: 50px;
  border-radius: 5px;
  width: 90%;
  max-width: 100px;
  height: 10px;
  background-color: white;

  :before {
    content: ' ';
    position: absolute;
    width: 70%;
    min-width: 50px;
    max-width: 100px;
    height: 10px;
    object-fit: cover;
    border-radius: inherit;
    background-image: linear-gradient(
      90deg,
      rgba(0, 0, 0, 0) 0%,
      rgba(38, 42, 54, 0.2) 50%,
      rgba(38, 42, 54, 0.7) 100%
    );
    background-size: 50px 50px;
    background-repeat: no-repeat;
    z-index: 1;
    animation: ${shimmer} 1s linear infinite;
    animation-delay: 0.9s;
  }
`

const Episode = styled.span`
  min-width: 50px;
  border-radius: 5px;
  width: 90%;
  max-width: 100px;
  height: 10px;
  position: relative;

  display: block;
  /* top: 0; */
  margin-top: 5px;
  background-color: white;

  :before {
    content: ' ';
    position: absolute;
    top: 0;
    left: 0;
    height: 10px;
    width: 100%;
    min-width: 50px;
    max-width: 100px;
    object-fit: cover;
    border-radius: inherit;
    background-image: linear-gradient(
      90deg,
      rgba(0, 0, 0, 0) 0%,
      rgba(38, 42, 54, 0.2) 50%,
      rgba(38, 42, 54, 0.7) 100%
    );
    background-size: 450px 450px;
    background-repeat: no-repeat;
    z-index: 1;
    animation: ${shimmer} 1s linear infinite;
    animation-delay: 0.2s;
  }
`
