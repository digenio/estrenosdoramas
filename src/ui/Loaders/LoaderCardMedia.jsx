import React from 'react'
import styled, { css, keyframes } from 'styled-components'

export const LoaderCardMedia = ({
  children,
  columns = 8,
  minsizeColumns = '132px',
  carousel,
  media,
  title,
  postel,
  detail
}) => (
  <>
    {title && <TitleLoader></TitleLoader>}
    <CarouselLoader
      columns={columns}
      minsizeColumns={minsizeColumns}
      carousel={carousel}
      mediaBoll={media}
      postel={postel}
      detail={detail}
    >
      {children}
    </CarouselLoader>
  </>
)

const shimmer = keyframes`
from{
background-position: -450px 0;
}

to{background-position: 450px 0;}
`

const TitleLoader = styled.h1`
  background-color: ${({ theme }) => theme.colors.body};
  min-width: 50px;
  max-width: 160px;
  width: 100%;
  height: 40px;
  border-radius: 5px;
  left: 1.5rem;

  bottom: 40px;
  :before {
    content: ' ';
    position: absolute;
    width: 100%;
    min-width: 50px;
    max-width: 160px;
    height: 40px;
    object-fit: cover;
    border-radius: inherit;
    background-image: linear-gradient(
      90deg,
      rgba(0, 0, 0, 0) 0%,
      rgba(38, 42, 54, 0.2) 50%,
      rgba(38, 42, 54, 0.7) 100%
    );
    background-size: 50px 50px;
    background-repeat: no-repeat;
    z-index: 1;
    animation: ${shimmer} 1s linear infinite;
    animation-delay: 0.9s;
  }
`
const CarouselLoader = styled.div`
  display: grid;
  width: 100%;
  grid-gap: 10px;
  grid-auto-rows: auto;
  margin-top: 30px;
  margin-bottom: 30px;
  min-height: 300px;
  ${({ carousel }) =>
    carousel &&
    css`
      ${({ postel }) =>
        postel &&
        css`
            grid-template-columns: repeat(2, 1fr);
            @media ${({ theme }) => theme.devices.mobileL} {
        grid-template-columns: repeat(3, 1fr);
      }
      @media ${({ theme }) => theme.devices.tabletS} {
        grid-template-columns: repeat(4, 1fr);
      }
      @media ${({ theme }) => theme.devices.tablet} {
        /* grid-template-columns: repeat(
          ${({ columns }) => columns},
          minmax(${({ minsizeColumns }) => minsizeColumns}, 1fr)
        ); */
        grid-template-columns: repeat(5, 1fr);
        grid-column: span 2;
        grid-row: 1;
       
      }
      @media ${({ theme }) => theme.devices.laptop} {
        grid-template-columns: repeat(8, minmax(0, 1fr));
      }
            `}

      ${({ detail }) =>
        detail &&
        css`
          @media ${({ theme }) => theme.devices.tablet} {
            grid-column: 2;
            grid-row: 2/-1;
          }
        `}
    `}
  ${({ mediaBoll }) =>
    mediaBoll &&
    css`
      grid-template-columns: repeat(2, minmax(0, 1fr));
      @media ${({ theme }) => theme.devices.mobileM} {
        grid-template-columns: repeat(3, minmax(0, 1fr));
        grid-gap: 1rem;
      }
      @media ${({ theme }) => theme.devices.mobileL} {
        grid-template-columns: repeat(4, minmax(0, 1fr));
        grid-gap: 1rem;
      }
      @media ${({ theme }) => theme.devices.tabletS} {
        grid-template-columns: repeat(5, minmax(0, 1fr));
        grid-gap: 1rem;
      }

      @media ${({ theme }) => theme.devices.laptop} {
        grid-template-columns: repeat(4, minmax(0, 1fr));
      }
      @media ${({ theme }) => theme.devices.laptopL} {
        grid-template-columns: repeat(5, minmax(0, 1fr));
        grid-gap: 1.5rem;
      }
    `};
`
