import styled, { css, keyframes } from 'styled-components'

export const LoaderAsideItem = ({}) => {
  return (
    <Item>
      <Figure></Figure>

      <Description>
        <Details>
          <ItemText />
          <ItemText />

          <ItemText />
        </Details>
      </Description>
    </Item>
  )
}
const shimmer = keyframes`
from{
background-position: -450px 0;
}

to{background-position: 450px 0;}
`
const Figure = styled.figure`
  position: relative;
  width: 100%;
  max-width: 55px;
  height: 85px;
  margin-bottom: 10px;
  background: white;
  border-radius: 5px;
  :before {
    content: ' ';
    position: absolute;
    max-width: 55px;
    width: 100%;
    top: 0;
    left: 0;
    min-width: 50px;
    height: 85px;
    object-fit: cover;
    border-radius: inherit;
    background-image: linear-gradient(
      90deg,
      rgba(0, 0, 0, 0) 0%,
      rgba(109, 110, 113, 0.2) 50%,
      rgba(93, 94, 96, 0.7) 100%
    );
    background-size: 450px 450px;
    background-repeat: no-repeat;
    z-index: 1;
    animation: ${shimmer} 1s linear infinite;
    animation-delay: 0.2s;
  }
`

const Description = styled.div`
  p {
    font-weight: 300;
  }
`
const Item = styled.div`
  position: relative;
  display: flex;
  min-width: 0;
  padding: 5px;
  gap: 10px;
  width: 100%;
  cursor: pointer;

  @media ${({ theme }) => theme.devices.laptop} {
  }
`

const Details = styled.div`
  width: 220px;
  height: 80px;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: flex-end;
`

const ItemText = styled.div`
  position: relative;
  width: 100%;
  max-width: 200px;
  height: 10px;
  background: white;
  border-radius: 5px;
  margin: 5px 0;

  :before {
    content: ' ';
    position: absolute;
    max-width: 200px;
    width: 100%;
    top: 0;
    left: 0;
    min-width: 50px;
    height: 10px;
    object-fit: cover;
    border-radius: inherit;
    background-image: linear-gradient(
      90deg,
      rgba(0, 0, 0, 0) 0%,
      rgba(109, 110, 113, 0.2) 50%,
      rgba(93, 94, 96, 0.7) 100%
    );
    background-size: 450px 450px;
    background-repeat: no-repeat;
    z-index: 1;
    animation: ${shimmer} 1s linear infinite;
    animation-delay: 0.2s;
  }
`
