import { LoaderCardMedia } from '../../ui/Loaders/LoaderCardMedia'
import { LoaderCardMediaItem } from '../../ui/Loaders/LoaderCardMediaItem'

export const LoaderMediaList = ({ title }) => {
  return (
    <LoaderCardMedia media title={title}>
      <LoaderCardMediaItem episode list />
      <LoaderCardMediaItem episode list />
      <LoaderCardMediaItem episode list />
      <LoaderCardMediaItem episode list />
      <LoaderCardMediaItem episode list />
      <LoaderCardMediaItem episode list />
      <LoaderCardMediaItem episode list />
      <LoaderCardMediaItem episode list />
      <LoaderCardMediaItem episode list />
      <LoaderCardMediaItem episode list />
    </LoaderCardMedia>
  )
}
