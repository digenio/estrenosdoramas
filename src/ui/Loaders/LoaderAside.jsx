import styled from 'styled-components'
import { RatingList } from '../Media/RatingList'
import { LoaderAsideItem } from './LoaderAsideItem'

export const LoaderAside = ({ movie, list }) => {
  return (
    <RatingMovie>
      <RatingList>
        {movie && (
          <CategorieTitle>
            <p>Peliculas</p>
            <div></div>
          </CategorieTitle>
        )}

        <LoaderAsideItem />
        <LoaderAsideItem />
        <LoaderAsideItem />
        <LoaderAsideItem />
        <LoaderAsideItem />
      </RatingList>
    </RatingMovie>
  )
}

const RatingMovie = styled.div`
  @media ${({ theme }) => theme.devices.laptop} {
    grid-row: 3/4;
  }
`
const CategorieTitle = styled.div`
  div {
    margin-top: 15px;
    position: relative;
    :before {
      border-radius: 3px;
      content: '';
      background-color: gray;
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 1px;
      opacity: 0.2;
      z-index: -1;
    }

    :after {
      position: absolute;
      height: 3px;
      width: 40px;
      left: 0;
      bottom: 0;
      content: '';
      background-color: ${({ theme }) => theme.colors.secondary};
    }
  }
`
