import { LoaderCardMedia } from './LoaderCardMedia'
import { LoaderCardMediaItem } from './LoaderCardMediaItem'

export const CarrouselLoader = ({ details }) => {
  return (
    <LoaderCardMedia carousel postel={details ? false : true} detail={details}>
      {details ? (
        <LoaderCardMediaItem carousel detail />
      ) : (
        <>
          <LoaderCardMediaItem carousel episode postel />
          <LoaderCardMediaItem carousel episode postel />
          <LoaderCardMediaItem carousel episode postel />
          <LoaderCardMediaItem carousel episode postel />
          <LoaderCardMediaItem carousel episode postel />
          <LoaderCardMediaItem carousel episode postel />
          <LoaderCardMediaItem carousel episode postel />
          <LoaderCardMediaItem carousel episode postel />
          <LoaderCardMediaItem carousel episode postel />
        </>
      )}
    </LoaderCardMedia>
  )
}
