import Img from 'react-cool-img'
import Link from 'next/link'
import styled from 'styled-components'
import { DetailRating } from '../Media/DetailRating'

import { CastList } from '../Base/CastList'
import { Button } from '../Buttons/Button'

export const CarouselItemDetail = ({
  name = 'The Good Doctor',
  episode,
  image = 'https://image.tmdb.org/t/p/w220_and_h330_face/rsdTehWrJPGZkggKm6Jvs4TxAkj.jpg',
  type = 'Dorama',
  href,
  as,
  overview,
  genrs,
  label,
  cast,
  page,
  link,
  country,
  premiere,
  air
}) => {
  const year = air?.split('-')[0]

  return (
    <Item>
      <a>
        <CarouselImage>
          <Img src={image} alt={name} />
        </CarouselImage>
      </a>

      <ItemHeader>
        <h2>{name}</h2>
        <DetailRating
          country={country}
          category={type}
          year={year}
          premiere={premiere}
        />
        <DescriptionOverview>{overview}</DescriptionOverview>

        <CastList cast={cast} />

        <Link href={page} as={link}>
          <a>
            <ButtonCarousel detailOne height={50} width={180}>
              Ver Dorama
            </ButtonCarousel>
          </a>
        </Link>
      </ItemHeader>
    </Item>
  )
}
const Item = styled.article`
  position: relative;
  transition: box-shadow 0.3s;
  background-color: ${({ theme }) => theme.colors.body};
  @media ${({ theme }) => theme.devices.laptop} {
    height: 650px;

    color: white;
    cursor: pointer;
    width: 100%;
    :hover {
      box-shadow: inset 0 0 10rem rgb(10 10 10 / 80%),
        0 0 2rem rgb(10 10 10 / 80%);
    }

    background-color: transparent;
  }

  a {
    span {
      display: none;
    }
  }
`

const ItemHeader = styled.div`
  position: relative;
  color: black;
  margin: 20px;
  width: 90%;
  z-index: 3;

  h2 {
    font-weight: 400;
    margin-bottom: 15px;
    font-size: 1rem;
    @media ${({ theme }) => theme.devices.laptop} {
      font-size: 1.5rem;
    }
  }
  p {
  }
  @media ${({ theme }) => theme.devices.laptop} {
    width: 80%;
    top: 10rem;
    left: 2rem;
    position: absolute;
    z-index: 3;
    color: white;
  }
`
const DescriptionOverview = styled.p`
  width: 100%;

  margin-top: 10px;
  font-size: 10px;
  overflow-y: scroll;
  max-height: 115px;
  margin-bottom: 10px;
  @media ${({ theme }) => theme.devices.laptop} {
    font-size: 14px;
  }
`
const CarouselImage = styled.figure`
  @media ${({ theme }) => theme.devices.laptop} {
    padding-top: 45%;
    min-height: 170px;
    z-index: -1;
    border-radius: 2rem;
    z-index: 10;

    :before {
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      object-fit: cover;
      background: linear-gradient(
        90deg,
        rgba(20, 20, 22, 0.9612219887955182) 22%,
        rgba(24, 23, 23, 0.32816876750700286) 77%,
        rgba(129, 129, 129, 0.165703781512605) 94%
      );

      height: 100%;
      top: auto;
      bottom: 0;
      z-index: 1;
      content: ' ';
    }
  }
  img {
    width: 100%;
    height: 350px;
    object-fit: cover;
    border-radius: 0.25rem;

    @media ${({ theme }) => theme.devices.laptop} {
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      z-index: -1;
    }
  }
`
const ButtonCarousel = styled(Button)`
  height: 40px;
  margin: 5px 0 40px 0;
  font-size: 13px;
`
