import { useQuery } from '@apollo/client'
import { useState } from 'react'
import styled, { css } from 'styled-components'

import { RatingListMovies } from '../../components/Base/RatingListMovies'
import { RatingListSeries } from '../../components/Base/RatingListSeries'
import { TREND_DORAMAS_TODAY } from '../../gql/dorama'
import { LIST_MOVIES } from '../../gql/movie'
import { URL_IMAGE_POSTER } from '../../utils/urls'

export const AsideRating = ({
  slug,
  movie,
  serie,
  slugIverse,
  chapterSlug
}) => {
  const [sort, changeSort] = useState('POPULARITY_DESC')

  const { data: { trendsDoramas = [] } = {}, loading: ldgDoramas } = useQuery(
    TREND_DORAMAS_TODAY,
    {
      variables: { limit: 9, days: 0 },
      pollInterval: 30 * 60 * 1000
    }
  )

  const { data: { listMovies = [] } = {}, loading: ldgMovies } = useQuery(
    LIST_MOVIES,
    {
      variables: { limit: 10, sort: sort }
    }
  )

  const listDoramas = trendsDoramas.map(item => ({
    name: item.name || item.name_es,
    image: item.poster || URL_IMAGE_POSTER + item.poster_path,
    year: item.first_air_date && item.first_air_date.split('-')[0],
    category: 'Dorama',
    page: `/doramas/[slug]`,
    link: `/doramas/${item.slug}`,
    airDate: item.first_air_date,
    count: item.count,
    country: item.country,
    premiere: item.premiere
  }))

  const listMovie = listMovies.map(item => ({
    name: item.name || item.name_es,
    image: item.poster || URL_IMAGE_POSTER + item.poster_path,
    year: item.release_date && item.release_date.split('-')[0],
    category: 'Movie',
    page: `/peliculas/[slug]`,
    link: `/peliculas/${item.slug}`,
    airDate: item.first_air_date,
    count: item.count,
    country: item.country,
    runtime: item.runtime
  }))

  return (
    <Aside slug={slug} slugIverse={slugIverse} chapterSlug={chapterSlug}>
      {movie ? (
        <RatingListMovies aside movie list={listMovie} ldgMovies={ldgMovies} />
      ) : serie ? (
        <RatingListSeries
          aside
          series
          list={listDoramas}
          ldgDoramas={ldgDoramas}
        />
      ) : (
        <>
          <RatingListMovies
            aside
            movie
            list={listMovie}
            ldgMovies={ldgMovies}
          />

          <RatingListSeries
            aside
            series
            list={listDoramas}
            ldgDoramas={ldgDoramas}
          />
        </>
      )}
    </Aside>
  )
}

const Aside = styled.div`
/* z-index: -1; */
  @media ${({ theme }) => theme.devices.laptop} {
    grid-row: 3;
    ${({ slug }) =>
    slug &&
    css`
        grid-column: 1;
        grid-row: 2/3;
      `}
    ${({ slugIverse }) =>
    slugIverse &&
    css`
        grid-column: 2;
        grid-row: 2/3;
      `}

      ${({ chapterSlug }) =>
    chapterSlug &&
    css`
          grid-column: 1/2;
          grid-row: 1/2;
        `}
  }
`
