import { useContext } from 'react'

import { HomeList } from '../../components/Home/HomeList'
import { ListContext } from '../../contexts/ListContext'
import {
  getHrefByCategory,
  getSlugByCategory,
  makeFistLetterUppercase
} from '../../utils/functions'
import { URL_IMAGE_POSTER } from '../../utils/urls'

export const HomeSeeing = () => {
  const { list } = useContext(ListContext)

  const listItems = list.map(item => ({
    name: item.name || item.name_es,
    nameEs: item.name_es,
    image: item.poster || URL_IMAGE_POSTER + item.poster_path,
    category: item.type == 'movie' ? 'Pelicula' : 'Dorama',
    page: getHrefByCategory(makeFistLetterUppercase(item.type)),
    link: getSlugByCategory(makeFistLetterUppercase(item.type), item.slug),
    status: item.status,
    overview: item.overview,
    genres: item.genres,
    labels: item.labels,
    country: item.country,
    premiere: item.premiere
  }))

  return (
    <HomeList list={listItems} loading={list.length === 0} categorie='Viendo' />
  )
}
