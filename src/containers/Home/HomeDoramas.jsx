import { HomeList } from '../../components/Home/HomeList'
import styled from 'styled-components'
import { useQuery } from '@apollo/client'
import { LIST_DORAMAS } from '../../gql/dorama'
import { URL_IMAGE_POSTER } from '../../utils/urls'

export const HomeDoramas = ({ tvShows = false }) => {
  const { data: { listDoramas = [] } = {}, load1 } = useQuery(LIST_DORAMAS, {
    variables: {
      filter: { isTVShow: false },
      limit: 10,
      sort: '_ID_DESC'
    }
  })

  const list = listDoramas.map(item => ({
    name: item.name || item.name_es,
    image: item.poster || URL_IMAGE_POSTER + item.poster_path,
    year: item.first_air_date && item.first_air_date.split('-')[0],
    category: tvShows ? 'KShow' : 'Dorama',
    page: `/${tvShows ? 'variedades' : 'doramas'}/[slug]`,
    link: `/${tvShows ? 'variedades' : 'doramas'}/${item.slug}`,
    overview: item.overview,
    genres: item.genres,
    labels: item.labels,
    country: item.country,
    premiere: item.premiere
  }))

  return (
    <Grid>
      <HomeList list={list} categorie='doramas' loading={load1} />
    </Grid>
  )
}

const Grid = styled.div`
  @media ${({ theme }) => theme.devices.laptop} {
    /* grid-column: span 2; */
  }
`
