import { useQuery } from '@apollo/client'

import { shuffleArray } from '../../utils/functions'

import { LIST_DORAMAS_CARROUSEL } from '../../gql/dorama'

import { CarouselDetails } from '../../components/Carousels/CarouselDetails'
import { CarrouselLoader } from '../../ui/Loaders/CarrouselLoader'

export const HomeCarouselDetails = () => {
  const {
    data: { carrouselDoramas = [] } = {},
    loading: loading,
    error: err
  } = useQuery(LIST_DORAMAS_CARROUSEL, {
    variables: { limit: 10 },
    fetchPolicy: 'cache-first'
  })

  const length = carrouselDoramas.length

  if (loading && length === 0) {
    return <CarrouselLoader details />
  }

  if (err) {
    return 'Error'
  }

  return <CarouselDetails list={carrouselDoramas} />
}
