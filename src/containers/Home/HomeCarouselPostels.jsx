import { useQuery } from '@apollo/client'

import { LIST_MOVIES_CARROUSEL } from '../../gql/movie'
import { CarouselPostels } from '../../components/Carousels/CarouselPostels'
import { CarrouselLoader } from '../../ui/Loaders/CarrouselLoader'
import { shuffleArray } from '../../utils/functions'
import { LIST_DORAMAS_CARROUSEL } from '../../gql/dorama'

export const HomeCarouselPostels = () => {
  const {
    data: { carrouselMovies = [] } = {},
    loading: ld1,
    error: err
  } = useQuery(LIST_MOVIES_CARROUSEL, {
    variables: { limit: 15 },
    fetchPolicy: 'cache-first'
  })

  const {
    data: { carrouselDoramas = [] } = {},
    loading: ld2,
    error: err2
  } = useQuery(LIST_DORAMAS_CARROUSEL, {
    variables: { limit: 10 },
    fetchPolicy: 'cache-first'
  })

  const length = carrouselMovies.length + carrouselDoramas.length

  if ((ld1 || ld2) && length === 0) {
    return <CarrouselLoader />
  }

  if (err || err2) {
    return 'Error'
  }

  return (
    <CarouselPostels
      list={shuffleArray(carrouselDoramas.concat(carrouselMovies))}
    />
  )
}
