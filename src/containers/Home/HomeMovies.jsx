import { useQuery } from '@apollo/client'
import { LIST_MOVIES } from '../../gql/movie'
import { HomeList } from '../../components/Home/HomeList'
import { URL_IMAGE_POSTER } from '../../utils/urls'

export const HomeMovies = () => {
  const { data: { listMovies = [] } = {}, loading } = useQuery(LIST_MOVIES, {
    variables: {
      limit: 10,
      sort: '_ID_DESC'
    }
  })
  const list = listMovies.map(item => ({
    name: item.name || item.name_es,
    image: item.poster || URL_IMAGE_POSTER + item.poster_path,
    year: item.release_date && item.release_date.split('-')[0],
    category: 'Película',
    page: `/peliculas/[slug]`,
    link: `/peliculas/${item.slug}`,
    overview: item.overview,
    genres: item.genres,
    labels: item.labels
  }))

  return (
    <>
      <HomeList list={list} categorie='peliculas' loading={loading} />
    </>
  )
}
