import { LAST_EPISODES } from '../../gql/episode'
import { URL_IMAGE_POSTER } from '../../utils/urls'
import { HomeList } from '../../components/Home/HomeList'
import { useQuery } from '@apollo/client'
import { useEffect } from 'react'

export const HomeEpisodes = () => {
  const {
    data: { premiereEpisodes = [] } = {},
    loading,
    refetch,
    error
  } = useQuery(LAST_EPISODES, {
    variables: { limit: 10 }
  })

  useEffect(() => {
    if (premiereEpisodes.length) {
      refetch && refetch()
    }
  }, [])

  const list = premiereEpisodes.map(item => ({
    name: item.serie_name || item.serie_name_es,
    image: item.poster || URL_IMAGE_POSTER + item.serie_poster,
    year: item?.air_date && item?.air_date.split('T')[0],
    season_number: item.season_number,
    episode_number: item.episode_number,
    category: 'Capitulo',
    page: `/capitulos/[slug]`,
    link: `/capitulos/${item.slug}`
  }))

  if (error) {
    return 'Error'
  }

  return (
    <HomeList list={list} chapter categorie='Capitulos' loading={loading} />
  )
}
