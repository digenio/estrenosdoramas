import { useQuery } from '@apollo/client'
import { useEffect } from 'react'

import { HomeList } from '../../components/Home/HomeList'
import { LIST_DORAMAS } from '../../gql/dorama'
import {
  getHrefByCategory,
  getSlugByCategory,
  makeFistLetterUppercase
} from '../../utils/functions'
import { URL_IMAGE_POSTER } from '../../utils/urls'

export const HomeTvShows = ({ tvShows = true }) => {
  const variables = {
    filter: { isTVShow: tvShows },
    limit: 10,
    sort: '_ID_DESC'
  }

  const { data: { listDoramas = [] } = {}, loading, refetch } = useQuery(
    LIST_DORAMAS,
    {
      variables
    }
  )

  useEffect(() => {
    if (listDoramas.length > 0) {
      refetch && refetch()
    }
  }, [])

  const listItems = listDoramas.map(item => ({
    name: item.name || item.name_es,
    image: item.poster || URL_IMAGE_POSTER + item.poster_path,
    year: item.first_air_date && item.first_air_date.split('-')[0],
    category: tvShows ? 'KShow' : 'Dorama',
    page: `/${tvShows ? 'variedades' : 'doramas'}/[slug]`,
    link: `/${tvShows ? 'variedades' : 'doramas'}/${item.slug}`,
    overview: item.overview,
    genres: item.genres,
    labels: item.labels,
    country: item.country,
    premiere: item.premiere
  }))

  return <HomeList list={listItems} categorie='variedades' />
}
