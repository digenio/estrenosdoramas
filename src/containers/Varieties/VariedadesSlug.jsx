import { VariedadesTabsCast } from '../Varieties/VariedadesTabsCast'
import { BanneDetails } from '../../components/Base/BanneDetails'
import { Categories } from '../../ui/Base/Categories/Categories'
import { AsideRating } from '../../containers/Home/AsideRating'
import { DoramasChapterList } from '../../components/Doramas/DoramasChapterList'
import { Trailer } from '../../ui/Base/Trailer'
import { VariedadesTabsList } from '../../containers/Varieties/VariedadesTabsList'

export const VariedadesSlug = (dorama = []) => {
  return (
    <>
      <BanneDetails />
      <Trailer />
      <div>
        <VariedadesTabsList dorama={dorama} />
        <VariedadesTabsCast dorama={dorama} />
      </div>
      <Categories />
      <AsideRating />
    </>
  )
}
