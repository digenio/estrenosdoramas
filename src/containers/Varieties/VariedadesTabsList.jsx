import { useContext, useEffect, useState } from 'react'
import { useLazyQuery, useQuery } from '@apollo/client'

import { DoramasSeasonTabs } from '../../components/Doramas/DoramasSeasonTabs'
import { VariedadesChapterList } from '../../components/Varieties/VariedadesChapterList'
import { LoaderAsync } from '../../components/Base/LoaderAsync'
// import { Pagination } from '../../components/Pagination/Pagination'
import { copy } from '../../utils/functions'
import { LIST_SEEN } from '../../gql/seen'

import { EPISODES_PAGINATION } from '../../gql/episode'

import { UserContext } from '../../contexts/UserContext'
import { Pagination } from '../../components/Pagination/Pagination'

export const VariedadesTabsList = ({ dorama = [] }) => {
  const { loggedUser } = useContext(UserContext)
  const [season, setSeason] = useState(1)
  const [page, changePage] = useState(1)
  const perPage = 10

  const variables = {
    page,
    perPage,
    serie_id: dorama._id,
    season_number: season
  }

  const [getSeen, { data: { listSeen = [] } = {}, refetch }] = useLazyQuery(
    LIST_SEEN,
    {
      fetchPolicy: 'no-cache'
    }
  )

  const {
    data: { paginationEpisode: { items = [], count = 0 } = {} } = {},
    loading,
    error
  } = useQuery(EPISODES_PAGINATION, {
    variables: copy(variables)
  })

  useEffect(() => {
    if (loggedUser) {
      getSeen({ variables: { group_id: dorama?._id, user_id: loggedUser._id } })
    }
  }, [dorama?._id, loggedUser])

  return (
    <DoramasSeasonTabs
      season={season}
      setSeason={setSeason}
      dorama={dorama}
      changePage={changePage}
    >
      <VariedadesChapterList
        listEpisodes={items}
        listSeen={listSeen}
        refetch={refetch}
      />
      <Pagination
        page={page}
        perPage={perPage}
        changePage={changePage}
        total={count}
      />
    </DoramasSeasonTabs>
  )
}
