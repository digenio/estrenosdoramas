import { useState, useEffect } from 'react'
import { VariedadesCastsTabs } from '../../components/Varieties/VariedadesCastsTabs'
import { ViewCasts } from '../../components/Base/ViewCasts'
import { ViewDetails } from '../../components/Base/ViewDetails'
export const VariedadesTabsCast = (dorama = []) => {
  const [current, changeCurrent] = useState('details')
  useEffect(() => {
    changeCurrent('details')
  }, [])
  return (
    <VariedadesCastsTabs current={current} changeCurrent={changeCurrent}>
      {current === 'details' ? <ViewDetails /> : <ViewCasts />}
    </VariedadesCastsTabs>
  )
}
