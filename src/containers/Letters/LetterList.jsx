import { useQuery } from '@apollo/client'
import { useEffect, useState } from 'react'
import styled, { css } from 'styled-components'

import { LetterSlugList } from '../../components/Letters/LetterSlugList'
import { Pagination } from '../../components/Pagination/Pagination'
import { LIST_DORAMAS } from '../../gql/dorama'
import { LIST_MOVIES } from '../../gql/movie'
import {
  getHrefByCategory,
  getSlugByCategory,
  shuffleArray
} from '../../utils/functions'
import { URL_IMAGE_POSTER } from '../../utils/urls'

export const LetterList = ({ name, _id, slugIverse, letter = '' }) => {
  const [filter, changeFilter] = useState({})
  const [page, changePage] = useState(1)

  const perPage = 20

  const variables = {
    filter: { letter }
  }

  useEffect(() => {
    changeFilter({ genreId: _id })
  }, [_id])

  const { data: { listMovies = [] } = {}, loading: ldgMovies } = useQuery(
    LIST_MOVIES,
    {
      variables
    }
  )
  const { data: { listDoramas = [] } = {}, loading: ldgDoramas } = useQuery(
    LIST_DORAMAS,
    {
      variables
    }
  )
  const lengthMedia = listMovies.length + listDoramas.length

  // const length = listMovies.length + listDoramas.length
  // const list = mixArray(listMovies, listDoramas, length)

  const title = `Doramas y peliculas por la letra "${letter}"`

  const mixData = shuffleArray(listMovies.concat(listDoramas))

  const pageList = mixData
    .filter((item, idx) => idx >= (page - 1) * perPage && idx < page * perPage)
    .map(item => ({
      name: item.name || item.name_es,
      image: item.poster || URL_IMAGE_POSTER + item.poster_path,
      year: item.release_date && item.release_date.split('-')[0],
      category:
        item.__typename === 'Dorama'
          ? item.isTVShow
            ? 'KShow'
            : 'Dorama'
          : 'Película',
      page: getHrefByCategory(item.__typename),
      link: getSlugByCategory(item.__typename, item.slug),
      overview: item.overview,
      genres: item.genres,
      labels: item.labels
    }))

  // if ((ldgMovies || ldgDoramas) && lengthMedia === 0) {
  //   return <LoaderMediaList />
  // }

  return (
    <Container slugIverse={slugIverse}>
      <LetterSlugList list={pageList} name={title} />
      <Pagination
        page={page}
        perPage={perPage}
        changePage={changePage}
        total={lengthMedia}
      />
    </Container>
  )
}

const Container = styled.div`
  margin: 50px 0;
  @media ${({ theme }) => theme.devices.laptop} {
    grid-row: 1/-1;

    ${({ slugIverse }) =>
      slugIverse &&
      css`
        grid-row: 1/2;
      `}
  }
`
