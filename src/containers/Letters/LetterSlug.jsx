import { Categories } from '../../ui/Base/Categories/Categories'
import { AsideRating } from '../Home/AsideRating'
import { LetterList } from './LetterList'
export const LetterSlug = letter => {
  return (
    <>
      <LetterList {...letter} slugIverse />
      <Categories slug slugIverse />
      <AsideRating movie slugIverse />
    </>
  )
}
