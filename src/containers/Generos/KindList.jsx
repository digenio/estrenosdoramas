import { useQuery } from '@apollo/client'
import { useEffect, useState } from 'react'
import styled, { css } from 'styled-components'
import { GenrsSlugList } from '../../components/Generos/GenrsSlugList'
import { Pagination } from '../../components/Pagination/Pagination'
import { LIST_DORAMAS } from '../../gql/dorama'
import { LIST_MOVIES } from '../../gql/movie'
import { LoaderMediaList } from '../../ui/Loaders/LoaderMediaList'
import {
  copy,
  getHrefByCategory,
  getSlugByCategory,
  shuffleArray
} from '../../utils/functions'
import { URL_IMAGE_POSTER } from '../../utils/urls'

export const KindList = ({ name, _id, slugIverse }) => {
  const [filter, changeFilter] = useState({})
  const [page, changePage] = useState(1)

  const perPage = 20

  const variables = {
    perPage,
    filter: copy(filter),
    page
  }

  useEffect(() => {
    changeFilter({ genreId: _id })
  }, [_id])

  const { data: { listMovies = [] } = {}, loading: ldgMovies } = useQuery(
    LIST_MOVIES,
    { variables }
  )
  const { data: { listDoramas = [] } = {}, loading: ldgDoramas } = useQuery(
    LIST_DORAMAS,
    { variables }
  )
  const lengthMedia = listMovies.length + listDoramas.length

  // const length = listMovies.length + listDoramas.length
  // const list = mixArray(listMovies, listDoramas, length)

  const title = `Doramas y peliculas de "${name}"`

  const mixData = shuffleArray(listMovies.concat(listDoramas))

  const pageList = mixData
    .filter((item, idx) => idx >= (page - 1) * perPage && idx < page * perPage)
    .map(item => ({
      name: item.name || item.name_es,
      image: item.poster || URL_IMAGE_POSTER + item.poster_path,
      year: item.release_date && item.release_date.split('-')[0],
      category:
        item.__typename === 'Dorama'
          ? item.isTVShow
            ? 'KShow'
            : 'Dorama'
          : 'Película',
      page: getHrefByCategory(item.__typename),
      link: getSlugByCategory(item.__typename, item.slug),
      overview: item.overview,
      genres: item.genres,
      labels: item.labels
    }))

  if ((ldgMovies || ldgDoramas) && lengthMedia === 0) {
    return (
      <Container slugIverse={slugIverse}>
        <LoaderMediaList title={'genero'} />
      </Container>
    )
  }

  return (
    <Container slugIverse={slugIverse}>
      <GenrsSlugList list={pageList} name={name} />
      <Pagination
        page={page}
        perPage={perPage}
        changePage={changePage}
        total={lengthMedia}
      />
    </Container>
  )
}

const Container = styled.div`
  margin: 50px 0;
  z-index: 300;
  @media ${({ theme }) => theme.devices.laptop} {
    grid-row: 1/-1;

    ${({ slugIverse }) =>
    slugIverse &&
    css`
        grid-row: 1/2;
      `}
  }
`
