import { KindList } from './KindList'
import { Categories } from '../../ui/Base/Categories/Categories'
import { AsideRating } from '../Home/AsideRating'

export const GeneroSlug = ({ kind }) => {
  return (
    <>
      <KindList {...kind} slugIverse />
      <Categories slug slugIverse />
      <AsideRating movie slugIverse />
    </>
  )
}
