import { useLazyQuery } from '@apollo/client'

import { DETAIL_GENRE } from '../../gql/genre'
import { GeneroSlug } from '../../containers/Generos/GeneroSlug'
import { LoaderAsync } from '../../components/Base/LoaderAsync'
import { ErrorLoad } from '../../ui/Base/ErrorLoad'

export function KindlError ({ slug }) {
  const [getLabel, { data: { detailGenre } = {}, loading }] = useLazyQuery(
    DETAIL_GENRE,
    { variables: { slug } }
  )

  if (loading) {
    return <LoaderAsync />
  }

  if (detailGenre) {
    return <GeneroSlug kind={detailGenre} />
  }

  return <ErrorLoad recharge={getLabel} />
}
