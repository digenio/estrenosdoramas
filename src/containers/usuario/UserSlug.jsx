import { useLazyQuery } from '@apollo/client'
import { useContext, useEffect } from 'react'
import { Banner } from '../../components/User/Banner'
import { FavoritesList } from '../../components/User/FavoritesList'
import { SeeingList } from '../../components/User/SeeingList'
import { SeeLate } from '../../components/User/SeeLater'
import { SeenList } from '../../components/User/SeenList'
import { FavsContext } from '../../contexts/FavsContext'
import { ListContext } from '../../contexts/ListContext'
import { LIST_FAVS } from '../../gql/favs'
import { getHrefByCategory, getSlugByCategory } from '../../utils/functions'
import { URL_IMAGE_POSTER } from '../../utils/urls'

export const UserSlug = ({ loggedUser }) => {
  const { names, email, username, _id } = loggedUser || {}

  const { favs = [], toggleFav } = useContext(FavsContext)
  const { list, toggleList } = useContext(ListContext)

  const listItems = list.map(item => ({
    name: item.name || item.name_es,
    nameEs: item.name_es,
    image: item.poster || URL_IMAGE_POSTER + item.poster_path,
    category: item.type == 'movie' ? 'Pelicula' : 'Dorama',
    page: getHrefByCategory(item.type),
    link: getSlugByCategory(item.type, item.slug),
    status: item.status
  }))

  const seeLateList =
    listItems.filter(item => item.status === 'Ver despues') || []
  const seenList = listItems.filter(item => item.status === 'Visto') || []
  const seeingList = listItems.filter(item => item.status === 'Viendo') || []

  let countFav = favs.length
  let countSeen = seenList.length
  let countSeeLate = seeLateList.length
  let countSeeing = seeingList.length

  return (
    <>
      <Banner
        username={username}
        name={names}
        fav={countFav}
        seeing={countSeeing}
        seen={countSeen}
      />
      <SeeingList list={seeingList} />
      <FavoritesList list={favs} />
      <SeeLate list={seeLateList} />
      <SeenList list={seenList} />
    </>
  )
}
