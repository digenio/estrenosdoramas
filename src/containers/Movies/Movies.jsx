import { useState } from 'react'
import { useQuery } from '@apollo/client'
import styled from 'styled-components'

import { copy } from '../../utils/functions'
import { URL_IMAGE_POSTER } from '../../utils/urls'
import { Pagination } from '../../components/Pagination/Pagination'
import { MoviesList } from '../../components/Movies/MoviesList'
import { Filters } from '../../components/Filter/Filters'
import { PAGINATION_MOVIES } from '../../gql/movie'
import { LoaderMediaList } from '../../ui/Loaders/LoaderMediaList'

export const Movies = () => {
  const [page, changePage] = useState(1)
  const [sort, changeSort] = useState('CREATEDAT_DESC')
  const [filter, changeFilter] = useState({})

  const perPage = 25

  const variables = {
    perPage,
    sort,
    filter: copy(filter),
    page
  }

  const {
    data: { paginationMovie: { items = [], count = 0 } = {} } = {},
    loading
  } = useQuery(PAGINATION_MOVIES, {
    variables: copy(variables)
  })

  const list = items.map(item => ({
    name: item.name || item.name_es,
    image: item.poster || URL_IMAGE_POSTER + item.poster_path,
    year: item.first_air_date && item.first_air_date.split('-')[0],
    category: 'Movie',
    page: `/peliculas/[slug]`,
    link: `/peliculas/${item.slug}`,
    overview: item.overview,
    genres: item.genres,
    labels: item.labels
  }))

  if (loading) {
    return (
      <Container>
        <Filters
          sort={sort}
          changeSort={changeSort}
          filter={filter}
          changeFilter={changeFilter}
          changePage={changePage}
        />
        <LoaderMediaList title={'movie'} />
      </Container>
    )
  }

  return (
    <Container>
      <Filters
        sort={sort}
        changeSort={changeSort}
        filter={filter}
        changeFilter={changeFilter}
        changePage={changePage}
      />
      <MoviesList list={list} />
      {/* {list.length == 0 ? <FilterNotFound /> : null} */}{' '}
      <Pagination
        page={page}
        perPage={perPage}
        changePage={changePage}
        total={count}
      />
    </Container>
  )
}

const Container = styled.div`
  /* margin: 50px 0; */
  @media ${({ theme }) => theme.devices.laptop} {
    grid-row: 1/-1;
  }
`
