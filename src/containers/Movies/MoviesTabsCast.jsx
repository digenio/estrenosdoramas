import { useState, useEffect } from 'react'

import { DoramasCastsTabs } from '../../components/Doramas/DoramasCastsTabs'
import { ViewCasts } from '../../components/Base/ViewCasts'
import { ViewDetails } from '../../components/Base/ViewDetails'

export const MoviesTabsCast = ({ movie = [] }) => {
  const [current, changeCurrent] = useState('details')
  useEffect(() => {
    changeCurrent('details')
  }, [])

  return (
    <DoramasCastsTabs current={current} changeCurrent={changeCurrent}>
      {current === 'details' ? (
        <ViewDetails dorama={movie} />
      ) : (
        <ViewCasts cast={movie.cast} />
      )}
    </DoramasCastsTabs>
  )
}
