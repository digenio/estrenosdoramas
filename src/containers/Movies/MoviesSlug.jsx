import { useState } from "react";
import { Links } from "../../ui/Base/Links";

import { MoviesTabsCast } from "../Movies/MoviesTabsCast";
import { BanneDetails } from "../../components/Base/BanneDetails";
import { Categories } from "../../ui/Base/Categories/Categories";
import { AsideRating } from "../../containers/Home/AsideRating";
import { PlayerContainer } from "../../components/Player/PlayerContainer";
import { useQuery } from "@apollo/client";
import { DETAIL_EXTRA_MOVIE, DETAIL_MOVIE_LINKS } from "../../gql/movie";
import { Disqus } from "../../components/Disqus/Disqu";
import config from "../../utils/config";

export const MoviesSlug = ({ movie = [] }) => {
  const [server, setServer] = useState();
  const [linksByReports, setLinksByReports] = useState();
  const [languajeCurrent, setLanguajeCurrent] = useState();

  const { data: { detailMovie = [] } = {}, loading: ld1 } = useQuery(DETAIL_EXTRA_MOVIE, {
    variables: { slug: movie.slug },
  });

  const { data: { getMovieLinks = [] } = {}, loading: ld2 } = useQuery(DETAIL_MOVIE_LINKS, {
    variables: { slug: movie.slug, app: config.appAndroid },
  });

  const { name, name_es, backdrop_path, backdrop, overview } = detailMovie;

  const { links_online = [] } = getMovieLinks;

  return (
    <>
      <BanneDetails movie name={name} poster_path={backdrop_path} overview={overview} backdrop={backdrop} />
      <PlayerContainer
        movie
        links={links_online}
        setLinks={setLinksByReports}
        setLanguajeCurrent={setLanguajeCurrent}
        server={server}
      />
      <div>
        <MoviesTabsCast movie={movie} />
        <Disqus _id={movie._id} name={movie.name} />
      </div>
      <Categories slugSlug />
      <AsideRating movie />
    </>
  );
};
