import { useLazyQuery } from '@apollo/client'

import { LoaderAsync } from '../../components/Base/LoaderAsync'

import { DETAIL_MOVIES } from '../../gql/movie'
import { ErrorLoad } from '../../ui/Base/ErrorLoad'

import { MoviesSlug } from './MoviesSlug'

export function MovieError ({ slug }) {
  const [getMovie, { data: { detailMovie } = {}, loading }] = useLazyQuery(
    DETAIL_MOVIES,
    { variables: { slug } }
  )

  if (loading) {
    return <LoaderAsync />
  }

  if (detailMovie) {
    return <MoviesSlug movie={detailMovie} />
  }

  return <ErrorLoad recharge={getMovie} />
}
