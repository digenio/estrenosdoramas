import { useState } from 'react'
import { useQuery } from '@apollo/client'
import styled, { css } from 'styled-components'

import { URL_IMAGE_POSTER } from '../../utils/urls'
import {
  copy,
  getHrefByCategory,
  getSlugByCategory,
  shuffleArray
} from '../../utils/functions'
import { LIST_MOVIES } from '../../gql/movie'
import { LIST_DORAMAS } from '../../gql/dorama'
import { Pagination } from '../../components/Pagination/Pagination'
import { PaisesList } from '../../components/Countries/PaisesList'
import { LoaderMediaList } from '../../ui/Loaders/LoaderMediaList'

export const ListCountrie = ({ slug, slugIverse }) => {
  const [page, changePage] = useState(1)

  const perPage = 20

  const variables = {
    perPage,
    filter: copy({ country: slug }),
    page
  }

  const { data: { listMovies = [] } = {}, loading: ldgMovies } = useQuery(
    LIST_MOVIES,
    { variables, fetchPolicy: 'no-cache' }
  )
  const { data: { listDoramas = [] } = {}, loading: ldgDoramas } = useQuery(
    LIST_DORAMAS,
    { variables, fetchPolicy: 'no-cache' }
  )
  const lengthMedia = listMovies.length + listDoramas.length

  const title = `Doramas y películas de "${slug}"`

  const mixData = shuffleArray(listMovies.concat(listDoramas))

  const pageList = mixData
    .filter((item, idx) => idx >= (page - 1) * perPage && idx < page * perPage)
    .map(item => ({
      name: item.name || item.name_es,
      image: item.poster || URL_IMAGE_POSTER + item.poster_path,
      year: item.release_date && item.release_date.split('-')[0],
      category:
        item.__typename === 'Dorama'
          ? item.isTVShow
            ? 'KShow'
            : 'Dorama'
          : 'Película',
      page: getHrefByCategory(item.__typename),
      link: getSlugByCategory(item.__typename, item.slug),
      overview: item.overview,
      genres: item.genres,
      labels: item.labels
    }))

  if ((ldgMovies || ldgDoramas) && lengthMedia === 0) {
    return (
      <Container slugIverse={slugIverse}>
        <LoaderMediaList title='paises' />
      </Container>
    )
  }
  return (
    <Container slugIverse={slugIverse}>
      <PaisesList list={pageList} slugIverse={slugIverse} name={slug} />
      <Pagination
        page={page}
        total={lengthMedia}
        perPage={perPage}
        changePage={changePage}
      />
    </Container>
  )
}
const Container = styled.div`
  margin: 50px 0;
  @media ${({ theme }) => theme.devices.laptop} {
    grid-row: 1/-1;

    ${({ slugIverse }) =>
      slugIverse &&
      css`
        grid-row: 1/2;
      `}
  }
`
