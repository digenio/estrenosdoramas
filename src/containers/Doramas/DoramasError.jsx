import { LoaderAsync } from '../../components/Base/LoaderAsync'
import { ErrorLoad } from '../../ui/Base/ErrorLoad'
import { DoramasSlug } from './DoramasSlug'
import { useLazyQuery } from '@apollo/client'
import { DETAIL_DORAMA } from '../../gql/dorama'
import { LIST_SEASON } from '../../gql/season'

export function DoramaError ({ slug }) {
  const [
    getDetailDorama,
    { data: { detailDorama } = {}, loading1 }
  ] = useLazyQuery(DETAIL_DORAMA, { variables: { slug } })

  const [
    getSeasonDorama,
    { data: { listSeasons } = {}, loading2 }
  ] = useLazyQuery(LIST_SEASON, { serie_id: detailDorama?._id })

  const handleDoramaData = () => {
    getDetailDorama()
    getSeasonDorama()
  }

  if (loading1 || loading2) {
    return <LoaderAsync />
  }

  if (detailDorama && listSeasons) {
    return <DoramasSlug {...detailDorama} seasons={listSeasons} />
  }

  return <ErrorLoad recharge={handleDoramaData} />
}
