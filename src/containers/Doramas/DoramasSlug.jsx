import { DoramasTabsCast } from '../Doramas/DoramasTabsCast'
import { BanneDetails } from '../../components/Base/BanneDetails'
import { Categories } from '../../ui/Base/Categories/Categories'
import { AsideRating } from '../../containers/Home/AsideRating'
import { Trailer } from '../../ui/Base/Trailer'
import { DoramasTabsList } from '../../containers/Doramas/DoramasTabsList'

export const DoramasSlug = (dorama = []) => {
  const { trailer } = dorama

  return (
    <>
      <BanneDetails {...dorama} dorama={dorama} />

      {trailer ? <Trailer link={trailer} /> : <div></div>}

      <div>
        <DoramasTabsList dorama={dorama} />
        <DoramasTabsCast dorama={dorama} />
      </div>
      <Categories slugSlug />
      <AsideRating serie />
    </>
  )
}
