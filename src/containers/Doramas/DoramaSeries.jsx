import { useState } from 'react'
import { useQuery } from '@apollo/client'
import styled from 'styled-components'

import { copy } from '../../utils/functions'
import { PAGINATION_DORAMAS } from '../../gql/dorama'
import { URL_IMAGE_POSTER } from '../../utils/urls'
import { Pagination } from '../../components/Pagination/Pagination'
import { DoramasList } from '../../components/Doramas/DoramasList'
import { Filters } from '../../components/Filter/Filters'
import { LoaderMediaList } from '../../ui/Loaders/LoaderMediaList'
import { FilterNotFound } from '../../ui/Filters/FilterNotFound'

export const DoramasSeries = () => {
  const [page, changePage] = useState(1)
  const [sort, changeSort] = useState('CREATEDAT_DESC')
  const [filter, changeFilter] = useState({})

  const perPage = 25

  const variables = {
    perPage,
    sort,
    filter: copy(filter),
    page
  }

  const {
    data: { paginationDorama: { items = [], count = 0 } = {} } = {},
    loading
  } = useQuery(PAGINATION_DORAMAS, {
    variables: copy(variables)
  })

  const list = items.map(item => ({
    name: item.name || item.name_es,
    type: item.__typename,
    image: item.poster || URL_IMAGE_POSTER + item.poster_path,
    year: item.first_air_date && item.first_air_date.split('-')[0],
    season: item.number_of_seasons,
    category: 'Dorama',
    page: `/doramas/[slug]`,
    link: `/doramas/${item.slug}`,
    overview: item.overview,
    genres: item.genres,
    labels: item.labels,
    createBy: item.created_by,
    country: item.country
  }))

  if (loading) {
    return (
      <Container>
        <Filters
          sort={sort}
          changeSort={changeSort}
          filter={filter}
          changeFilter={changeFilter}
          changePage={changePage}
        />
        <LoaderMediaList title={'dorama'} />
      </Container>
    )
  }
  return (
    <Container>
      <Filters
        sort={sort}
        changeSort={changeSort}
        filter={filter}
        changeFilter={changeFilter}
        changePage={changePage}
      />
      <DoramasList list={list} />
      {list.length == 0 ? <FilterNotFound /> : null}
      <Pagination
        page={page}
        perPage={perPage}
        changePage={changePage}
        total={count}
      />
    </Container>
  )
}

const Container = styled.div`
  @media ${({ theme }) => theme.devices.laptop} {
    grid-row: 1/-1;
  }
`
