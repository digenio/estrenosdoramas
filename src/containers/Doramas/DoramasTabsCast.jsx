import { useState, useEffect } from 'react'
import { DoramasCastsTabs } from '../../components/Doramas/DoramasCastsTabs'
import { ViewCasts } from '../../components/Base/ViewCasts'
import { ViewDetails } from '../../components/Base/ViewDetails'

export const DoramasTabsCast = ({ dorama = [] }) => {
  const [current, changeCurrent] = useState('details')
  useEffect(() => {
    changeCurrent('details')
  }, [])

  return (
    <DoramasCastsTabs current={current} changeCurrent={changeCurrent}>
      {current === 'details' ? (
        <ViewDetails dorama={dorama} />
      ) : (
        <ViewCasts cast={dorama.cast} />
      )}
    </DoramasCastsTabs>
  )
}
