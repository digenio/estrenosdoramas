import { useLazyQuery, useMutation } from "@apollo/client";
import { useContext, useEffect, useState } from "react";

import { PlayerContainer } from "../../components/Player/PlayerContainer";
import { AsideRating } from "../../containers/Home/AsideRating";
import { UserContext } from "../../contexts/UserContext";
import { EPISODE_LINKS } from "../../gql/episode";
import { ADD_SEEN } from "../../gql/seen";
import { Disqus } from "../../components/Disqus/Disqu";
import config from "../../utils/config";
import { Player } from "../../components/Player/Player";

export const ChapterSlug = ({ chapter = [], extra = [] }) => {
  const { air_date, serie_name, serie_id, _id } = chapter;
  const [getExtraMovie, { loading: ld1, data: { getEpisodeLinks = {} } = {} }] = useLazyQuery(EPISODE_LINKS, {
    fetchPolicy: "no-cache",
    nextFetchPolicy: "network-only",
  });

  const { links_online = [] } = getEpisodeLinks;

  const { loggedUser } = useContext(UserContext);
  const [seenEpisode] = useMutation(ADD_SEEN);

  const [server, setServer] = useState();

  useEffect(() => {
    if (_id) {
      getExtraMovie({ variables: { id: _id, app: config.appAndroid } });
    }
  }, [_id]);

  useEffect(() => {
    if (loggedUser) {
      seenEpisode({
        variables: {
          record: {
            seen_id: _id,
            group_id: serie_id,
            user_id: loggedUser._id,
            platform: config.app,
            seen_type: "episode",
          },
        },
      });
    }
  }, [_id, loggedUser]);

  const langExtra = links_online.map((item) => {
    return item.lang;
  });

  const langOffDuplicate = [...new Set(langExtra)];

  const date = new Date();
  date.setDate(date.getDate() - 1);
  date.setHours(0, 0, 0, 0);
  // premiere
  const air = air_date
    ? new Date(air_date.replace("T00:00:00.000Z", "T10:00:00.000Z").replace("T05:00:00.000Z", "T10:00:00.000Z"))
    : undefined;

  const isBeforeDate = links_online.length > 0 ? false : air && air.getTime() >= date.getTime();

  return (
    <>
      <div>
        <PlayerContainer
          chapter={chapter}
          extra={extra}
          links={links_online}
          isBeforeDate={isBeforeDate}
          air={air}
          setServer={setServer}
        />
        <Disqus _id={_id} name={serie_name} />
      </div>
      <AsideRating serie chapterSlug />
    </>
  );
};
