import styled, { css } from 'styled-components'
import { useState, useEffect } from 'react'
import { useQuery } from '@apollo/client'
import { URL_IMAGE_POSTER } from '../../utils/urls'
import { LAST_EPISODES } from '../../gql/episode'
import { usePagination } from '../../hooks/usePagination'
import { ChapterList } from '../../components/Episode/ChapterList'

export const EpisodesList = ({ slugIverse }) => {
  const [page, changePage] = useState(1)
  const [sort, changeSort] = useState('CREATEDAT_DESC')
  const [filter, changeFilter] = useState({})

  const perPage = 15

  const {
    data: { premiereEpisodes = [] } = {},
    loading,
    refetch,
    error
  } = useQuery(LAST_EPISODES, {
    variables: { limit: 200 }
  })

  useEffect(() => {
    if (premiereEpisodes.length) {
      refetch && refetch()
    }
  }, [])

  const list = premiereEpisodes.map(item => ({
    name: item.serie_name || item.serie_name_es,
    image: item.poster || URL_IMAGE_POSTER + item.serie_poster,
    year: item?.air_date && item?.air_date.split('T')[0],
    season_number: item.season_number,
    episode_number: item.episode_number,
    category: 'Episodio',
    page: `/capitulos/[slug]`,
    link: `/capitulos/${item.slug}`
  }))
  const [newData, ButtonsPagination] = usePagination({
    data: list,
    initialCurrentPage: page,
    postPerPageInitial: perPage
  })

  return (
    <Container slugIverse={slugIverse}>
      <ChapterList list={newData} />
      <ButtonsPagination />
    </Container>
  )
}

const Container = styled.div`
  margin: 50px 0;
  @media ${({ theme }) => theme.devices.laptop} {
    grid-row: 1/-1;

    ${({ slugIverse }) =>
      slugIverse &&
      css`
        grid-row: 1/2;
      `}
  }
`
