import { useLazyQuery, useQuery } from '@apollo/client'

import { LoaderAsync } from '../../components/Base/LoaderAsync'

import { ChapterSlug } from './ChapterSlug'
import { EPISODE_DETAIL, EPISODE_DETAIL_EXTRA } from '../../gql/episode'
import { ErrorLoad } from '../../ui/Base/ErrorLoad'

export function ChapterError ({ slug }) {
  const { data: { detailEpisode = {} } = {}, load1 } = useQuery(
    EPISODE_DETAIL,
    {
      variables: { slug }
    }
  )

  const [getChapter, { load2, data = {} }] = useLazyQuery(
    EPISODE_DETAIL_EXTRA,
    {
      variables: {
        slug: detailEpisode?.serie_slug,
        episode_id: detailEpisode?._id
      }
    }
  )

  if (load1 || load2) {
    return <LoaderAsync />
  }

  if (detailEpisode && data) {
    return <ChapterSlug chapter={detailEpisode} extra={data} />
  }

  return <ErrorLoad recharge={getChapter} />
}
