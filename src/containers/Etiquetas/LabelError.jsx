import { useLazyQuery } from '@apollo/client'

import { LoaderAsync } from '../../components/Base/LoaderAsync'
import { DETAIL_LABEL } from '../../gql/label'
import { LabelSlug } from './LabelSlug'

export function LabelError ({ slug }) {
  const [getLabel, { data: { detailLabel } = {}, loading }] = useLazyQuery(
    DETAIL_LABEL,
    { variables: { slug } }
  )

  if (loading) {
    return <LoaderAsync />
  }

  if (detailLabel) {
    return <LabelSlug label={detailLabel} />
  }

  return (
    <div>
      <span>Ocurrio un error</span>
      <a onClick={() => getLabel()}>recargar</a>
    </div>
  )
}
