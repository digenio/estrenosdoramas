import { Categories } from '../../ui/Base/Categories/Categories'
import { AsideRating } from '../Home/AsideRating'
import { TagList } from '../Etiquetas/TagList'

export const LabelSlug = ({ label }) => {
  return (
    <>
      <TagList {...label} slugIverse />
      <Categories slugIverse slug />
      <AsideRating movie slugIverse />
    </>
  )
}
