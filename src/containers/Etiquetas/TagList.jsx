import { useQuery } from '@apollo/client'
import { useState } from 'react'
import { LabelSlugList } from '../../components/Etiquetas/LabelSlugList'

import { Pagination } from '../../components/Pagination/Pagination'
import { LABEL_DORAMAS_LIST, LABEL_PELICULAS_LIST } from '../../gql/label'
import { mixArray } from '../../utils/functions'
import { URL_IMAGE_POSTER } from '../../utils/urls'
import { getSlugByCategory, getHrefByCategory } from '../../utils/functions'
import styled from 'styled-components'
import { LoaderMediaList } from '../../ui/Loaders/LoaderMediaList'

export const TagList = ({ name, _id, slugIverse }) => {
  const perPage = 20

  const [page, changePage] = useState(1)

  const variables = {
    labelId: _id
  }

  const { data: { listMovies = [] } = {}, loading: ldgMovies } = useQuery(
    LABEL_PELICULAS_LIST,
    { variables }
  )
  const { data: { listDoramas = [] } = {}, loading: ldgDoramas } = useQuery(
    LABEL_DORAMAS_LIST,
    { variables }
  )

  const length = listMovies.length + listDoramas.length
  const list = mixArray(listMovies, listDoramas, length)

  const pageList = list
    .filter((item, idx) => idx >= (page - 1) * perPage && idx < page * perPage)
    .map(item => ({
      name: item.name || item.name_es,
      image: item.poster || URL_IMAGE_POSTER + item.poster_path,
      year: item.release_date && item.release_date.split('-')[0],
      category:
        item.__typename === 'Dorama'
          ? item.isTVShow
            ? 'KShow'
            : 'Dorama'
          : 'Película',
      page: getHrefByCategory(item.__typename),
      link: getSlugByCategory(item.__typename, item.slug),
      overview: item.overview,
      genres: item.genres,
      labels: item.labels
    }))
  if ((ldgMovies || ldgDoramas) && length === 0) {
    return (
      <Container>
        <LoaderMediaList title={'tags'} />
      </Container>
    )
  }
  return (
    <Container>
      <LabelSlugList list={pageList} name={name} />
      <Pagination
        page={page}
        perPage={perPage}
        changePage={changePage}
        total={length}
      />
    </Container>
  )
}

const Container = styled.div`
  margin: 50px 0;
  @media ${({ theme }) => theme.devices.laptop} {
    grid-row: 1/-1;

    ${({ slugIverse }) =>
      slugIverse &&
      css`
        grid-row: 1/2;
      `}
  }
`
