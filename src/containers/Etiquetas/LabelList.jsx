import { useContext } from 'react'
import styled from 'styled-components'

import { DropItemsWrapper } from '../../components/Base/DropItemsWrapper'
import { LabelContext } from '../../contexts/LabelContext'
import { DropItemList } from '../../ui/Base/DropItemList'

export const LabelList = () => {
  const { labels } = useContext(LabelContext)

  const labelsTransfor = labels.map(list => {
    return {
      name: list.name,
      slug: list.slug,
      imagen: list.platforms[0].image_default,
      id: list._id
    }
  })
  return (
    <>
      <Title>Generos</Title>
      <DropItemsWrapper>
        {labelsTransfor.map(({ name, slug, imagen, id }) => {
          return (
            <DropItemList
              key={id}
              name={name}
              slug={slug}
              imagen={imagen}
              path='generos'
            />
          )
        })}
      </DropItemsWrapper>
    </>
  )
}

const Title = styled.h2`
  color: black;
  font-size: 2.3rem;
  margin: 20px 20px;
`
