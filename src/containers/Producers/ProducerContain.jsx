import { useState, useEffect } from 'react'
import { useQuery } from '@apollo/client'

import { URL_IMAGE_POSTER } from '../../utils/urls'
import { ProducerList } from '../../components/Producers/ProducerList'
import {
  copy,
  getHrefByCategory,
  getSlugByCategory,
  shuffleArray
} from '../../utils/functions'
import { LIST_MOVIES } from '../../gql/movie'
import { LIST_DORAMAS } from '../../gql/dorama'
import { LoaderMediaList } from '../../ui/Loaders/LoaderMediaList'
import { Pagination } from '../../components/Pagination/Pagination'
import styled from 'styled-components'

export const ProducerContain = ({ name, _id }) => {
  const [page, changePage] = useState(1)

  const perPage = 20

  const variables = {
    perPage,
    filter: copy({ networkId: _id }),
    page
  }

  const { data: { listMovies = [] } = {}, loading: ldgMovies } = useQuery(
    LIST_MOVIES,
    { variables }
  )
  const { data: { listDoramas = [] } = {}, loading: ldgDoramas } = useQuery(
    LIST_DORAMAS,
    { variables }
  )
  const lengthMedia = listMovies.length + listDoramas.length

  const title = `Doramas y peliculas de "${name}"`

  const mixData = shuffleArray(listMovies.concat(listDoramas))

  const pageList = mixData
    .filter((item, idx) => idx >= (page - 1) * perPage && idx < page * perPage)
    .map(item => ({
      name: item.name || item.name_es,
      image: item.poster || URL_IMAGE_POSTER + item.poster_path,
      year: item.release_date && item.release_date.split('-')[0],
      category:
        item.__typename === 'Dorama'
          ? item.isTVShow
            ? 'KShow'
            : 'Dorama'
          : 'Película',
      page: getHrefByCategory(item.__typename),
      link: getSlugByCategory(item.__typename, item.slug),
      overview: item.overview,
      genres: item.genres,
      labels: item.labels
    }))

  if ((ldgMovies || ldgDoramas) && lengthMedia === 0) {
    return (
      <Container>
        <LoaderMediaList title='producers' />
      </Container>
    )
  }

  return (
    <div>
      <Container>
        <ProducerList list={pageList} name={name} />
        <Pagination
          page={page}
          perPage={perPage}
          changePage={changePage}
          total={lengthMedia}
        />
      </Container>
    </div>
  )
}
const Container = styled.div`
  margin: 50px 0;
  @media ${({ theme }) => theme.devices.laptop} {
    grid-row: 1/-1;

    ${({ slugIverse }) =>
      slugIverse &&
      css`
        grid-row: 1/2;
      `}
  }
`
