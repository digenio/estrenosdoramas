import { useLazyQuery, useQuery } from '@apollo/client'
import { useContext, useEffect, useState } from 'react'

import { TemporadasSeries } from '../../components/Temporada/TemporadasSeries'
import { SEASON_DETAIL_EXTRA } from '../../gql/season'
import { DETAIL_DORAMA } from '../../gql/dorama'
import { LIST_SEEN } from '../../gql/seen'
import { UserContext } from '../../contexts/UserContext'
import { Categories } from '../../ui/Base/Categories/Categories'
import { AsideRating } from '../Home/AsideRating'

export const TemporadaSlug = ({ season }) => {
  const [seasonTab, setSeason] = useState(1)
  const { loggedUser } = useContext(UserContext)

  const { load1, err1, data: { listEpisodes, listSeasons } = [] } = useQuery(
    SEASON_DETAIL_EXTRA,
    {
      variables: {
        slug: season?.serie_slug,
        season_number: seasonTab
      }
    }
  )
  const { load2, err2, data: { detailDorama } = [] } = useQuery(DETAIL_DORAMA, {
    variables: {
      slug: season?.serie_slug
    }
  })

  const [getSeen, { data: { listSeen = [] } = {}, refetch }] = useLazyQuery(
    LIST_SEEN,
    {
      fetchPolicy: 'no-cache'
    }
  )

  useEffect(() => {
    if (loggedUser && season) {
      getSeen({
        variables: { group_id: season?.serie_id, user_id: loggedUser._id }
      })
    }
  }, [season?._id, loggedUser])

  if (err2 || err1) {
    return <DoramaError />
  }

  if (load1 || load2) {
    return <LoaderAsync />
  }

  return (
    <>
      <TemporadasSeries
        listEpisodes={listEpisodes}
        listSeasons={listSeasons}
        seasonNumber={seasonTab}
        changeSeason={setSeason}
        listSeen={listSeen}
        refetch={refetch}
      />
      <Categories slug slugIverse />
      <AsideRating serie slugIverse />
    </>
  )
}
