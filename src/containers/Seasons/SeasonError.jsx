import { useLazyQuery } from '@apollo/client'

import { LoaderAsync } from '../../components/Base/LoaderAsync'

import { TemporadaSlug } from './TemporadaSlug'
import { SEASON_DETAIL } from '../../gql/season'
import { ErrorLoad } from '../../ui/Base/ErrorLoad'

export function SeasonError ({ slug }) {
  const [getSeason, { loading, data: { detailSeason } = [] }] = useLazyQuery(
    SEASON_DETAIL,
    {
      variables: { slug }
    }
  )

  if (loading) {
    return <LoaderAsync />
  }

  if (detailSeason) {
    return <TemporadaSlug season={detailSeason} />
  }

  return <ErrorLoad recharge={getSeason} />
}
