import styled from 'styled-components'

import { FadeIn } from '../../styles/animations'
import { DetailSeriesChapter as Chapter } from '../../ui/Details/DetailSeriesChapter'

export const DoramasChapterList = ({
  listEpisodes = [],
  listSeen = [],
  refetch
}) => {
  return (
    <ContentPlayerSeason>
      {listEpisodes.map((chapter, idx) => (
        <Chapter
          key={idx}
          href={`/capitulos/[slug]`}
          as={`/capitulos/${chapter.slug}`}
          {...chapter}
          listSeen={listSeen}
          refetch={refetch}
        />
      ))}
    </ContentPlayerSeason>
  )
}

const ContentPlayerSeason = styled.div`
  animation: ${FadeIn} 0.4s;
  transition: all 1s;
  animation-fill-mode: both;
`
