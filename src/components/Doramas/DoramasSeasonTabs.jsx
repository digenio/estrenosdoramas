import { useEffect } from 'react'
import styled, { css } from 'styled-components'
import { TabItem } from '../../ui/Tabs/TabItem'
import { Tabs } from '../../ui/Tabs/Tabs'

export const DoramasSeasonTabs = ({
  season,
  setSeason,
  changePage,
  children,
  dorama
}) => {
  const { seasons = [1, 2, 3, 4] } = dorama || {}

  useEffect(() => {
    setSeason(1)
    changePage(1)
  }, [dorama?._id])

  // if (!dorama) {
  //   return null
  // }
  return (
    <Sidebar trailer={dorama?.trailer}>
      <div>
        <div>
          <Player>
            <Tabs sideBar>
              {seasons.map((item, idx) => {
                return (
                  <TabItem
                    key={idx}
                    id={item.season_number}
                    label='t1'
                    value={season}
                    onChange={setSeason}
                    onClick={() => changePage(1)}
                    sideBar
                    button
                  >
                    T{item.season_number}
                  </TabItem>
                )
              })}
            </Tabs>
            {children}
          </Player>
        </div>
      </div>
    </Sidebar>
  )
}

const Sidebar = styled.article`
  position: relative;
  grid-row: span 2 / span 2;


  ${({ trailer }) =>
    !trailer &&
    css`
     @media ${({ theme }) => theme.devices.laptop} {
     margin-top: -600px;
  }
    
    `}
`
const Player = styled.div`
  padding: 5px;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
`
