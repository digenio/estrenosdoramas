import { CategoriesList } from '../../layouts/CategoriesList'
import { MediaItem } from '../../ui/Media/MediaItem'
import MediaContainer from '../../layouts/MediaContainer'

export const DoramasList = ({ list = [] }) => {
  return (
    <>
      <CategoriesList categorie='Doramas'>
        <MediaContainer>
          {list.map((item, idx) => (
            <MediaItem key={idx} {...item} />
          ))}
        </MediaContainer>
      </CategoriesList>
    </>
  )
}
