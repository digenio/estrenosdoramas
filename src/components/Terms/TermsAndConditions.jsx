import styled from 'styled-components'
import config from '../../utils/config'

export const TermsAndConditions = () => {
  return (
    <Terms>
      <h1>Términos y condiciones</h1>

      <p>
        El uso de {config.name}, están sujetos a los siguientes términos y al
        momento de acceder o utilizar {config.name}, usted acepta estos términos
        y todas las leyes de reglamentos aplicables. Si usted no está de acuerdo
        con cualquiera de estos términos, está prohibido el uso o acceso a
        {config.name}. Los materiales contenidos en {config.name} pueden están
        protegidos por derechos de autor, derecho de marcas de logotipos,
        marcas, nombres comerciales y las imágenes utilizadas en este sitio son
        propiedad de sus respectivos titulares de derechos de autor.
      </p>
      <p>Mediante el uso de {config.name} usted acepta que:</p>

      <ul>
        <li>
          Para utilizar {config.name} debe tener 15 años de edad o más. Si usted
          es menor de 15 años no se le permite tener acceso o utilizarla sin
          supervisión de sus padres o representantes. {config.name} no aloja
          ningún vídeo presentado y no se hace responsable de posibles daños en
          su equipo por la visualización o descarga de estos archivos.
          {config.name} se reserva el derecho de suspender el acceso a la página
          web o cualquier contenido o información publicada o que se haya
          generado en esta, así como cancelar, prohibir la entrada o el uso de
          la página web, con o sin previo aviso ni razón necesaria a su sola
          elección. El uso no autorizado de {config.name} como tratar de acceder
          a paneles administrativos, tratar de vulnerar la página web, etc. será
          penalizado con un bloqueo de IP.
        </li>
      </ul>
      <p>
        Estos términos de uso, están sujetos a posibles cambios sin previo
        aviso.
      </p>
    </Terms>
  )
}

const Terms = styled.section`
  padding: 10px 40px;
  color: ${({ theme }) => theme.colors.lighGray};
  font-size: 0.8rem;

  @media ${({ theme }) => theme.devices.laptop} {
    font-size: 1rem;
  }
  h1 {
    color: white;
    font-size: 40px;
    margin-bottom: 10px;
  }
  ul {
    padding: 0 40px;
    margin-top: 20px;
    margin-bottom: 20px;

    li {
      line-height: 25px;
      margin: 10px 0;
    }
  }

  p {
    line-height: 25px;
  }
`
