import { CategorieUserList } from './CategorieUserList'

import { getHrefByCategory, getSlugByCategory } from '../../utils/functions'
import { URL_IMAGE_POSTER } from '../../utils/urls'
import { CarouselUser } from '../Carousels/CarouselUser'

export const FavoritesList = ({ list = [] }) => {
  if (list.length === 0) {
    return <CategorieUserList categorie='Favorito' empty />
  }

  const listItems = list.map(item => ({
    name: item.name || item.name_es,
    nameEs: item.name_es,
    image: item.poster || URL_IMAGE_POSTER + item.poster_path,
    year: item.first_air_date && item.first_air_date.split('-')[0],
    category: item.type == 'movie' ? 'Pelicula' : 'Dorama',
    page: getHrefByCategory(item.type),
    link: getSlugByCategory(item.type, item.slug)
  }))

  return <CarouselUser categorie='Favoritos' list={listItems} />
}
