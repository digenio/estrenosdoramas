import { LayoutGrid } from '../../layouts/LayoutGrid'
import { CategorieUserList } from './CategorieUserList'
import { CarouselUser } from '../Carousels/CarouselUser'
export const SeeingList = ({ list = [] }) => {
  if (list.length === 0) {
    return <CategorieUserList categorie='Viendo' empty />
  }
  return <CarouselUser categorie='Viendo' list={list} />
}
