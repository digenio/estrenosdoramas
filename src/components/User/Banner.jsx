import styled from 'styled-components'
import { AiFillStar, AiFillPlayCircle, AiFillEye } from 'react-icons/ai'
export const Banner = ({ name, username, fav = 5, seen, seeing }) => {
  return (
    <BannerUser
      className='banner'
      style={{
        backgroundImage: `url(/banner.jpeg)`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center top'
      }}
    >
      <BannerUID className='banner__container'>
        <h1>{name}</h1>
        <h2>{`${username}`}</h2>
        <figure>
          <img src='/user.png' alt='' />
        </figure>

        <Highlight>
          <span>
            <AiFillStar color={'white'} size={20} /> <p>{fav}</p>
          </span>
          <span>
            <AiFillPlayCircle color={'white'} size={20} /> <p>{seeing}</p>
          </span>
          <span>
            <AiFillEye color={'white'} size={20} /> <p>{seen}</p>
          </span>
        </Highlight>
      </BannerUID>
    </BannerUser>
  )
}

const BannerUser = styled.section`
  position: relative;
  height: 400px;
  width: 100%;
  object-fit: contain;
  user-select: none;
  margin-top: 50px;

  color: white;
  border-radius: 5px;
  margin-bottom: 40px;

  :before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    background: linear-gradient(transparent 90%, $dark-grey 100%);

    box-shadow: 15px 15px 20px #000000;
  }
`

const BannerUID = styled.div`
  max-width: 400px;
  max-height: 300px;
  min-height: 200px;
  min-width: 200px;
  padding: 20px;
  width: 70%;
  height: 100%;
  position: absolute;
  top: 8%;
  left: 0;
  right: 0%;
  margin: auto;

  border-radius: 5px;
  background-color: black;
  opacity: 0.85;

  h1 {
    font-size: 16px;
    text-align: center;

    @media ${({ theme }) => theme.devices.laptop} {
      font-size: 24px;
    }
  }
  h2 {
    font-size: 14px;
    text-align: center;
    color: ${({ theme }) => theme.colors.details};
    @media ${({ theme }) => theme.devices.laptop} {
      font-size: 18px;
    }
  }

  figure {
    width: 100px;
    margin: 20px auto;
    border-radius: 100%;
    overflow: hidden;
    img {
      object-fit: cover;
    }
  }

  @media ${({ theme }) => theme.devices.laptop} {
    left: 10%;
    margin: 0;
  }
`

const Highlight = styled.div`
  display: flex;
  justify-content: center;
  span {
    display: flex;
    margin-left: 10px;
    align-items: center;

    p {
      margin-left: 10px;
    }
  }
`
