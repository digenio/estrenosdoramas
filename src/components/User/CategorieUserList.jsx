import { CategoriesList } from '../../layouts/CategoriesList'
import { MediaItem } from '../../ui/Media/MediaItem'
import MediaContainer from '../../layouts/MediaContainer'
import { CategorieEmpty } from './CategorieEmpty'

export const CategorieUserList = ({ list = [], categorie, empty }) => {
  if (empty) {
    return <CategorieEmpty categorie={categorie} />
  }
  return (
    <>
      <CategoriesList categorie={categorie}>
        <MediaContainer>
          {list.map((item, idx) => (
            <MediaItem key={idx} {...item} carousel />
          ))}
        </MediaContainer>
      </CategoriesList>
    </>
  )
}
