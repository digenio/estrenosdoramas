import styled from 'styled-components'
import Link from 'next/link'
import { FaUserCircle } from 'react-icons/fa'

export const NavProfile = ({
  username,
  names = 'Jesus Hernandez',
  email,
  logout
}) => {
  return (
    <Profile>
      <IconUser>
        <FaUserCircle color={'white'} size={45} />
      </IconUser>

      <OptionsUser>
        <OptionItemName>{names}</OptionItemName>
        <Division></Division>
        <Link href={`/usuario/[slug]`} as={`/usuario/${username}`}>
          <a>
            <OptionItem onClick={() => {}}>Perfil</OptionItem>
          </a>
        </Link>

        <Division></Division>
        <Link href={`/cuenta/`}>
          <a>
            <OptionItem>Editar cuenta</OptionItem>
          </a>
        </Link>

        <Division></Division>
        <OptionItem
          onClick={() => {
            logout()
          }}
        >
          Cerrar Sesion
        </OptionItem>
      </OptionsUser>
    </Profile>
  )
}

const OptionsUser = styled.div`
  display: none;
  right: 5%;
  position: absolute;
  width: 150px;

  /* margin-top: 10px; */
  border-radius: 5px;
  background-color: ${({ theme }) => theme.colors.bodyH};
`
const IconUser = styled.figure`
  cursor: pointer;
  margin: 10px 0;
`
const Profile = styled.div`
  position: relative;
  margin: 5px;
  :hover ${OptionsUser} {
    display: block;
  }
`

const Division = styled.div`
  width: 100%;
  height: 2px;
  opacity: 0.6;
  border-radius: 5px;
  background-color: ${({ theme }) => theme.colors.white};
`
const OptionItem = styled.span`
  display: inline-block;
  color: ${({ theme }) => theme.colors.black};
  margin-bottom: 2px solid red;
  padding: 10px;
  cursor: pointer;
  border-radius: 5px;
  width: 100%;
  user-select: none;
  :hover {
    background-color: ${({ theme }) => theme.colors.primary};
    color: ${({ theme }) => theme.colors.white};
    opacity: 0.6;
  }
`

const OptionItemName = styled.span`
  display: inline-block;
  color: ${({ theme }) => theme.colors.black};
  margin-bottom: 2px solid red;
  padding: 10px;
  border-radius: 5px;
  width: 100%;
  user-select: none;
`
