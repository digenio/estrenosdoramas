import { CarouselUser } from '../Carousels/CarouselUser'
import { CategorieUserList } from './CategorieUserList'

export const SeeLate = ({ list = [] }) => {
  if (list.length === 0) {
    return <CategorieUserList categorie='Ver Despues' empty />
  }

  return <CarouselUser categorie='Ver despues' list={list} />
}
