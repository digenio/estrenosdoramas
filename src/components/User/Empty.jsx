import styled from 'styled-components'
import Img from 'react-cool-img'

export const Empty = () => {
  return (
    <ContainerEmpty>
      <figure>
        <Img src={`/Empty.svg`}></Img>
      </figure>
      <p>Aun no hay elementos agregados a esta lita.</p>
    </ContainerEmpty>
  )
}

const ContainerEmpty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 20px;
  width: 100%;
  height: 100%;
  min-height: 200px;
  margin-bottom: 30px;
  background-color: ${({ theme }) => theme.colors.bodyH};
  border-radius: 5px;

  figure {
    width: 100%;
    max-width: 200px;
    margin-bottom: 10px;
  }
  p {
    color: black;
    font-size: 12px;
    -webkit-user-select: none; /* Safari */
    -moz-user-select: none; /* Firefox */
    -ms-user-select: none; /* IE10+/Edge */
    user-select: none; /* Standard */

    @media ${({ theme }) => theme.devices.laptop} {
      font-size: 20px;
    }
  }
`
