import { CarouselUser } from '../Carousels/CarouselUser'
import { CategorieUserList } from './CategorieUserList'

export const SeenList = ({ list = [] }) => {
  if (list.length === 0) {
    return <CategorieUserList categorie='Visto' empty />
  }

  return <CarouselUser categorie='Visto' list={list} />
}
