import styled from 'styled-components'
import { Empty } from './Empty'

export const CategorieEmpty = ({ categorie }) => {
  return (
    <Categorie>
      <TitleCategorie>{categorie}</TitleCategorie>
      <Empty />
    </Categorie>
  )
}

const Categorie = styled.div``

const TitleCategorie = styled.h2`
  color: ${({ theme }) => theme.colors.black};
  margin-right: 1rem;
  font-size: 30px;
  font-weight: 700;
  text-transform: capitalize;
`
