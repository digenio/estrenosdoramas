import { ProducerContain } from '../../containers/Producers/ProducerContain'
import { Categories } from '../../ui/Base/Categories/Categories'
import { AsideRating } from '../../containers/Home/AsideRating'

export const ProcucerSlug = ({ network }) => {
  return (
    <>
      <ProducerContain {...network} slugIverse />
      <Categories slugIverse slug />
      <AsideRating movie slugIverse />
    </>
  )
}
