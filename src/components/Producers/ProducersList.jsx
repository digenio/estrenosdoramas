import { useContext } from 'react'
import styled from 'styled-components'

import { DropItemList } from '../../ui/Base/DropItemList'

import { DropItemsWrapper } from '../../components/Base/DropItemsWrapper'
import { NetworkContext } from '../../contexts/NetworkContext'

export const ProducersList = () => {
  const { networks } = useContext(NetworkContext)

  const producerTransfor = networks.map(list => {
    return {
      name: list.name,
      slug: list.slug,
      imagen: list.platforms[0].image_default,
      id: list._id
    }
  })
  return (
    <>
      <Title>Productoras</Title>
      <DropItemsWrapper>
        {producerTransfor.map(({ name, slug, imagen, id }) => {
          return (
            <DropItemList
              key={id}
              name={name}
              slug={slug}
              imagen={imagen}
              path='productoras'
            />
          )
        })}
      </DropItemsWrapper>
    </>
  )
}

const Title = styled.h2`
  color: black;
  font-size: 2.3rem;
  margin: 20px 20px;
`
