import config from '../../utils/config'
import HeadNext from 'next/head'
import { useRouter } from 'next/router'

export function Head ({ title, description, image, url, extra, noFolow }) {
  const router = useRouter()

  const defaulTitle = `${config.name}【 ▷ 】Ver Estrenos Doramas Online Gratis HD`
  const defaulDescription = `En ✓ ${config.name} ✓ puedes ver los estrenos doramas y peliculas asiáticas gratis HD. Estrenos doramas, capitulos y Temporadas Completas en HD`

  return (
    <HeadNext>
      <title>{title || defaulTitle}</title>
      <meta name='description' content={description || defaulDescription} />
      <meta property='og:title' content={title || defaulTitle} />
      <meta
        property='og:description'
        content={description || defaulDescription}
      />
      <link rel='canonical' href={url || config.url} />
      <meta property='og:url' content={url || config.url + router.asPath} />
      <meta property='og:image' content={image || `${config.url}/meta.jpeg`} />
      <meta name='twitter:card' content={image || `${config.url}/meta.jpeg`} />
      <meta name='twitter:title' content={title || defaulTitle} />
      <meta
        name='twitter:description'
        content={description || defaulDescription}
      />
      <meta
        name='viewport'
        content='width=device-width,minimum-scale=1,initial-scale=1'
      />
      <script async src={`https://arc.io/widget.min.js#${config.arc}`}></script>
      {extra}
      <link rel='icon' href={`${config.url}/favicon1.ico`} />
      <meta
        name='google-play-app'
        content={`app-id=com.playgo.${config.app}`}
      />
      <meta charSet='UTF-8' />
      <meta name='theme-color' content={config.color} />

      <meta property='og:type' content='website' />
      <meta property='og:site_name' content={config.name} />
      <meta property='og:locale' content='es_MX' />
      <meta property='og:type' content='website' />
      {noFolow ? (
        <meta name='robots' content='nofollow, noindex' />
      ) : (
        <meta name='robots' content='follow, index' />
      )}
    </HeadNext>
  )
}
