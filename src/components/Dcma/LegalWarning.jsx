import styled from 'styled-components'
import config from '../../utils/config'

export const LegalWarning = () => {
  return (
    <Warning>
      <Dcma>DMCA</Dcma>
      <WarningTittle>AVISO LEGAL</WarningTittle>
      <p>
        {config.url} está de acuerdo con 17 USC § 512 y la Digital Millennium
        Copyright Act (“DMCA”). Es nuestra política responder a cualquier
        notificación de infracción y tomar las acciones apropiadas bajo la
        Digital Millennium Copyright Act (“DMCA”) y otras leyes de propiedad
        intelectual aplicables.
      </p>
      <ol>
        <li>
          Los enlaces que figuran en esta página web han sido encontrados en
          páginas de la índole de Youtube, Digiload.co, Mystream.to, Uqload.com,
          Fembed.com, Tu.tv, Openload.co, Bigfile.to, Streamcloud.eu,
          allmyvideos.net, 1fichier.com, streamin.to, hugefiles.net,
          powvideo.net, uptobox.com, flashx.tv, ul.to, hqq.tv, mp4upload.com,
          yourupload.com, nowvideo.sx, etc. Y desconocemos si los mismos tienen
          contratos de cesión de derechos sobre estos videos para reproducirlos,
          alojarlos o permitir su descarga.
        </li>
        <li>
          Todas las marcas aquí mencionadas y logos están registrados por sus
          legítimos propietarios y solamente se emplean en referencia a las
          mismas y con un fin de cita o comentario, de acuerdo con el articulo
          32 LPI.
        </li>
        <li>
          No nos hacemos responsables del uso indebido que puedas hacer del
          contenido de nuestra página.
        </li>
        <li>
          En ningún caso o circunstancia se podrá responsabilizar directamente o
          indirectamente al propietario ni a los colaboradores del ilícito uso
          de la información contenida en {config.url}
        </li>
        <li>
          Así mismo tampoco se nos podrá responsabilizar directamente o
          indirectamente de incorrecto uso o mala interpretación que se haga de
          la información y servicios incluidos. Igualmente quedara fuera de
          nuestra responsabilidad el material al que usted pueda acceder desde
          nuestros enlaces.
        </li>
        <li>
          Si en tu país, este tipo de página está prohibido, tú y solo tú eres
          el responsable de la entrada a {config.name}.
        </li>
        <li>
          Si decides permanecer en nuestro sitio web, quiere decir que has
          leído, comprendido y aceptas las condiciones de esta página.
        </li>
        <li>
          Todo el contenido ha sido exclusivamente sacado de sitios públicos de
          Internet, por lo que este material es considerado de libre
          distribución. En ningún artículo legal se menciona la prohibición de
          material libre por lo que esta página no infringe en ningún caso la
          ley. Si alguien tiene alguna duda o problema al respecto, no dude en
          ponerse en contacto con nosotros.
        </li>
        <li>
          Todo la información y programas aquí recogidos van destinados al
          efectivo cumplimiento de los derechos recogidos en el artículo 31
          RD/1/1996 por el que se aprueba el texto refundido de la Ley de la
          Propiedad Intelectual (LPI) en especial referencia al artículo 31.2
          LPI, y en concordancia con lo expresado en el artículo 100.2 de esta
          misma ley.
        </li>
        <li>
          Nos reservamos el derecho de vetar la entrada a cualquier sujeto a
          nuestra página web y a su vez se reserva el derecho de prohibir el uso
          de cualquier programa y/o información, en concordancia con los
          derechos de autor otorgados por el artículo 14 LPI.
        </li>
        <li>
          La visita o acceso a esta página web, que es de carácter privado y no
          público, exige la aceptación del presente aviso. En esta web se podrá
          acceder a contenidos publicados por Youtube, Tu.tv, Openload.co,
          Bigfile.to, Streamcloud.eu, allmyvideos.net, 1fichier.com,
          streamin.to, hugefiles.net, powvideo.net, uptobox.com, flashx.tv,
          ul.to, hqq.tv, mp4upload.com, yourupload.com, nowvideo.sx, etc. El
          único material que existe en la web son enlaces ha dicho contenido,
          facilitando únicamente su visionado. Los propietarios de Youtube,
          Digiload.co, Mystream.to, Uqload.com, Tu.tv, Openload.co, Bigfile.to,
          Streamcloud.eu, allmyvideos.net, 1fichier.com, streamin.to,
          hugefiles.net, powvideo.net, uptobox.com, flashx.tv, ul.to, hqq.tv,
          mp4upload.com, yourupload.com, nowvideo.sx, etc. son plenamente
          responsables de los contenidos que publiquen. Por consiguiente,
          {config.name} ni aprueba, ni hace suyos los productos, servicios,
          contenidos, información, datos, opiniones archivos y cualquier clase
          de material existente en Youtube, Digiload.co, Mystream.to,
          Uqload.com, Tu.tv, Openload.co, Bigfile.to, Streamcloud.eu,
          allmyvideos.net, 1fichier.com, streamin.to, hugefiles.net,
          powvideo.net, uptobox.com, flashx.tv, ul.to, hqq.tv, mp4upload.com,
          yourupload.com, nowvideo.sx, etc. y no controla ni se hace responsable
          de la calidad, licitud, fiabilidad y utilidad de la información,
          contenidos y servicios existentes en Youtube, Tu.tv, Openload.co,
          Bigfile.to, Streamcloud.eu, allmyvideos.net, 1fichier.com,
          streamin.to, hugefiles.net, powvideo.net, uptobox.com, flashx.tv,
          ul.to, hqq.tv, mp4upload.com, yourupload.com, nowvideo.sx, etc.
        </li>
      </ol>

      <p>
        English: {config.name} does not host any of the doramas links available
        on this website and does not participate in copying or uploading doramas
        to any website. {config.name} is simply an index of links available on
        the Internet, mainly from popular hosting sources such as Youtube,
        Digiload.co, Mystream.to, Uqload.com, Fembed.com, Tu. tv, Openload.co,
        Bigfile.to, Streamcloud.eu, allmyvideos.net, 1fichier.com, streamin.to,
        hugefiles.net, powvideo.net, uptobox.com, flashx.tv, ul.to, hqq.tv,
        mp4upload.com, yourupload.com, nowvideo.sx, etc. {config.name} is not
        responsible for the content of movies hosted on third party websites.
      </p>

      <p>Email: dmca@{config.platform}.co</p>
    </Warning>
  )
}

const WarningTittle = styled.h2`
  color: white;
  font-size: 25px;
  margin-bottom: 10px;
`

const Dcma = styled.h1`
  color: white;
  font-size: 40px;
  margin-bottom: 10px;
`

const Warning = styled.section`
  padding: 10px 40px;
  color: ${({ theme }) => theme.colors.lighGray};
  font-size: 0.8rem;

  @media ${({ theme }) => theme.devices.laptop} {
    font-size: 1rem;
  }
  ol {
    list-style: number;
    padding: 0 40px;
    margin-top: 20px;

    li {
      line-height: 25px;
      margin: 10px 0;
    }
  }

  p {
    line-height: 25px;
  }
`
