import { useContext } from 'react'
import styled from 'styled-components'

import { DropItemList } from '../../ui/Base/DropItemList'
import { GenreContext } from '../../contexts/GenreContext'
import { DropItemsWrapper } from '../../components/Base/DropItemsWrapper'

export const GenerosList = () => {
  const { genres } = useContext(GenreContext)

  const genersTransfor = genres.map(list => {
    return {
      name: list.name,
      slug: list.slug,
      imagen: list.platforms[0].image_default,
      id: list._id
    }
  })
  return (
    <>
      <Title>Generos</Title>
      <DropItemsWrapper>
        {genersTransfor.map(({ name, slug, imagen, id }) => {
          return (
            <DropItemList
              key={id}
              name={name}
              slug={slug}
              imagen={imagen}
              path='generos'
            />
          )
        })}
      </DropItemsWrapper>
    </>
  )
}

const Title = styled.h2`
  color: black;
  font-size: 2.3rem;
  margin: 20px 20px;
`
