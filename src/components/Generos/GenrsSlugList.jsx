import { useRef } from 'react'

import { CategoriesList } from '../../layouts/CategoriesList'
import MediaContainer from '../../layouts/MediaContainer'
import { MediaItem } from '../../ui/Media/MediaItem'

export const GenrsSlugList = ({ list = [], name, slugIverse }) => {
  const refContainer = useRef()

  return (
    <CategoriesList categorie={name} slug>
      <MediaContainer ref={refContainer}>
        {list.map((item, idx) => (
          <MediaItem key={idx} {...item} refContainer={refContainer} />
        ))}
      </MediaContainer>
    </CategoriesList>
  )
}
