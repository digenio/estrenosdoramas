import { CategoriesList } from '../../layouts/CategoriesList'
import { MediaItem } from '../../ui/Media/MediaItem'
import MediaContainer from '../../layouts/MediaContainer'
import styled from 'styled-components'

export const MoviesList = ({ list = [] }) => {
  return (
    <>
      <CategoriesList categorie='Peliculas'>
        <MediaContainer>
          {list.map((item, idx) => (
            <MediaItem numberPublicacion={idx + 1} key={idx} {...item} />
          ))}
        </MediaContainer>
      </CategoriesList>
    </>
  )
}
