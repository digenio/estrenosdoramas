import { CategoriesList } from '../../layouts/CategoriesList'
import MediaContainer from '../../layouts/MediaContainer'
import { MediaItem } from '../../ui/Media/MediaItem'

export const LetterSlugList = ({ list = [], name, slugIverse }) => {
  return (
    <CategoriesList categorie={name} slug>
      <MediaContainer>
        {list.map((item, idx) => (
          <MediaItem key={idx} {...item} />
        ))}
      </MediaContainer>
    </CategoriesList>
  )
}
