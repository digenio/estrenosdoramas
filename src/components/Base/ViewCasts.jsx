import { useState, useEffect } from 'react'
import styled from 'styled-components'
import { FaYoutube, FaRegClock, FaCalendarTimes } from 'react-icons/fa'
import Link from 'next/link'
import slug from 'slug'

import { FadeIn } from '../../styles/animations'
import { Button } from '../../ui/Buttons/Button'
import { DetailActorView as Actor } from '../../ui/Details/DetailViewActor'

export const ViewCasts = ({ cast }) => {
  const listCast = cast?.map(item => ({
    character: item.character,
    credit_id: item.credit_id,
    gender: item.gender,
    id: item.id,
    known_for_department: item.known_for_department,
    name: item.name,
    order: item.order,
    original_name: item.original_name,
    popularity: item.popularity,
    profile_path: item.profile_path,
    slug: slug(`${item.id} ${item.name}`, { lower: true })
  }))

  return (
    <DetailsView>
      <DetailsContent>
        <article>
          <ListActors>
            {listCast?.map((item, idx) => (
              <Actor key={idx} {...item} />
            ))}
          </ListActors>
        </article>
      </DetailsContent>
    </DetailsView>
  )
}

const DetailsView = styled.article`
  padding: 10px;
  color: white;
  animation: ${FadeIn} 0.4s;
  transition: all 1s;
  animation-fill-mode: both;
  min-height: 200px;
  /* min-height: 600px; */
`

const DetailsContent = styled.div`
  width: 100%;
  @media ${({ theme }) => theme.devices.laptop} {
    display: flex;
    justify-content: space-between;
    gap: 10px;
  }
`

const ListActors = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 20px;
`
