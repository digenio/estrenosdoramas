import React from "react";
import config from "../../utils/config";
import styled, { css } from "styled-components";

export function HomeMessage() {
  return (
    <Info>
      <ChapterDescription>
        <ChapterTitle>{config.name}</ChapterTitle>
        <Paragraf>
          <strong>{config.name}.io</strong> se destaca como una reconocida
          página web para disfrutar <strong>doramas online</strong>. En
          <strong>Estrenos Doramas</strong> disfrutarás de los mejores doramas
          asiáticos en un solo sitio. Aquí podrás encontrar
          <strong>doramas online gratis</strong>, tales como los K-Drama,
          C-drama, , J-Drama y T-Drama.
        </Paragraf>
        <Paragraf>
          Recuerda que los mejores <strong>estrenos de doramas</strong>, estarán disponible en Sub Español, Latino HD Gratis.
        </Paragraf>
      </ChapterDescription>
    </Info>
  );
}

const Info = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 1rem 0;
  padding: 10px 15px;
  border-radius: 0.25rem;
  background-color: ${({ theme }) => theme.colors.bodyH};
  color: ${({ theme }) => theme.colors.white};

  p {
    margin: 0 0 1.5rem;
    line-height: 20px;
    max-height: 200px;
    @media ${({ theme }) => theme.devices.tablet} {
      font-size: 1rem;
    }
  }
`;

const ChapterDescription = styled.div`
  margin-left: 10px;
  margin: 20px 0;
  font-size: 1rem;
  strong {
    color: ${({ theme }) => theme.colors.lighGray};
    font-size: 1rem;
    margin: 3px;
  }

  color: ${({ theme }) => theme.colors.black};
  line-height: 1.5rem;
`;

const ChapterTitle = styled.h1`
  font-size: 1.4rem;
  margin-bottom: 20px;
  text-align: center;
  strong {
    color: ${({ theme }) => theme.colors.yellow};
  }
`;
const Paragraf = styled.p`
  a {
    color: ${({ theme }) => theme.colors.secondary};
  }
`;
