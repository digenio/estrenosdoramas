import { FaFlag } from 'react-icons/fa'
import styled from 'styled-components'
import { Button } from '../../ui/Buttons/Button'

export const ReportFlag = ({ setshowModal }) => {
  return (
    <Report onClick={() => setshowModal(true)}>
      <div>
        <Button background radius={5} width={40}>
          <FaFlag size={15} />
        </Button>
      </div>
    </Report>
  )
}

const Report = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 1rem;
  margin-bottom: 1rem;

  div {
    z-index: 1;
  }
`
