import styled from 'styled-components'
import { MediaItemAside } from '../../ui/Media/MediaItemAside'

export const SeriesListAside = ({ categorie = 'Peliculas' }) => {
  return (
    <SeriesAside>
      <CategorieTitle>
        <p>{categorie}</p>
        <div></div>
      </CategorieTitle>

      <SeriesList>
        <MediaItemAside />
        <MediaItemAside />
        <MediaItemAside />
        <MediaItemAside />

        <MediaItemAside />
      </SeriesList>
    </SeriesAside>
  )
}

const SeriesAside = styled.div`
  background-color: ${({ theme }) => theme.colors.body};
  padding: 10px;
  border-radius: 5px;
  padding: 1.25rem;
`

const SeriesList = styled.div`
  width: 100%;
  margin-top: 30px;
  display: grid;
  gap: 5px;
  grid-template-columns: 1fr 1fr 1fr;
`

const CategorieTitle = styled.div`
  div {
    margin-top: 15px;
    position: relative;
    :before {
      border-radius: 3px;
      content: '';
      background-color: gray;
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 1px;
      opacity: 0.2;
    }

    :after {
      position: absolute;
      height: 3px;
      width: 40px;
      left: 0;
      bottom: 0;
      content: '';
      background-color: ${({ theme }) => theme.colors.secondary};
    }
  }
`
