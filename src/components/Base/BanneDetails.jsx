import styled, { css } from 'styled-components'
import { DetailRating } from '../../ui/Media/DetailRating'
import { ShareSocial } from '../../ui/Base/ShareSocial'
import { URL_IMAGE } from '../../utils/urls'
import { getImageUrlEpisode } from '../../utils/functions'
import { ActionsBar } from './ActionsBar'
import { UserContext } from '../../contexts/UserContext'
import { useContext } from 'react'
export const BanneDetails = ({
  overview,
  seasons,
  premiere,
  name,
  name_es,
  poster_path,
  movie,
  dorama,
  backdrop
}) => {
  const { loggedUser, logout } = useContext(UserContext)
  const numberOfSeasons = seasons?.length

  const url = getImageUrlEpisode(poster_path, backdrop)

  return (
    <Banner>
      <BannerBackground>
        <img src={url} alt='background' />
      </BannerBackground>
      <BannerImage>
        <img src={`${URL_IMAGE}${poster_path}`} alt='' />
      </BannerImage>
      <BannerHeader>
        <div>
          <BannerTitle>{name}</BannerTitle>

          {!movie && (
            <BannerSubtitle>
              <Status> {premiere ? 'Vivo' : 'Ended'} </Status>
              <Season>{numberOfSeasons}</Season> Temporada
            </BannerSubtitle>
          )}

          <BannerDescription>
            <p>{overview}</p>
          </BannerDescription>
        </div>
        <BannerFooter>
          <DetailRating />

          {loggedUser && <ActionsBar media={dorama} />}

          <ShareSocial />
        </BannerFooter>
      </BannerHeader>

      <Over></Over>
    </Banner>
  )
}

const Banner = styled.div`
  position: relative;
  z-index: 1;
  padding: 1.25rem;
  margin-bottom: 1.25rem;
  min-height: 300px;
  gap: 20px;
  border-radius: 3px !important;
  color: white;
  display: grid;
  margin-top: 30px;
  /* height: 500px; */

  overflow: hidden;

  @media ${({ theme }) => theme.devices.laptop} {
    height: 300px;
    grid-template-columns: 200px 1fr;
    grid-column: 1 /-1;
  }
`
const BannerBackground = styled.figure`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: -1;

  img {
    object-fit: cover;
    width: 100%;
    height: 100%;
  }
  :before {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    content: '';

    box-shadow: inset 0 0 100px rgb(0 0 0 / 80%);
    background-color: black;

    opacity: 0.3;
  }
`

const BannerHeader = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  @media ${({ theme }) => theme.devices.laptop} {
    height: 300px;
    grid-template-columns: 200px 1fr;
  }
`
const BannerTitle = styled.h1`
  line-height: 36px;
  padding: 7px 0;
  margin-bottom: 0;
  max-height: none;
  font-weight: 700;
  transition: 0.4s;
  font-size: 1rem;
  color: ${({ theme }) => theme.colors.secondary};
  @media ${({ theme }) => theme.devices.laptop} {
    font-size: 1.875rem;
  }
`

const BannerSubtitle = styled.h2`
  font-size: 0.8rem;
  font-weight: 400;
  margin: 5px 0;
  @media ${({ theme }) => theme.devices.laptop} {
    font-size: 1em;
  }
`
const StyleSpan = css`
  white-space: nowrap;
  font-size: 1rem;
  line-height: 20px;
  color: ${({ theme }) => theme.colors.secondary};
  margin-right: 0.3rem;
  font-weight: 700;
`
const Status = styled.span`
  display: inline-block;
  margin-right: 0.3rem;
  vertical-align: top;
  text-transform: uppercase;
  line-height: 20px;
  padding: 0 0.6rem;
  border-radius: 10px;
  background-color: ${({ theme }) => theme.colors.secondary};
  color: white;
  font-size: 0.625rem;
`
const Season = styled.span`
  ${StyleSpan}
`

const BannerImage = styled.figure`
  left: 0;
  top: 0;
  width: 180px;
  height: 260px;
  margin: 0 auto 1.25rem;
  z-index: -1;
  background-color: ${({ theme }) => theme.colors.body};
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    border-radius: 3px;
  }
`
const BannerDescription = styled.div`
  margin-top: 10px;
  font-size: 0.8rem;
  max-height: 100px;
  overflow-y: scroll;

  @media ${({ theme }) => theme.devices.laptop} {
    font-size: 13px;
  }

  @media ${({ theme }) => theme.devices.laptopL} {
    font-size: 1rem;
  }
`

const BannerFooter = styled.div`
  width: 100%;
  padding-top: 19px;
  padding-bottom: 15px;
  border-top: 1px solid;
  border-top-color: rgba(255, 255, 255, 0.2);
  margin-top: 50px;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-evenly;
  gap: 10px;

  @media ${({ theme }) => theme.devices.laptop} {
    grid-template-columns: 200px 1fr;
    /* margin-top: 20px; */
    flex-direction: row;
    justify-content: space-between;
  }

  @media ${({ theme }) => theme.devices.laptopL} {
    margin-top: 5px;
  }
`
const Over = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  /* background-color: ${({ theme }) => theme.colors.primary}; */
  z-index: -2;
  /* opacity: 0.7; */
  background-color: ${({ theme }) => theme.colors.body};
  :before {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    /* content: ''; */
    z-index: -1;
    box-shadow: inset 0 0 100px rgb(0 0 0 / 80%);
    background-color: ${({ theme }) => theme.colors.primary};
    opacity: 0.3;
  }

  :after {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;

    background-color: rgba(0, 0, 0, 0.6);
    /* content: ''; */
  }
`
