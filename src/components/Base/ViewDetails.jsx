import styled from 'styled-components'
import { BiRadioCircleMarked } from 'react-icons/bi'

import { getDateByMonthName } from '../../utils/functions'
export const ViewDetails = ({ dorama = [] }) => {
  const {
    genres = [],
    created_by,
    first_air_date,
    premiere,
    release_date,
    __typename,
    name_es,
    seasons
  } = dorama

  const director = created_by?.filter(item => item.department == 'Directing')

  let date = getDateByMonthName(first_air_date)

  if (release_date) {
    date = getDateByMonthName(release_date)
  }

  return (
    <Details>
      <div>
        <Icon>
          <BiRadioCircleMarked />
        </Icon>
        Fecha de estreno <Premiere>{date}</Premiere>
      </div>
      <div>
        <Icon>
          <BiRadioCircleMarked />
        </Icon>
        Nombre Latino <ItemSpan>{name_es}</ItemSpan>
      </div>
      <GenerList>
        <Icon>
          <BiRadioCircleMarked />
        </Icon>
        Generos
        {genres.map((item, idx) => (
          <ItemSpan key={idx}>{item.name}</ItemSpan>
        ))}
      </GenerList>

      {!director?.length < 0 && (
        <Direction>
          <Icon>
            <BiRadioCircleMarked />
          </Icon>
          Direccion
          {director?.map((item, idx) => (
            <ItemSpan key={idx}>{item.name}</ItemSpan>
          ))}
        </Direction>
      )}

      {__typename == 'Dorama' && (
        <>
          <div>
            <Icon>
              <BiRadioCircleMarked />
            </Icon>
            Temporadas <ItemSpan>{seasons?.length}</ItemSpan>
          </div>

          <div>
            <Icon>
              <BiRadioCircleMarked />
            </Icon>
            Estado
            {premiere ? (
              <ItemSpan>En emision</ItemSpan>
            ) : (
              <ItemSpan>Culminado</ItemSpan>
            )}
          </div>
        </>
      )}
    </Details>
  )
}

const Details = styled.div`
  min-height: 200px;
  border-radius: 12px;
  div {
    color: white;
    margin: 10px 0;
    font-weight: 700;
    font-size: 0.9rem;
  }
`

const Premiere = styled.span`
  font-weight: 400;
`

const ItemSpan = styled.span`
  color: ${({ theme }) => theme.colors.secondary};
  font-weight: 400;
  margin: 0 3px;
  display: inline-block;
`

const Icon = styled.span`
  color: ${({ theme }) => theme.colors.secondary};
  margin-right: 10px;
  font-size: 20px;
`

const GenerList = styled.div`
  display: flex;
  flex-wrap: wrap;
`

const Direction = styled.div`
  display: flex;
`
