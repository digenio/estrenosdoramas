import Link from 'next/link'
import styled from 'styled-components'
import { LogoIcon } from '../../ui/Base/LogoIcon'
import { Button } from '../../ui/Buttons/Button'

export const GotoApp = () => {
  return (
    <App>
      <LogoIcon width={100} height={35} />
      <Description>
        <p>Estrenos Doramas : App Doramas</p>
        <span>Abrir la App Estrenos Doramas</span>
      </Description>

      <ButtonApp>
        <Link href='https://play.google.com/store/apps/details?id=com.gratis.estrenosdoramas&hl=es'>
          <a target='_blank' rel='noopener noreferrer'>
            Abrir
          </a>
        </Link>
      </ButtonApp>
    </App>
  )
}

const App = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  padding:10px;
  width: 90%;
  margin: 10px auto;
  margin-bottom: 10px;
  justify-content: center;
  height: auto;
  min-height: 50px;
  border-radius: 5px;
  color: white;
  /* background-color: ${({ theme }) => theme.colors.detail}; */
  background-color:#143c63 ;
  margin-top: 90px;
  @media ${({ theme }) => theme.devices.tablet} {
    display: none;
  }
`

const Description = styled.div`
  margin: 0 20px;
  font-size: 10px;

  span {
    opacity: 0.7;
  }

  @media ${({ theme }) => theme.devices.mobileM} {
    font-size: 13px;
  }
`

const ButtonApp = styled(Button)`
  font-size: 13px;
  background-color: white;
  color: black;
  width: 70px;
  height: 30px;
  display: block;
`
