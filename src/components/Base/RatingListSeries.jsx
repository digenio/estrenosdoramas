import styled from 'styled-components'
import { RatingList } from '../../ui/Media/RatingList'
import { RatingListItem } from '../../ui/Media/RatingListItem'
import { LoaderAside } from '../../ui/Loaders/LoaderAside'

export const RatingListSeries = ({ series, list, ldgDoramas, aside }) => {
  if (ldgDoramas) {
    // return <LoaderAside />
  }

  return (
    <RatingSeries>
      <RatingList>
        {series && (
          <CategorieTitle>
            <p>Series</p>
            <div></div>
          </CategorieTitle>
        )}

        {list?.map((item, idex) => (
          <RatingListItem aside={aside} key={idex} {...item} />
        ))}
      </RatingList>
    </RatingSeries>
  )
}

const RatingSeries = styled.div`
  @media ${({ theme }) => theme.devices.laptop} {
    grid-column: 1/2;
    grid-row: 1/2;
  }
`

const CategorieTitle = styled.div`
  div {
    margin-top: 15px;
    position: relative;
    :before {
      border-radius: 3px;
      content: '';
      background-color: gray;
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 1px;
      opacity: 0.2;
      z-index: -1;
    }

    :after {
      position: absolute;
      height: 3px;
      width: 40px;
      left: 0;
      bottom: 0;
      content: '';
      background-color: ${({ theme }) => theme.colors.secondary};
    }
  }
`
