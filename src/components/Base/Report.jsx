import styled, { css } from 'styled-components'
import { Alert } from './Alert'

export const Report = ({
  setReportOption,
  ReportOption,
  changeText,
  success,
  sendReport,
  loading
}) => {
  return (
    <>
      {success ? (
        <Alert
          type='Exitoso'
          message='Hemos recibido con éxito tu reporte, gracias.'
        ></Alert>
      ) : (
        <ReportContent>
          <ReporItem
            onClick={() => setReportOption('options')}
            active={ReportOption == 'options'}
          >
            <div></div>
            <span>No tiene opciones</span>
          </ReporItem>
          <ReporItem
            onClick={() => setReportOption('video')}
            active={ReportOption == 'video'}
          >
            <div></div>
            <span>Video eliminado</span>
          </ReporItem>
          <ReporItem
            onClick={() => setReportOption('subtitles')}
            active={ReportOption == 'subtitles'}
          >
            <div></div>
            <span>Subtitulos desfasados</span>
          </ReporItem>
          <ReporItem
            onClick={() => setReportOption('other')}
            active={ReportOption == 'other'}
          >
            <div></div>
            <span>Otro</span>
            <label name='options'></label>
          </ReporItem>
          {ReportOption === 'other' && (
            <Other onChange={e => changeText(e.target.value)}></Other>
          )}

          {loading ? (
            <a>Enviando...</a>
          ) : (
            <SendReport onClick={sendReport}>Reportar</SendReport>
          )}
        </ReportContent>
      )}
    </>
  )
}

const ReportContent = styled.div`
  padding: 10%;
  display: flex;
  flex-direction: column;
  a {
    display: inline-block;
    width: 60%;
    height: 30px;
    border: 5px;
    display: inline-block;
    margin: 15px auto;
    color: white;
    text-align: center;
    background-color: #9e9ca1;
    user-select: none;
    line-height: 30px;
    border-radius: 5px;
  }
`

const ReporItem = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  margin-bottom: 20px;
  div {
    position: relative;
    border: 2px solid ${({ theme }) => theme.colors.primary};
    border-radius: 100px;
    margin-right: 10p;
    width: 30px;
    height: 30px;
    display: inline-block;
    margin-right: 10px;
    ${({ active }) =>
      active &&
      css`
        :before {
          content: '';
          width: 15px;
          height: 15px;
          position: absolute;
          background-color: ${({ theme }) => theme.colors.primary};
          border-radius: 100%;
          top: 6px;
          left: 0;
          right: 0;
          bottom: 0;
          margin: 0 auto;
        }
      `}
  }
  span {
    display: inline-block;
  }
`
const Other = styled.textarea`
  width: 100%;
  max-height: 50px;
  max-width: 100%;
  padding: 10px;
  margin-bottom: 30px;
`

const SendReport = styled.button`
  width: 60%;
  height: 30px;
  border: 5px;
  display: inline-block;
  margin: 15px auto;
  color: white;
  text-align: center;
  cursor: pointer;
  background-color: ${({ theme }) => theme.colors.primary};
  :hover {
    background-color: #f29805b3;
  }
`
