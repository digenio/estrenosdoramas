import styled from 'styled-components'
import { useState, useEffect, useRef } from 'react'
import ReactDOM from 'react-dom'

import { handleScrollTop } from '../../utils/functions'
import { useScrollPosition } from '../../hooks/useScrollPosition'

export const Modal = ({ show, onClose, children, title }) => {
  const isMounted = useRef(true)
  const [isBroser, setIsBroser] = useState(false)
  const [position] = useScrollPosition()

  useEffect(() => {
    setIsBroser(true)
    handleScrollTop(position)
  }, [show])

  const handleClose = e => {
    e.preventDefault()
    onClose()
  }

  const modalContent = show ? (
    <ModalContainer>
      <StyledModalOverlay onClick={handleClose}></StyledModalOverlay>
      <StyledModal>
        <StyledModalHeader>
          {title && <StyledModalTitle>{title}</StyledModalTitle>}
          <a href='#' onClick={handleClose}>
            x
          </a>
        </StyledModalHeader>

        <StyledModalBody>{children}</StyledModalBody>
      </StyledModal>
    </ModalContainer>
  ) : null

  if (isBroser) {
    return ReactDOM.createPortal(
      modalContent,
      document.getElementById('modal-root')
    )
  } else {
    return null
  }
}
const StyledModalBody = styled.div`
  padding-top: 10px;
`

const StyledModalHeader = styled.div`
  display: flex;
  position: relative;
  justify-content: center;
  align-items: center;
  user-select: none;
  font-size: 20px;
  a {
    position: absolute;
    font-size: 25px;
    right: 5%;
    :hover {
      color: red;
    }
  }
`

const StyledModal = styled.div`
  background-color: ${({ theme }) => theme.colors.bodyH};
  width: 500px;
  height: 450px;
  border-radius: 5px;
  padding: 15px;
  color: black;
`
const StyledModalOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  height: 200%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: -1;
`
const StyledModalTitle = styled.h2`
  text-transform: capitalize;
  display: inline-block;
`

const ModalContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 11;
  padding: 20px;
`
