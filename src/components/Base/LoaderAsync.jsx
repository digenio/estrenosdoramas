import ClipLoader from 'react-spinners/RingLoader'
import { colors } from '../../styles/theme'
import styled, { css } from 'styled-components'

export const LoaderAsync = ({ black }) => {
  return (
    <LoaderContainer className='sweet-loading'>
      <ClipLoader color={colors.details} size={100} css={override} />
      <DescriptionLoader black={black}>Cargando...</DescriptionLoader>
    </LoaderContainer>
  )
}

const LoaderContainer = styled.article`
  width: 100%;
  height: 300px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`
const override = css`
  display: block;
  margin: 0 auto;
`
const DescriptionLoader = styled.span`
  display: inline-block;
  font-size: 1.3rem;
  margin-top: 20px;
  color: white;
  ${({ black }) =>
    black &&
    css`
      color: ${({ theme }) => theme.colors.black};
    `}
`
