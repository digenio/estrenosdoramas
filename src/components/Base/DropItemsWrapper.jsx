import styled from 'styled-components'

export const DropItemsWrapper = ({ children, list }) => {
  return <GeneroWrapper list={list}>{children}</GeneroWrapper>
}

const GeneroWrapper = styled.div`
  display: grid;
  width: 100%;
  grid-template-columns: repeat(2, 1fr);
  gap: 10px;
  @media ${({ theme }) => theme.devices.tabletS} {
    grid-template-columns: repeat(3, 1fr);
  }
  @media ${({ theme }) => theme.devices.tablet} {
    grid-template-columns: repeat(4, 1fr);
  }

  ${({ active }) => active && css``}
`
