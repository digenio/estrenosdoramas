import styled, { css } from 'styled-components'
import { useEffect } from 'react'
import { useLazyQuery } from '@apollo/client'

import { getHrefByCategory, getSlugByCategory } from '../../utils/functions'
import { SearchMediaItem } from '../../ui/Media/SearchMediaItem'
import { URL_IMAGE_POSTER } from '../../utils/urls'
import { FadeIn } from '../../styles/animations'
import { LoaderAsync } from './LoaderAsync'
import { SEARCH } from '../../gql/search'

export const Search = ({ searchStringLength, searchValue, close }) => {
  const [
    getResults,
    { data: { searchDorama = [], searchMovie = [] } = {}, loading }
  ] = useLazyQuery(SEARCH, {
    variables: { input: `${searchValue}` }
  })

  const searchResults = searchDorama.concat(searchMovie) || []

  const list = searchResults.map(item => ({
    name: item.name || item.name_es,
    nameEs: item.name_es,
    image: item.poster || URL_IMAGE_POSTER + item.poster_path,
    year: item.first_air_date && item.first_air_date.split('-')[0],
    category: item.__typename === 'Movie' ? 'Pelicula' : 'Dorama',
    page: getHrefByCategory(item.__typename),
    link: getSlugByCategory(item.__typename, item.slug)
  }))

  //Cada vez que cambia el valor del input y dicho valor es mayor a 4 caracteres dispara la busqueda
  useEffect(() => {
    if (searchStringLength < 3) {
      return
    }
    getResults()
  }, [searchValue])

  if (searchStringLength < 3) {
    return (
      <Warning>
        <p>Debes ingresar tres o mas caracteres para iniciar la busqueda.</p>
      </Warning>
    )
  }

  if (loading) {
    return (
      <Results>
        <LoaderAsync black />
      </Results>
    )
  }
  if (searchResults.length === 0) {
    return (
      <Warning>
        <p>No se encontraron coincidencias</p>
      </Warning>
    )
  }
  return (
    <Results className='results' scrollActive={list.length > 5}>
      {list.map(item => (
        <SearchMediaItem item={item} close={close} />
      ))}
    </Results>
  )
}

const SearchStyled = css`
  position: absolute;
  padding: 10px;
  width: 90%;
  left: 5%;
  min-height: 80px;
  max-height: 435px;
  border-radius: 5px;
  top: 80px;
  overflow: scroll;
  background-color: ${({ theme }) => theme.colors.body};
  color: ${({ theme }) => theme.colors.black};

  @media ${({ theme }) => theme.devices.laptop} {
    width: 35%;
    right: 0;
    left: 63%;
  }
`
const Warning = styled.div`
  ${SearchStyled}

  animation: ${FadeIn} .3s;
  transition: all ;
  animation-fill-mode: both;

  ::-webkit-scrollbar {
    display: none;

  }

  display: flex;
  align-items: center;
  justify-content: center;
  p {
    margin-bottom: 0;
  }
`

const Results = styled.div`
  ${SearchStyled}
  .result::-webkit-scrollbar-thumb {
    background: #ccc;
    border-radius: 4px;
  }

  justify-content: center;
  animation: ${FadeIn} 0.3s;
  transition: all;
  animation-fill-mode: both;

  overflow-x: hidden;
  overflow-y: hidden;

  ${({ scrollActive }) =>
    scrollActive &&
    css`
      overflow-y: scroll;
    `}
`
