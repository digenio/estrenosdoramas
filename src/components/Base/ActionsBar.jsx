import styled, { css } from 'styled-components'
import { FaHeart, FaWindowClose } from 'react-icons/fa'
import { GoClock, GoEye, GoEyeClosed } from 'react-icons/go'
import { useContext, useEffect, useState } from 'react'
import { ListContext } from '../../contexts/ListContext'
import { FavsContext } from '../../contexts/FavsContext'
import { UserContext } from '../../contexts/UserContext'

export const ActionsBar = ({ setshowModal, media = {} }) => {
  const [liked, changeLiked] = useState(false)
  const { favs, toggleFav } = useContext(FavsContext)

  const {
    _id,
    slug,
    name_es,
    name,
    poster_path,
    serie_id,
    poster,
    __typename,
    isTVShow,
    genres,
    labels,
    overview
  } = media

  const { loggedUser } = useContext(UserContext) || {}

  useEffect(() => {
    const index = favs.findIndex(item => item?.serie_id === _id)
    changeLiked(index !== -1)
  }, [_id, favs])

  const handleFav = () => {
    if (loggedUser) {
      if (!liked) {
        toggleFav({
          serie_id: _id,
          name,
          name_es,
          poster_path,
          poster,
          slug,
          type:
            __typename === 'Movie' ? 'movie' : isTVShow ? 'tvshow' : 'dorama'
        })
        changeLiked(true)
      } else {
        toggleFav({
          serie_id: _id
        })
        changeLiked(false)
      }
    }
  }

  const { list, toggleList } = useContext(ListContext)

  let inList = null
  let status = ''

  if (list && list.length > 0) {
    inList = list.find(item => item.serie_id === _id)
    status = inList?.status
  }
  const handleEdit = status => {
    toggleList(
      {
        name,
        slug,
        name_es,
        poster_path,
        serie_id: _id,
        poster,
        _id: inList?._id,
        type: __typename === 'Movie' ? 'movie' : isTVShow ? 'tvshow' : 'dorama'
      },
      status
    )
  }
  return (
    <WrapperActions>
      <Favorite onClick={handleFav}>
        <Icon liked={liked}>
          <FaHeart size={20} />
        </Icon>
        <span>Favoritos</span>
      </Favorite>

      <SeeLate onClick={() => handleEdit('Ver despues')}>
        <Icon liked={status === 'Ver despues'}>
          <GoClock size={20} />
        </Icon>
        <span>Ver despues</span>
      </SeeLate>

      <Seeing onClick={() => handleEdit('Viendo')}>
        <Icon liked={status === 'Viendo'}>
          <GoEye size={20} />
        </Icon>
        <span>Viendo</span>
      </Seeing>

      <Viewed onClick={() => handleEdit('Visto')}>
        <Icon liked={status === 'Visto'}>
          <GoEyeClosed size={20} />
        </Icon>
        <span>visto</span>
      </Viewed>

      {inList && (
        <Remove onClick={() => handleEdit('remove')}>
          <FaWindowClose size={20} color={'red'} /> <span>visto</span>
        </Remove>
      )}
    </WrapperActions>
  )
}

const ActionsStyled = css`
  margin-right: 10px;
  font-size: 10px;
  cursor: pointer;
  text-transform: capitalize;

  :hover {
    color: ${({ theme }) => theme.colors.lighGray};
  }

  span {
    display: none;

    @media ${({ theme }) => theme.devices.laptop} {
      display: initial;
      margin-right: 5px;
    }
  }
`

const WrapperActions = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`
const Icon = styled.i`
  margin-right: 5px;
  ${({ liked }) =>
    liked &&
    css`
      color: ${({ theme }) => theme.colors.primary};
    `}
`

const Favorite = styled.div`
  ${ActionsStyled}
  margin-left: 10px;
  @media ${({ theme }) => theme.devices.laptop} {
    margin-left: 0px;
  }
`

const SeeLate = styled.div`
  ${ActionsStyled}
`

const Seeing = styled.div`
  ${ActionsStyled}
`
const Viewed = styled.div`
  ${ActionsStyled}
`

const Remove = styled.div`
  ${ActionsStyled}
`
