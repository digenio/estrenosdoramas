import { MediaItem } from '../../ui/Media/MediaItem'
import MediaContainer from '../../layouts/MediaContainer'

import { CategoriesList } from '../../layouts/CategoriesList'

export const PaisesList = ({ name, slugIverse, list }) => {
  return (
    <CategoriesList categorie={name} slug>
      <MediaContainer>
        {list.map((item, idx) => (
          <MediaItem key={idx} {...item} />
        ))}
      </MediaContainer>
    </CategoriesList>
  )
}
