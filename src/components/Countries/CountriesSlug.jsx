import React from 'react'
import { AsideRating } from '../../containers/Home/AsideRating'
import { ListCountrie } from '../../containers/Countries/ListCountrie'
import { Categories } from '../../ui/Base/Categories/Categories'

export const CountriesSlug = ({ slug }) => {
  return (
    <>
      <ListCountrie slug={slug} slugIverse />
      <Categories slugIverse slug />
      <AsideRating movie slugIverse />
    </>
  )
}
