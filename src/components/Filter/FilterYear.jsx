import { Filter } from '../../ui/Filters/Filter'
import { FilterItem } from '../../ui/Filters/FilterItem'
import { FaCalendarTimes } from 'react-icons/fa'
import { makeYears } from '../../utils/functions'

export const FilterYear = ({
  filter,
  changeFilter,
  menu,
  changeMenu,
  changePage
}) => {
  const listYears = makeYears()
  const { years = [] } = filter

  const visible = menu === 'years'
  const open = () => changeMenu('years')
  const close = () => changeMenu('')
  const toggle = visible ? close : open

  const handleChange = (checked, id) => {
    if (checked) {
      years.push(id)
    } else {
      const index = years.findIndex(item => item === id)
      years.splice(index, 1)
    }
    if (years.length === 0) {
      changeFilter({ ...filter, years: undefined })
    } else {
      changeFilter({ ...filter, years })
    }
    changePage(1)
  }

  return (
    <Filter
      name='Año'
      icon={<FaCalendarTimes />}
      visible={visible}
      languagesIds={years}
      toggle={toggle}
      active={years.length > 0}
    >
      {listYears.map(id => (
        <FilterItem
          key={id}
          value={id}
          name={id}
          onChange={handleChange}
          filterCheck={years}
        />
      ))}
    </Filter>
  )
}
