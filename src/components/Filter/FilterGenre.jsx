import { useContext, useEffect } from 'react'

import { Filter } from '../../ui/Filters/Filter'
import { FilterItem } from '../../ui/Filters/FilterItem'
import { FaSitemap } from 'react-icons/fa'
import { GenreContext } from '../../contexts/GenreContext'

export const FilterGenre = ({
  filter,
  changeFilter,
  menu,
  changeMenu,
  changePage
}) => {
  const { genres } = useContext(GenreContext)
  const { genreIds = [] } = filter
  const visible = menu === 'genre'
  const open = () => changeMenu('genre')
  const close = () => changeMenu('')
  const toggle = visible ? close : open

  const handleChange = (checked, id) => {
    if (checked) {
      genreIds.push(id)
    } else {
      const index = genreIds.findIndex(item => item === id)
      genreIds.splice(index, 1)
    }
    if (genreIds.length === 0) {
      changeFilter({ ...filter, genreIds: undefined })
    } else {
      changeFilter({ ...filter, genreIds })
    }
    changePage(1)
  }

  return (
    <Filter
      name='Generos'
      icon={<FaSitemap />}
      visible={visible}
      genreIds={genreIds}
      toggle={toggle}
      active={genreIds.length > 0}
    >
      {genres.map(({ _id, name }) => (
        <FilterItem
          key={_id}
          value={_id}
          name={name}
          onChange={handleChange}
          filterCheck={genreIds}
        />
      ))}
    </Filter>
  )
}
