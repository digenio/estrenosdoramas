import { Filter } from '../../ui/Filters/Filter'
import { FilterItem } from '../../ui/Filters/FilterItem'
import { FaProductHunt } from 'react-icons/fa'
import { useContext } from 'react'

import { NetworkContext } from '../../contexts/NetworkContext'

export const FilterProducers = ({
  filter,
  changeFilter,
  menu,
  changeMenu,
  changePage
}) => {
  const { networks } = useContext(NetworkContext)
  const { networkIds = [] } = filter

  const handleChange = (checked, id) => {
    if (checked) {
      networkIds.push(id)
    } else {
      const index = networkIds.findIndex(item => item === id)
      networkIds.splice(index, 1)
    }
    if (networkIds.length === 0) {
      changeFilter({ ...filter, networkIds: undefined })
    } else {
      changeFilter({ ...filter, networkIds })
    }
    changePage(1)
  }

  const visible = menu === 'network'
  const open = () => changeMenu('network')
  const close = () => changeMenu('')
  const toggle = visible ? close : open

  return (
    <Filter
      name='Productoras'
      icon={<FaProductHunt />}
      visible={visible}
      networkIds={networkIds}
      toggle={toggle}
      active={networkIds.length > 0}
    >
      {networks.map(({ _id, name }) => (
        <FilterItem
          key={_id}
          value={_id}
          name={name}
          onChange={handleChange}
          filterCheck={networkIds}
        />
      ))}
    </Filter>
  )
}
