import { useContext, useEffect } from 'react'

import { Filter } from '../../ui/Filters/Filter'
import { FilterItem } from '../../ui/Filters/FilterItem'
import { IoLanguageSharp } from 'react-icons/io5'
import { LanguageContext } from '../../contexts/LanguageContext'

export const FilterIdiom = ({
  filter,
  changeFilter,
  menu,
  changeMenu,
  changePage
}) => {
  const { languages: lang } = useContext(LanguageContext)
  const { bylanguages = [] } = filter

  const visible = menu === 'languages'
  const open = () => changeMenu('languages')
  const close = () => changeMenu('')
  const toggle = visible ? close : open

  const handleChange = (checked, id) => {
    if (checked) {
      bylanguages.push(id)
    } else {
      const index = bylanguages.findIndex(item => item === id)
      bylanguages.splice(index, 1)
    }
    if (bylanguages.length === 0) {
      changeFilter({ ...filter, bylanguages: undefined })
    } else {
      changeFilter({ ...filter, bylanguages })
    }
    changePage(1)
  }

  return (
    <Filter
      name='Idioma'
      icon={<IoLanguageSharp />}
      visible={visible}
      languagesIds={bylanguages}
      toggle={toggle}
    >
      {lang.map(({ code_flix, name }) => (
        <FilterItem
          key={code_flix}
          value={code_flix}
          name={name}
          onChange={handleChange}
          filterCheck={bylanguages}
        />
      ))}
    </Filter>
  )
}
