import { Filter } from '../../ui/Filters/Filter'
import { FilterItem } from '../../ui/Filters/FilterItem'
import { AiOutlineSortAscending } from 'react-icons/ai'

export const FilterSort = ({
  sort,
  changeSort,
  changePage,
  menu,
  changeMenu
}) => {
  const label =
    sort === 'CREATEDAT_DESC'
      ? 'Últimas'
      : sort === 'POPULARITY_DESC'
      ? 'Populares'
      : 'Vistas'

  const visible = menu === 'sort'
  const open = () => changeMenu('sort')
  const close = () => changeMenu('')
  const toggle = visible ? close : open

  return (
    <Filter
      name='Ordernar'
      icon={<AiOutlineSortAscending />}
      sort={sort}
      changeSort={changeSort}
      toggle={toggle}
      visible={visible}
    ></Filter>
  )
}
