import styled from 'styled-components'
import { useState } from 'react'

import { FilterGenre } from '../../components/Filter/FilterGenre'
import { FilterProducers } from '../../components/Filter/FilterProducers'
import { FilterCountrie } from '../../components/Filter/FilterCountrie'
import { FilterLang } from './FilterLang'
import { FilterSort } from './FilterSort'
import { FilterYear } from '../../components/Filter/FilterYear'

export const Filters = ({
  filter,
  changeFilter,
  sort,
  changeSort,
  changePage
}) => {
  const [menu, changeMenu] = useState('')

  return (
    <FiltersContainer>
      <FilterGenre
        filter={filter}
        changeFilter={changeFilter}
        changePage={changePage}
        changeMenu={changeMenu}
        menu={menu}
      />
      <FilterProducers
        filter={filter}
        changeFilter={changeFilter}
        changePage={changePage}
        changeMenu={changeMenu}
        menu={menu}
      />
      <FilterCountrie
        filter={filter}
        changeFilter={changeFilter}
        changePage={changePage}
        changeMenu={changeMenu}
        menu={menu}
      />
      <FilterLang
        filter={filter}
        changeFilter={changeFilter}
        changePage={changePage}
        changeMenu={changeMenu}
        menu={menu}
      />
      {sort && (
        <FilterSort
          sort={sort}
          changeSort={changeSort}
          changePage={changePage}
          changeMenu={changeMenu}
          menu={menu}
        />
      )}

      <FilterYear
        filter={filter}
        changeFilter={changeFilter}
        changePage={changePage}
        changeMenu={changeMenu}
        menu={menu}
      />
    </FiltersContainer>
  )
}

const FiltersContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(2, auto);
  grid-gap: 10px;
  /* display: flex; */
  /* flex-direction: column; */
  /* flex-wrap: wrap; */
  margin-top: 10px;

  @media ${({ theme }) => theme.devices.tablet} {
  }

  @media ${({ theme }) => theme.devices.laptop} {
    grid-template-columns: repeat(6, auto);
    justify-content: flex-start;
  }
`
