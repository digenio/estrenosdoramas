import { useContext, useEffect } from 'react'

import { Filter } from '../../ui/Filters/Filter'
import { FilterItem } from '../../ui/Filters/FilterItem'
import { BiWorld } from 'react-icons/bi'
import { CountryContext } from '../../contexts/CountryContext'

export const FilterCountrie = ({
  filter,
  changeFilter,
  menu,
  changeMenu,
  changePage
}) => {
  const { countries: cts } = useContext(CountryContext)
  const { countries = [] } = filter
  const visible = menu === 'countries'
  const open = () => changeMenu('countries')
  const close = () => changeMenu('')
  const toggle = visible ? close : open

  const handleChange = (checked, id) => {
    if (checked) {
      countries.push(id)
    } else {
      const index = countries.findIndex(item => item === id)
      countries.splice(index, 1)
    }
    if (countries.length === 0) {
      changeFilter({ ...filter, countries: undefined })
    } else {
      changeFilter({ ...filter, countries })
    }
    changePage(1)
  }

  return (
    <Filter
      name='Paises'
      icon={<BiWorld />}
      visible={visible}
      countriesIds={countries}
      toggle={toggle}
      active={countries.length > 0}
    >
      {cts.map(({ slug, name }) => (
        <FilterItem
          key={slug}
          name={name}
          value={slug}
          onChange={handleChange}
          filterCheck={countries}
        />
      ))}
    </Filter>
  )
}
