import { LabelList } from '../../containers/Etiquetas/LabelList'
import { Categories } from '../../ui/Base/Categories/Categories'
import { AsideRating } from '../../containers/Home/AsideRating'

export const TagSlug = () => {
  return (
    <>
      <LabelList list={[1, 2, 3, 4, 5, 6, 7, 8, 9, 20]} slugIverse />
      <Categories slugIverse />
      <AsideRating slugIverse />
    </>
  )
}
