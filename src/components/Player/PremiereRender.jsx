import styled from 'styled-components'
import { getDateString } from '../../utils/functions'

export const PremiereRender = ({ schedule }) => {
  return (
    <NoOptions>
      <NoOptionsText>
        El dorama se estrenó el {getDateString(new Date(schedule.startEmision))}{' '}
        {schedule.days.length > 0
          ? `y se emite los dias ${schedule.days.join(',')}`
          : null}
        .
      </NoOptionsText>
      Los doramas en emisión se publican después de 5 a 6 horas luego de su
      estreno en Asia. En ciertos casos, puede haber retrasos de un día o en
      casos excepcionales hasta una semana, por retrasos del drama o del equipo
      de traducción. Para más información, únete por favor a nuestro grupo
      oficial en Telegram.
    </NoOptions>
  )
}

const NoOptions = styled.p`
  width: 100%;
  height: 300px;
  border-radius: 5px;
  margin: 0;
  background-color: ${({ theme }) => theme.colors.black};
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 10px 20px;
  text-align: center;
  font-size: 0.6rem;
  padding-bottom: 0;
  line-height: 1rem;
  color: ${({ theme }) => theme.colors.white};
  line-height: 20px;

  @media ${({ theme }) => theme.devices.tablet} {
    font-size: 0.8rem;
  }

  @media ${({ theme }) => theme.devices.laptop} {
    font-size: 0.9rem;
    height: 500px;
  }

  @media ${({ theme }) => theme.devices.laptopL} {
    font-size: 1.1rem;
  }
`

const NoOptionsText = styled.span`
  font-size: 0.8rem;

  color: ${({ theme }) => theme.colors.white};
  text-align: center;
  margin-bottom: 10px;

  @media ${({ theme }) => theme.devices.tablet} {
    font-size: 0.9rem;
  }

  @media ${({ theme }) => theme.deviceslaptop} {
    font-size: 1rem;
  }

  @media ${({ theme }) => theme.devices.laptopL} {
    font-size: 1.2rem;
  }
`
