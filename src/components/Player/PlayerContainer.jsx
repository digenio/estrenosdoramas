import styled, { css } from 'styled-components'
import { GoChevronDown } from 'react-icons/go'
import { FaFlag } from 'react-icons/fa'
import { Player } from '../Player/Player'
import { DropDown } from '../../ui/Buttons/DropDown'
import { useContext, useEffect, useState } from 'react'
import { DropItem } from '../../ui/Buttons/DropDownItem'

import { ShareSocial } from '../../ui/Base/ShareSocial'
import { useOptions } from '../../hooks/useOptions'

import { LanguageContext } from '../../contexts/LanguageContext'
import { ControlPlay } from './ControlPlay'
import { LoaderAsync } from '../../components/Base/LoaderAsync'
import { PremiereRender } from './PremiereRender'
import { BeforeDateRender } from './BeforeDateRender'
import { NotOptionsRender } from './NotOptionsRender'
import { Modal } from '../Base/Modal'
import { ReportFlag } from '../Base/ReportFlag'
import { Report } from '../Base/Report'
import { CREATE_PROBLEM } from '../../gql/report'
import { useMutation } from '@apollo/client'
import { URL } from '../../utils/urls'

export const PlayerContainer = ({
  movie,
  chapter,
  extra,
  links = [],
  setServer,
  isBeforeDate,
  air
}) => {
  const [languajeCurrent, setLanguaje] = useState()
  const [option, changeOption] = useState({})
  const [showModal, setshowModal] = useState(false)
  const [text, changeText] = useState('')
  const [ReportOption, setReportOption] = useState('')
  const [success, reportSuccess] = useState(false)

  const [createProblem, { loading }] = useMutation(CREATE_PROBLEM)

  const sendReport = () => {
    createProblem({
      variables: {
        record: {
          dorama_id: chapter && chapter.serie_id,
          movie_id: movie && movie._id,
          serie_name: movie ? movie.name : chapter.serie_name,
          season_number: chapter && chapter.season_number,
          episode_number: chapter && chapter.episode_number,
          type_serie: movie ? 'movie' : 'dorama',
          type: ReportOption,
          server: option.server,
          text
        }
      }
    }).then(() => {
      reportSuccess(true)
    })
  }

  useEffect(() => {
    reportSuccess(false)
  }, [])

  const { options, langs, loading: load } = useOptions({
    _id: chapter?._id,
    type: 'episode',
    links_online: links,
    changeLang: setLanguaje,
    changeServer: changeOption,
    lang: languajeCurrent
  })

  const { languages: lang } = useContext(LanguageContext)

  const machtLang =
    lang.filter(lang => {
      return langs.find(mediaLang => {
        if (lang.code_flix === mediaLang) {
          return lang
        }
      })
    }) || []

  return (
    <PlayerMedia movie={movie}>
      <Over movie={movie}></Over>
      <PlayerHeader>
        <Chapter>
          {!movie && (
            <div>
              <Number>{`${chapter.serie_name} ${chapter.season_number}x${chapter.episode_number}`}</Number>
              <Title>{`Capitulo ${chapter.episode_number}`}</Title>
            </div>
          )}
          <ChapterHeader>
            <ContentOptions>
              {machtLang.map((item, idx) => {
                return (
                  <DropDown
                    key={idx}
                    width={110}
                    icon={<GoChevronDown />}
                    value={item.code_flix}
                    position='none'
                    onClick={e => {}}
                    player
                    movie
                    visible={item.code_flix === languajeCurrent}
                    changeIdiom={setLanguaje}
                    languajeSelect={item.name}
                    links={options}
                    languajeCurrent={languajeCurrent}
                  >
                    {options
                      .filter(it => it.lang == item.code_flix)
                      .map((item, idx) => (
                        <DropItem
                          setServer={setServer}
                          key={idx}
                          number={idx}
                          id={item.code_flix}
                          value={item.server}
                          onChange={() => changeOption(item)}
                          linkOnline={item.link}
                          activeLink={option?.link}
                          player
                          buttonList
                          languaje
                          server={item.server}
                        ></DropItem>
                      ))}
                  </DropDown>
                )
              })}
            </ContentOptions>

            {!movie && (
              <ControlPlay
                extra={extra}
                season={chapter?.season_number}
                chapter={chapter}
              />
            )}
          </ChapterHeader>
        </Chapter>
      </PlayerHeader>

      <Player
        link={option?.link}
        load={load}
        isBeforeDate={isBeforeDate}
        date={air}
        premiere={extra?.detailDorama?.premiere}
        schedule={extra?.detailDorama?.schedule}
      />
      {!movie && (
        <PlayerFooter>
          <DescriptionChapter>
            <hr />
          </DescriptionChapter>

          <HighList>
            <ReportFlag
              setshowModal={setshowModal}
              media={chapter ? chapter : movie}
            />
            <ShareSocial
              url={`${URL}/capitulos/${chapter.serie_slug}-${chapter.season}`}
              name={`${chapter.serie_name} ${
                chapter.season > 1 ? `${chapter.season}` : ''
              } capitulo ${chapter.episode_number}`}
            />
          </HighList>
        </PlayerFooter>
      )}
      <Modal
        show={showModal}
        onClose={() => {
          setshowModal(false)
          reportSuccess(false)
        }}
        title='Reportar video'
      >
        <Report
          setReportOption={setReportOption}
          ReportOption={ReportOption}
          changeText={changeText}
          success={success}
          sendReport={sendReport}
          loading={loading}
        />
      </Modal>
    </PlayerMedia>
  )
}

const PlayerMedia = styled.div`
  position: relative;
  z-index: 1;
  padding: 1.25rem;
  margin-bottom: 1.25rem;
  min-height: 300px;
  color: #fff;
  margin-top: 10px;
  /* background-color: black; */
`

const PlayerHeader = styled.div``
const Chapter = styled.div`
  margin-bottom: 20px;
`
const ChapterHeader = styled.div`
  display: flex;
  justify-content: space-between;
`

const Number = styled.h1`
  display: block;
  font-size: 1rem;
  color: ${({ theme }) => theme.colors.orange};
  @media ${({ theme }) => theme.devices.laptop} {
    font-size: 1.5rem;
    font-weight: 400;
  }
`
const Title = styled.h2`
  display: block;
  font-size: 0.8rem;
  margin: 15px 0;

  @media ${({ theme }) => theme.devices.laptop} {
    font-size: 1rem;
    font-weight: 400;
  }
`

const PlayerFooter = styled.div``
const DescriptionChapter = styled.div`
  p {
    margin-top: 20px;
    width: 90%;
    font-size: 12px;
    @media ${({ theme }) => theme.devices.laptop} {
      font-size: 14px;
    }
  }
  hr {
    margin: 20px 0;
    width: 100%;
    height: 1px;
    background-color: white;
  }
`

const HighList = styled.div`
  display: flex;
  justify-content: space-between;
`
const Over = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  /* background-color: ${({ theme }) => theme.colors.primary}; */
  z-index: -1;
  /* opacity: 0.7; */

  :before {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    content: '';
    z-index: -1;
    box-shadow: inset 0 0 100px rgb(0 0 0 / 80%);
    background-color: ${({ theme }) => theme.colors.primary};
    opacity: 0.3;
    ${({ movie }) =>
      movie &&
      css`
        background-color: transparent;
        box-shadow: none;
      `}
  }

  :after {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;

    background-color: rgba(0, 0, 0, 0.6);
    content: '';
    ${({ movie }) =>
      movie &&
      css`
        background-color: transparent;
        box-shadow: none;
      `}
  }
`

const ContentOptions = styled.div`
  @media ${({ theme }) => theme.devices.laptop} {
    display: flex;
    width: 40%;
    justify-content: flex-start;
    align-items: center;
    gap: 10px;
  }
`
