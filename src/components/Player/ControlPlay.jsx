import Link from 'next/link'
import styled from 'styled-components'
import { FaFlag, FaStepForward, FaStepBackward, FaThList } from 'react-icons/fa'
import { Button } from '../../ui/Buttons/Button'

export const ControlPlay = ({ extra, season, chapter }) => {
  const { prevEpisode = {}, nextEpisode = {} } = extra
  const { serie_slug } = chapter

  return (
    <Container>
      {prevEpisode && (
        <Link href={`/capitulos/[slug]`} as={`/capitulos/${prevEpisode.slug}`}>
          <ButtonControlPlay>
            <FaStepBackward /> <span>Anterior</span>
          </ButtonControlPlay>
        </Link>
      )}

      <Link href={`/temporadas/${serie_slug}-${season}`}>
        <a>
          <ButtonControlPlay>
            <FaThList /> <span>Temporadas</span>
          </ButtonControlPlay>
        </a>
      </Link>

      {nextEpisode && (
        <Link href={`/capitulos/[slug]`} as={`/capitulos/${nextEpisode.slug}`}>
          <ButtonControlPlay>
            <span>Siguiente</span> <FaStepForward />
          </ButtonControlPlay>
        </Link>
      )}
    </Container>
  )
}

const Container = styled.div`
  margin-bottom: 20px;
  padding: 10px;
  span {
    display: none;
  }
  @media ${({ theme }) => theme.devices.laptop} {
    span {
      display: initial;
    }
  }
`

const ButtonControlPlay = styled(Button)`
  /* padding: 10px; */
  margin: 0 5px;
  width: 30px;
  height: 35px;
  background-color: ${({ theme }) => theme.colors.primary};
  border-radius: 5px;
  color: white;
  :hover {
    background-color: #f29805b3;
  }
  @media ${({ theme }) => theme.devices.laptop} {
    width: 150px;
    font-size: 14px;
  }
`
