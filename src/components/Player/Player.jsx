import styled, { css } from 'styled-components'
import { AiOutlineClose } from 'react-icons/ai'

import { LoaderAsync } from '../../components/Base/LoaderAsync'
import { PremiereRender } from './PremiereRender'
import { BeforeDateRender } from './BeforeDateRender'
import { NotOptionsRender } from './NotOptionsRender'

export const Player = ({
  link,
  load,
  isBeforeDate,
  date,
  premiere,
  chapter,
  schedule
}) => {
  if (load) {
    return <LoaderAsync />
  }
  if (!link) {
    return <LoaderAsync />
  }
  if (isBeforeDate) {
    return <BeforeDateRender date={date} />
  }

  if (premiere && !link) {
    return <PremiereRender schedule={schedule} />
  }

  if (!link) {
    return <NotOptionsRender />
  } else {
    return (
      <PlayerMedia chapter={chapter}>
        <iframe
          src={link}
          type='text/html'
          allowFullScreen
          frameborder='0'
        ></iframe>
      </PlayerMedia>
    )
  }
}

const PlayerMedia = styled.section`
  width: 100%;
  position: relative;
  height: 300px;
  border-radius: 5px;
  background-color: black;
  order: -1;

  @media ${({ theme }) => theme.devices.tablet} {
    height: 400px;
  }
  @media ${({ theme }) => theme.devices.laptop} {
    order: initial;
    height: 500px;
  }
  iframe {
    width: 100%;
    height: 100%;
    border-radius: 5px;
    border: none;
  }
  ${({ chapter }) =>
    chapter &&
    css`
      height: 300px;
      @media ${({ theme }) => theme.devices.laptop} {
        height: 480px;
      }
    `}
`

const Close = styled.span`
  position: absolute;
  right: 5%;

  color: white;
  font-size: 30px;
  cursor: pointer;

  :hover {
    color: red;
  }
`
