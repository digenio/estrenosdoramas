import { useRouter } from 'next/router'
import { DiscussionEmbed } from 'disqus-react'
import styled from 'styled-components'
import config from '../../utils/config'

export function Disqus ({ _id, name }) {
  const router = useRouter()

  const url = config.url + router.asPath

  return (
    <Comments>
      <DiscussionEmbed
        shortname={config.platform}
        config={{
          url,
          identifier: _id,
          title: name
        }}
      />
    </Comments>
  )
}

const Comments = styled.div`
  background-color: ${({ theme }) => theme.colors.mediumGray};
  color: black;
  margin-top: 10px;
  padding: 20px;
  a {
    color: ${({ theme }) => theme.colors.primary};
  }
`
