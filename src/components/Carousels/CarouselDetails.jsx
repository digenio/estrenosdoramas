import styled, { createGlobalStyle } from 'styled-components'

import 'react-multi-carousel/lib/styles.css'
import Carousel from 'react-multi-carousel'
import { CarouselItemDetail } from '../../ui/Carousel/CarouselItemDetail'
import { FadeIn } from '../../styles/animations'
import { URL_IMAGE_BACK_DROPS_1X } from '../../utils/urls'

export const CarouselDetails = ({ list = [] }) => {
  return (
    <CarouselContainer>
      <Carousel
        containerClass='carousel-container'
        responsive={responsiveCarousel}
        itemClass='carouselItem'
        autoPlaySpeed={4000}
        autoPlay
        infinite
        swipeable
        arrows={false}
        minimumTouchDrag={70}
        removeArrowOnDeviceType={['mobile', 'tablet']}
      >
        {list.map((item, key) => (
          <CarouselItemDetail
            key={key}
            name={item.name}
            page={`/${
              item.__typename === 'Movie'
                ? 'peliculas'
                : item.isTVShow
                ? 'varidades'
                : 'doramas'
            }/[slug]`}
            link={`/${
              item.__typename === 'Movie'
                ? 'peliculas'
                : item.isTVShow
                ? 'varidades'
                : 'doramas'
            }/${item.slug}`}
            type={
              item.__typename === 'Movie'
                ? 'Película'
                : item.isTVShow
                ? 'KShow'
                : 'Dorama'
            }
            image={
              item.backdrop || URL_IMAGE_BACK_DROPS_1X + item.backdrop_path
            }
            overview={item.overview}
            genrs={item.genres}
            label={item.labels}
            cast={item.cast}
            premiere={item.premiere}
            country={item.country}
            air={item.first_air_date}
          />
        ))}
      </Carousel>
    </CarouselContainer>
  )
}

export const responsiveCarousel = {
  superLargeDesktop: {
    breakpoint: { max: 4000, min: 3000 },
    items: 1
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 1
  },
  tablet: {
    breakpoint: { max: 1024, min: 500 },
    items: 1
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1
  }
}

const CarouselContainer = styled.div`
  animation: ${FadeIn} 0.4s;
  transition: all 1s;
  animation-fill-mode: both;

  @media ${({ theme }) => theme.devices.laptop} {
    grid-column: 2;
    grid-row: 2/-1;
  }
`
