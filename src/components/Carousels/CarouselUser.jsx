import 'react-multi-carousel/lib/styles.css'
import styled, { createGlobalStyle } from 'styled-components'
import Carousel from 'react-multi-carousel'

import { FadeIn } from '../../styles/animations'

import { MediaItem } from '../../ui/Media/MediaItem'
import { URL_IMAGE_BACK_DROPS_1X } from '../../utils/urls'

export const CarouselUser = ({ list = [], categorie }) => {
  return (
    <CarouselContainer>
      <TitleCategorie>{categorie}</TitleCategorie>
      <Carousel
        responsive={responsiveCarousel}
        itemClass='carouselposter'
        autoPlaySpeed={4000}
        autoPlay
        infinite
        swipeable
        arrows={false}
        minimumTouchDrag={70}
        removeArrowOnDeviceType={['mobile', 'tablet']}
      >
        {list.map((item, key) => (
          <MediaItem
            carousel
            key={key}
            name={item.name}
            page={`/${
              item.__typename === 'Movie'
                ? 'peliculas'
                : item.isTVShow
                ? 'varidades'
                : 'doramas'
            }/[slug]`}
            link={`/${
              item.__typename === 'Movie'
                ? 'peliculas'
                : item.isTVShow
                ? 'varidades'
                : 'doramas'
            }/${item.slug}`}
            type={
              item.__typename === 'Movie'
                ? 'Película'
                : item.isTVShow
                ? 'KShow'
                : 'Dorama'
            }
            image={item.image || URL_IMAGE_BACK_DROPS_1X + item.image}
          />
        ))}
      </Carousel>
    </CarouselContainer>
  )
}

export const responsiveCarousel = {
  superLargeDesktop: {
    breakpoint: { max: 4000, min: 3000 },
    items: 8
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 8
  },
  tabletg: {
    breakpoint: { max: 1024, min: 768 },
    items: 5
  },
  tablet: {
    breakpoint: { max: 768, min: 640 },
    items: 4
  },
  mobilem: {
    breakpoint: { max: 500, min: 410 },
    items: 3
  },
  mobile: {
    breakpoint: { max: 400, min: 0 },
    items: 2
  }
}

const CarouselContainer = styled.div`
  animation: ${FadeIn} 0.4s;
  transition: all 1s;
  animation-fill-mode: both;
  @media ${({ theme }) => theme.devices.laptop} {
    grid-column: span 2;
    grid-row: 1;
  }
`
const TitleCategorie = styled.p`
  font-size: 15px;
  font-weight: 700;
  @media ${({ theme }) => theme.devices.laptop} {
    font-size: 25px;
  }
`
