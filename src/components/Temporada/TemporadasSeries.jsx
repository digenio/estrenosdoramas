import styled from 'styled-components'
import { MediaItem } from '../../ui/Media/MediaItem'
import { TemporadaSeasonTabs } from '../Temporada/TemporadaSeasonTabs'
import { DetailSeriesChapter as Chapter } from '../../ui/Details/DetailSeriesChapter'
export const TemporadasSeries = ({
  seasonNumber,
  changeSeason,
  listSeasons = [],
  listEpisodes = [],
  listSeen = [],
  refetch
}) => {
  return (
    <WrapperTemporadas>
      <TemporadaSeasonTabs
        season={seasonNumber}
        setSeason={changeSeason}
        listSeason={listSeasons}
      >
        <ContentSeason>
          {listEpisodes.map((chapter, idx) => (
            <Chapter
              key={idx}
              number='1'
              href={`/capitulos/[slug]`}
              as={`/capitulos/${chapter.slug}`}
              {...chapter}
              listSeen={listSeen}
              refetch={refetch}
            />
          ))}
        </ContentSeason>
      </TemporadaSeasonTabs>
    </WrapperTemporadas>
  )
}

const WrapperTemporadas = styled.section`
  padding: 10px;
`

const ContentSeason = styled.div`
  @media ${({ theme }) => theme.devices.tablet} {
    display: grid;
    grid-gap: 10px;
    grid-template-columns: 50% 50%;
    padding: 0 30px;
  }
`

const ContentDetails = styled.div`
  padding: 10px;
`
