import styled from 'styled-components'
import { TabItem } from '../../ui/Tabs/TabItem'
import { Tabs } from '../../ui/Tabs/Tabs'

export const TemporadaSeasonTabs = ({
  season,
  setSeason,
  children,
  listSeason = []
}) => {
  return (
    <Sidebar>
      <div>
        <div>
          <Player>
            <Tabs sideBar season>
              {listSeason.map((item, idx) => {
                return (
                  <TabItem
                    key={idx}
                    id={item.season_number}
                    label='t1'
                    value={season}
                    onChange={setSeason}
                    sideBar
                    button
                    season
                  >
                    Temporada {item.season_number}
                  </TabItem>
                )
              })}
            </Tabs>
            {children}
          </Player>
        </div>
      </div>
    </Sidebar>
  )
}

const Sidebar = styled.article`
  position: relative;
  grid-row: span 2 / span 2;
`
const Player = styled.div`
  padding: 5px;

  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
`
