import { DetailFigure } from '../../components/Base/DetailFigure'

export const TemporadasHeader = ({ detailDorama = [] }) => {
  return (
    <article>
      <DetailFigure
        genres={detailDorama.genres}
        name={detailDorama.name}
        nameEs={detailDorama.name_es}
        poster_path={detailDorama.poster_path}
        labels={detailDorama.labels}
        season
      />
    </article>
  )
}
