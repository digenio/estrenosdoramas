import styled from 'styled-components'
import { useContext } from 'react'

import { HomeDoramas } from '../../containers/Home/HomeDoramas'
import { HomeEpisodes } from '../../containers/Home/HomeEpisodes'
import { HomeSeeing } from '../../containers/Home/HomeSeeing'
import { HomeMovies } from '../../containers/Home/HomeMovies'
import { HomeTvShows } from '../../containers/Home/HomeTvShows'
import { UserContext } from '../../contexts/UserContext'
import { HomeMessage } from '../Base/HomeMessage'

export const HomeCategories = () => {
  const { loggedUser, logout } = useContext(UserContext)
  return (
    <Container>
      <HomeEpisodes />
      <HomeDoramas />
      <HomeMovies />
      <HomeTvShows />
      {loggedUser && <HomeSeeing />}
      <HomeMessage />
    </Container>
  )
}
const Container = styled.div`
  margin-top: 50px;
  @media ${({ theme }) => theme.devices.laptop} {
  }
`
