import { CategoriesList } from '../../layouts/CategoriesList'
import { MediaItem } from '../../ui/Media/MediaItem'
import MediaContainer from '../../layouts/MediaContainer'
import { LoaderMediaList } from '../../ui/Loaders/LoaderMediaList'


export const HomeList = ({ list = [], categorie, chapter, loading }) => {
  if (loading) {
    return <LoaderMediaList title={categorie} />
  }

  return (
    <>
      <CategoriesList categorie={categorie}>
        <MediaContainer>
          {list.map((item, idx) => (
            <MediaItem
              categorie={categorie}
              numberPublicacion={idx + 1}
              key={idx}
              {...item}
              chapter={chapter}
            />
          ))}
        </MediaContainer>
      </CategoriesList>
    </>
  )
}
