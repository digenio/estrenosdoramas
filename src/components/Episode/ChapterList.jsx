import { CategoriesList } from '../../layouts/CategoriesList'
import { MediaItem } from '../../ui/Media/MediaItem'
import MediaContainer from '../../layouts/MediaContainer'

export const ChapterList = ({ list }) => {
  return (
    <>
      <CategoriesList categorie={'Episodios'}>
        <MediaContainer>
          {list.map((item, idx) => (
            <MediaItem key={idx} {...item} chapter />
          ))}
        </MediaContainer>
      </CategoriesList>
    </>
  )
}
