import styled from 'styled-components'
import config from '../../utils/config'

export const Faq = () => {
  return (
    <Questions>
      <h1>Preguntas frecuentes</h1>

      <h3>¿Qué es {config.name}?</h3>
      <p>
        {config.name} es un sitio web donde puedes ver doramas online gratis
        desde la mayor comodidad de tu hogar, oficina o donde te encuentres.
      </p>

      <h3>¿Subirán el capítulo de tal dorama en vivo?</h3>

      <p>
        Para estar al tanto de eventos y transmisiones en vivo, te invitamos a
        que te suscribas a las notificaciones o puedes seguirnos a través de
        Facebook como {config.name}.
      </p>
      <h3>No encuentro mi dorama favorita, ¿Cómo hago?</h3>

      <p>
        Constantemente estamos actualizando nuestra lista de doramas, te
        invitamos a que nos hagas el pedido a través de Facebook.
      </p>
      <h3>Quiero ver una dorama y no sé como. ¿Me apoyan?</h3>
      <p>
        ¡Por supuesto! Te dejamos los siguientes pasos para que puedas ver y
        disfrutar de tu película favorita.
      </p>
      <ol>
        <li>
          Encuentra la dorama que quieras ver. Esto lo puedes conseguir en la
          categoría de géneros o puedes usar el buscador en la parte superior
          derecha.
        </li>
        <li>
          Si aún así no encuentras lo que buscabas, te pedimos nos busques en
          Facebook y pidas tu dorama.
        </li>
        <li>
          Si encontraste tu dorama favorita ve a la sección de opciones de
          reproducción. Selecciona el servidor externo que mejor te guste y
          listo. ¡Disfruta de tu dorama!
        </li>
        <li>
          Puedes leer un poco más de nosotros, en el siguiente
          <a
            target='__blank'
            rel='noreferrer'
            href='https://diariodevalladolid.elmundo.es/articulo/gente/cuales-son-mejores-series-momento/20210113104919393334.html'
          >
            link.
          </a>
        </li>
      </ol>
    </Questions>
  )
}

const Questions = styled.section`
  padding: 10px 40px;
  font-size: 0.8rem;

  @media ${({ theme }) => theme.devices.laptop} {
    font-size: 1rem;
  }
  color: ${({ theme }) => theme.colors.lighGray};
  h1 {
    color: white;
    font-size: 40px;
    margin-bottom: 30px;
  }
  h3 {
    color: white;
    font-size: 20px;
    margin-bottom: 10px;
    margin-left: 30px;
    margin-bottom: 20px;
  }

  p {
    margin-bottom: 10px;
  }

  ol {
    list-style: number;
    padding: 0 40px;
    margin-top: 20px;
    li {
      line-height: 20px;
      margin: 10px 0;
    }
  }
  a {
    display: inline-block;
    margin-left: 5px;
    color: white;
  }
`
