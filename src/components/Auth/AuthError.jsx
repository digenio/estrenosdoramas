import styled from 'styled-components'

export const AuthError = ({ error, message }) => {
  if (message) {
    return (
      <FormError>
        <FormErrorItem>{message}</FormErrorItem>
      </FormError>
    )
  }

  if (!error || !error.graphQLErrors) {
    return null
  }

  return (
    <FormError>
      {error.graphQLErrors.map(({ message }, idx) => (
        <FormErrorItem key={idx}>{message}</FormErrorItem>
      ))}
    </FormError>
  )
}

const FormError = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 100%;
  max-width: 370px;

  border-radius: 5px;
  margin: 0 auto;
`

const FormErrorItem = styled.p`
  font-size: 0.8rem;
  color: ${({ theme }) => theme.colors.white};
  font-weight: bold;
  background-color: red;
  padding: 10px;
  margin: 5px 0;
  width: 100%;
  text-align: center;
  border-radius: 5px;
`
