import { useState } from 'react'
import styled from 'styled-components'
import { useRouter } from 'next/router'

import { RequestForm } from './RequestForm'
import { LoginForm } from './LoginForm'
import { RegisterForm } from './RegisterForm'
import { useRegister } from '../../hooks/useRegister'
import { useRequestPassword } from '../../hooks/useRequestPassword'
import { useLogin } from '../../hooks/useLogin'

export const AuthForm = () => {
  const [form, setForm] = useState('login')
  const router = useRouter()

  const title =
    form === 'request'
      ? 'Solicitar Contraseña'
      : form === 'login'
      ? 'Inicio de sesión'
      : 'Registro'

  const handleSuccess = () => {
    setTimeout(() => {
      router.push('/autenticacion')
    }, 1000)
  }
  const handleSuccessLogin = () => {
    setTimeout(() => {
      router.reload()
    }, 2000)
    router.push('/')
  }
  const { handleLogin, ldLogin, errLogin } = useLogin({
    onSuccess: handleSuccessLogin
  })
  const { registerUser, ldRegister, errRegister } = useRegister({
    onSuccess: handleSuccess
  })
  const { handleRequest, ldRequest, errRequest } = useRequestPassword({
    onSuccess: () => handleSuccess
  })

  return (
    <FormWrapper>
      <Form>
        <Title>{title}</Title>

        {form === 'request' ? (
          <RequestForm
            onSubmit={handleRequest}
            loading={ldRequest}
            error={errRequest}
          />
        ) : form === 'login' ? (
          <LoginForm
            onSubmit={handleLogin}
            loading={ldLogin}
            error={errLogin}
          />
        ) : (
          <RegisterForm
            onSubmit={registerUser}
            loading={ldRegister}
            error={errRegister}
          />
        )}

        {form === 'request' ? (
          <FormMore>
            ¿No tienes cuenta?
            <a onClick={() => setForm('login')}>Register</a>
          </FormMore>
        ) : form === 'login' ? (
          <>
            <FormMore>
              ¿Olvidaste tu contraseña?{' '}
              <a onClick={() => setForm('request')}>Recuperar</a>
            </FormMore>
            <FormMore>
              ¿No tienes cuenta?
              <a onClick={() => setForm('register')}>Registrate</a>{' '}
            </FormMore>
          </>
        ) : (
          <>
            <FormMore>
              ¿Ya tienes cuenta? <a onClick={() => setForm('login')}>Inicia</a>
            </FormMore>
          </>
        )}
      </Form>
    </FormWrapper>
  )
}

const FormWrapper = styled.div`
  margin: 50px auto;
  max-width: 400px;
  background-color: ${({ theme }) => theme.colors.body};
  box-shadow: -7px 5px 41px -2px rgba(0, 0, 0, 0.43);
  -webkit-box-shadow: -7px 5px 41px -2px rgba(0, 0, 0, 0.43);
  -moz-box-shadow: -7px 5px 41px -2px rgba(0, 0, 0, 0.43);
`

const Form = styled.div`
  color: ${({ theme }) => theme.colors.lighGray};
  padding: 1rem;
`

const Title = styled.h1`
  color: ${({ theme }) => theme.colors.black};
`
const FormMore = styled.p`
  margin-top: 20px;
  padding: 1rem;
  text-align: center;
  display: block;
  color: white;
  background-color: ${({ theme }) => theme.colors.primary};

  a {
    color: ${({ theme }) => theme.colors.white};
    margin-left: 5px;
    display: inline-block;
    cursor: pointer;
    border-bottom: 1px solid white;
    :hover {
      color: ${({ theme }) => theme.colors.lighGray};
      border-bottom: 1px solid ${({ theme }) => theme.colors.lighGray};
    }
  }
`
