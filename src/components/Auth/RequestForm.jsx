import styled from 'styled-components'
import { Button } from '../../ui/Buttons/Button'
import { FaUserAlt } from 'react-icons/fa'
import { useForm } from '../../hooks/useForm'
import { useEffect } from 'react'
import { AuthError } from './AuthError'
export const RequestForm = ({ onSubmit, loading, error }) => {
  const [request, handleChange, reset] = useForm({
    email: ''
  })

  useEffect(() => {
    return () => {
      reset()
    }
  }, [])

  const handleSubmit = () => {
    !loading && onSubmit(request)
  }

  return (
    <FormReqiest>
      <WrapperInput>
        <i>
          <FaUserAlt />
        </i>
        <input
          type='email'
          name='email'
          placeholder='Email'
          autocomplete='off'
          onChange={handleChange}
        />
      </WrapperInput>
      {error && <AuthError error={error} />}
      <TextRecovery>
        Se te enviara un email con una contraseña temporal que puedes cambiar
        luego desde tu perfil.
      </TextRecovery>
      <FormButton
        background
        height={50}
        width={'auto'}
        radius={5}
        onClick={handleSubmit}
      >
        Recuperar
      </FormButton>
    </FormReqiest>
  )
}

const FormReqiest = styled.div`
  padding: 1rem;
`
const TextRecovery = styled.p`
  text-align: center;
  line-height: 20px;
`
const FormButton = styled(Button)`
  margin-top: 20px;
  color: ${({ theme }) => theme.colors.white};
  background-color: ${({ theme }) => theme.colors.orange};
  :hover {
    background-color: #f29805b3;
  }
`

const WrapperInput = styled.p`
  display: flex;
  position: relative;
  border-bottom: 1px solid black;
  height: 38px;
  /* border-radius: 5px; */
  padding: 0;
  width: 100%;
  margin: 20px 0;
  /* background-color: ${({ theme }) => theme.colors.mediumGray}; */
  i {
    width: 10%;
    text-align: center;
    line-height: 2rem;
  }
  :focus-within {
    /* background-color: white; */
  }
  input {
    width: 100%;
    background: transparent;
    border: none;
    padding-left: 10px;
    outline: none;
    color: ${({ theme }) => theme.colors.lighGray};
  }
`
