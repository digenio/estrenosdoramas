import styled from 'styled-components'
import { Button } from '../../ui/Buttons/Button'
import { FaUserAlt, FaKey } from 'react-icons/fa'
import { useForm } from '../../hooks/useForm'
import { useEffect } from 'react'
import { AuthError } from './AuthError'
import config from '../../utils/config'

export const LoginForm = ({ onSubmit, loading, error }) => {
  const [login, handleChange, reset] = useForm({
    email: '',
    password: '',
    platform: config.platform
  })
  useEffect(() => {
    return () => {
      reset()
    }
  }, [])

  const handleSubmit = () => {
    !loading && onSubmit(login)
  }

  return (
    <FormReqiest>
      <WrapperInput>
        <i>
          <FaUserAlt />
        </i>
        <input
          type='email'
          name='email'
          placeholder='Email'
          autocomplete='off'
          onChange={handleChange}
        />
      </WrapperInput>

      <WrapperInput>
        <i>
          <FaKey />
        </i>
        <input
          type='password'
          name='password'
          placeholder='Contraseña'
          autocomplete='off'
          onChange={handleChange}
        />
      </WrapperInput>

      {error && <AuthError error={error} />}

      <FormButton
        background
        height={50}
        width={'auto'}
        radius={5}
        onClick={handleSubmit}
      >
        Iniciar
      </FormButton>
    </FormReqiest>
  )
}

const FormReqiest = styled.div`
  padding: 1rem;
`

const FormButton = styled(Button)`
  color: white;
  margin: 20px auto;
  /* padding: 20px; */
  background-color: ${({ theme }) => theme.colors.orange};
  :hover {
    background-color: #f29805b3;
  }
`

const WrapperInput = styled.p`
  display: flex;
  position: relative;
  border-bottom: 1px solid black;
  height: 38px;
  /* border-radius: 5px; */
  padding: 0;
  width: 100%;
  margin: 20px 0;
  /* background-color: ${({ theme }) => theme.colors.mediumGray}; */
  i {
    width: 10%;
    text-align: center;
    line-height: 2rem;
  }
  :focus-within {
    /* background-color: white; */
  }
  input {
    width: 100%;
    background: transparent;
    border: none;
    padding-left: 10px;
    outline: none;
    color: ${({ theme }) => theme.colors.lighGray};
  }
`
