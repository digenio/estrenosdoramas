import styled from 'styled-components'
import { Button } from '../../ui/Buttons/Button'
import { FaUserAlt, FaKey } from 'react-icons/fa'
import { useForm } from '../../hooks/useForm'
import { useEffect } from 'react'
import { AuthError } from './AuthError'
import config from '../../utils/config'

export const RegisterForm = ({ onSubmit, loading, error }) => {
  const [register, handleChange, reset] = useForm({
    names: '',
    email: '',
    password: '',
    register_platform: config.platform
  })
  useEffect(() => {
    return () => {
      reset()
    }
  }, [])

  const handleSubmit = () => {
    !loading && onSubmit(register)
  }
  return (
    <FormReqiest>
      <WrapperInput>
        <i>
          <FaUserAlt />
        </i>
        <input
          type='text'
          name='names'
          placeholder='Nombre'
          onChange={handleChange}
          autocomplete='off'
        />
      </WrapperInput>
      <WrapperInput>
        <i>
          <FaUserAlt />
        </i>
        <input
          type='email'
          name='email'
          placeholder='Email'
          autocomplete='off'
          onChange={handleChange}
        />
      </WrapperInput>

      <WrapperInput>
        <i>
          <FaKey />
        </i>
        <input
          type='password'
          name='password'
          placeholder='Contraseña'
          autocomplete='off'
          onChange={handleChange}
        />
      </WrapperInput>
      {error && <AuthError error={error} />}

      <FormButton
        background
        height={50}
        width={'auto'}
        radius={5}
        onClick={handleSubmit}
      >
        Registrar
      </FormButton>
    </FormReqiest>
  )
}

const FormReqiest = styled.div`
  padding: 1rem;
`

const FormButton = styled(Button)`
  margin-top: 20px;

  color: ${({ theme }) => theme.colors.white};
  background-color: ${({ theme }) => theme.colors.orange};
  :hover {
    background-color: #f29805b3;
  }
`

const WrapperInput = styled.p`
  display: flex;
  position: relative;
  border-bottom: 1px solid black;
  height: 38px;
  /* border-radius: 5px; */
  padding: 0;
  width: 100%;
  margin: 20px 0;
  /* background-color: ${({ theme }) => theme.colors.mediumGray}; */
  i {
    width: 10%;
    text-align: center;
    line-height: 2rem;
  }
  :focus-within {
    /* background-color: white; */
  }
  input {
    width: 100%;
    background: transparent;
    border: none;
    padding-left: 10px;
    outline: none;
    color: ${({ theme }) => theme.colors.lighGray};
  }
`
