import styled from 'styled-components'
import { TabItem } from '../../ui/Tabs/TabItem'
import { Tabs } from '../../ui/Tabs/Tabs'

export const VariedadesCastsTabs = ({ children, current, changeCurrent }) => {
  return (
    <Container>
      <Tabs>
        <TabItem
          id='details'
          label='Detalle'
          value={current}
          onChange={changeCurrent}
        />
        <TabItem
          id='casts'
          label='Reparto'
          value={current}
          onChange={changeCurrent}
        />
      </Tabs>

      {children}
    </Container>
  )
}
const Container = styled.div`
  position: relative;
  left: 0;
  top: 0;
  width: 100%;
  height: auto;
  padding: 5px 20px;
  grid-column: 2/-1;

  :before {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    content: '';
    z-index: -1;
    box-shadow: inset 0 0 100px rgb(0 0 0 / 80%);
    background-color: ${({ theme }) => theme.colors.primary};
    opacity: 0.3;
  }

  :after {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    z-index: -1;
    background-color: rgba(0, 0, 0, 0.6);
    content: '';
  }
`
