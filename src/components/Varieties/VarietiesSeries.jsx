import { CategoriesList } from '../../layouts/CategoriesList'
import { MediaItem } from '../../ui/Media/MediaItem'
import MediaContainer from '../../layouts/MediaContainer'

export const VarietiesSeries = ({ list = [] }) => {
  return (
    <>
      <CategoriesList categorie='Variedades'>
        <MediaContainer>
          {list.map((item, idx) => (
            <MediaItem key={idx} {...item} />
          ))}
        </MediaContainer>
      </CategoriesList>
    </>
  )
}
