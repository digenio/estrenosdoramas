import styled from 'styled-components'
import { FaUserAlt, FaKey } from 'react-icons/fa'

import { useForm } from '../../hooks/useForm'
import { Button } from '../../ui/Buttons/Button'
import { useContext } from 'react'
import { UserContext } from '../../contexts/UserContext'
import config from '../../utils/config'
import { AuthError } from '../Auth/AuthError'
import { useRouter } from 'next/router'

export const FormAccount = () => {
  const { loggedUser, updateUser, ldUpdate, errUpdate, reload } = useContext(
    UserContext
  )

  const { names, email, username } = loggedUser || {}

  const [edit, handleChange, reset] = useForm({
    names: names,
    email: email,
    password: '',
    register_platform: config.platform
  })
  const router = useRouter()
  const handleSubmit = () => {
    updateUser({
      variables: {
        record: {
          _id: loggedUser._id,
          ...edit
        }
      }
    }).then(
      ({
        data: {
          updateUser: {
            record: { username }
          }
        }
      }) => {
        reload && reload()

        router.push('/usuario/[user]', `/usuario/${username}`)
      }
    )
  }

  return (
    <FormWrapper>
      <Form>
        <Title>Editar Usuario</Title>

        <FormReqiest>
          <WrapperInput>
            <i>
              <FaUserAlt />
            </i>
            <input
              type='text'
              name='names'
              placeholder='Nombre'
              onChange={handleChange}
              autocomplete='off'
              value={names}
            />
          </WrapperInput>
          <WrapperInput>
            <i>
              <FaUserAlt />
            </i>
            <input
              type='email'
              name='email'
              placeholder='Email'
              autocomplete='off'
              onChange={handleChange}
              value={email}
            />
          </WrapperInput>

          <WrapperInput>
            <i>
              <FaKey />
            </i>
            <input
              type='password'
              name='password'
              placeholder='Contraseña'
              autocomplete='off'
              onChange={handleChange}
            />
          </WrapperInput>
          {errUpdate && <AuthError error={errUpdate} />}
          <FormButton
            background
            width={'auto'}
            radius={5}
            onClick={handleSubmit}
          >
            EDITAR
          </FormButton>
        </FormReqiest>
      </Form>
    </FormWrapper>
  )
}

const FormWrapper = styled.div`
  margin: 50px auto;
  max-width: 400px;
  background-color: ${({ theme }) => theme.colors.body};
  box-shadow: -7px 5px 41px -2px rgba(0, 0, 0, 0.43);
  -webkit-box-shadow: -7px 5px 41px -2px rgba(0, 0, 0, 0.43);
  -moz-box-shadow: -7px 5px 41px -2px rgba(0, 0, 0, 0.43);
`

const Form = styled.div`
  color: ${({ theme }) => theme.colors.lighGray};
  padding: 1rem;
`

const FormReqiest = styled.div`
  padding: 1rem;
`

const FormButton = styled(Button)`
  color: white;
  margin: 20px auto;
  /* padding: 20px; */
  height: 40px;
  background-color: ${({ theme }) => theme.colors.primaryHover};
  :hover {
    background-color: ${({ theme }) => theme.colors.primary};
  }
`
const Title = styled.h1`
  color: ${({ theme }) => theme.colors.black};
`
const WrapperInput = styled.p`
  display: flex;
  position: relative;
  border-bottom: 1px solid black;
  height: 38px;
  /* border-radius: 5px; */
  padding: 0;
  width: 100%;
  margin: 20px 0;
  /* background-color: ${({ theme }) => theme.colors.mediumGray}; */
  i {
    width: 10%;
    text-align: center;
    line-height: 2rem;
  }
  :focus-within {
    /* background-color: white; */
  }
  input {
    width: 100%;
    background: transparent;
    border: none;
    padding-left: 10px;
    outline: none;
    color: ${({ theme }) => theme.colors.lighGray};
  }
`
