import { useButtonsPagination } from '../../hooks/useButtonsPagination'

export function Pagination ({ total, perPage, changePage, page }) {
  const numberOfPages =
    total % perPage === 0 ? total / perPage : parseInt(total / perPage) + 1

  const isFirst = page === 1
  const isLast = page === numberOfPages

  const [Buttons] = useButtonsPagination(
    numberOfPages,
    page,
    isFirst,
    isLast,
    changePage
  )
  // const buttons = Array.from(Array(numberOfPages).keys())

  return <Buttons />
}
