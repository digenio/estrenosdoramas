import React, { useContext } from 'react'
import { GoClock, GoEye, GoEyeClosed } from 'react-icons/go'
import { FaTimes } from 'react-icons/fa'

import { ListContext } from '../../contexts/ListContext'
import { DropItem } from '../../ui/Buttons/DropDownItem'

export const ListAction = ({
  _id,
  name,
  poster_path,
  __typename,
  slug,
  name_es,
  isTVShow,
  poster
}) => {
  const { list, toggleList } = useContext(ListContext)

  let inList = null
  let status = ''

  if (list && list.length > 0) {
    inList = list.find(item => item.serie_id === _id)
    status = inList?.status
  }
  const handleEdit = status => {
    toggleList(
      {
        name,
        slug,
        name_es,
        poster_path,
        serie_id: _id,
        poster,
        _id: inList?._id,
        type: __typename === 'Movie' ? 'movie' : isTVShow ? 'tvshow' : 'dorama'
      },
      status
    )
  }

  return (
    <>
      <DropItem
        buttonList
        liked={status === 'Ver despues'}
        icon={<GoClock />}
        eventClick={() => handleEdit('Ver despues')}
      >
        Ver despues
      </DropItem>
      <DropItem
        liked={status === 'Viendo'}
        buttonList
        icon={<GoEye />}
        eventClick={() => handleEdit('Viendo')}
      >
        Viendo
      </DropItem>
      <DropItem
        buttonList
        liked={status === 'Visto'}
        icon={<GoEyeClosed />}
        eventClick={() => handleEdit('Visto')}
      >
        Visto
      </DropItem>
      {inList && (
        <DropItem
          buttonList
          icon={<FaTimes color={'red'} />}
          eventClick={() => handleEdit('remove')}
        >
          Eliminar
        </DropItem>
      )}
    </>
  )
}
