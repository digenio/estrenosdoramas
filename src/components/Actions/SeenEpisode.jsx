import { useMutation } from '@apollo/client'
import styled, { css } from 'styled-components'
import { MdRemoveRedEye } from 'react-icons/md'
import React, { useContext, useEffect, useState } from 'react'

import config from '../../utils/config'
import { UserContext } from '../../contexts/UserContext'
import { ADD_SEEN, DELETE_SEEN } from '../../gql/seen'

export const SeenEpisode = ({ _id, listSeen = [], serie_id, reload }) => {
  const { loggedUser } = useContext(UserContext)

  const [seen, changeSeen] = useState(false)
  const [seenIndex, changeSeenIndex] = useState(false)

  useEffect(() => {
    if (listSeen.length !== 0) {
      const isSeen = listSeen.findIndex(item => item.seen_id === _id)
      changeSeen(isSeen !== -1)
      changeSeenIndex(isSeen)
    }
  }, [listSeen])

  const [addSeen] = useMutation(ADD_SEEN)
  const [deleteSeen] = useMutation(DELETE_SEEN)

  return (
    <>
      <TouchableView
        active={seen}
        onClick={() => {
          if (loggedUser) {
            changeSeen(!seen)
            if (!seen) {
              addSeen({
                variables: {
                  record: {
                    seen_id: _id,
                    group_id: serie_id,
                    user_id: loggedUser._id,
                    platform: config.app,
                    seen_type: 'episode'
                  }
                }
              })
                .catch(err => {
                  console.error(err)
                  alert('Ocurrio un error')
                  changeSeen(false)
                })
                .then(() => {
                  reload && reload()
                })
            } else {
              deleteSeen({ variables: { _id: listSeen[seenIndex]._id } })
                .catch(err => {
                  console.error(err)
                  alert('Ocurrio un error')
                  changeSeen(true)
                })
                .then(() => {
                  reload && reload()
                })
            }
          } else {
            // changeType('login')
            // changeVisibleModal(true)
          }
        }}
      >
        <MdRemoveRedEye size={20} />
      </TouchableView>
    </>
  )
}

const TouchableView = styled.a`
  padding: 2px 1px;
  margin-left: 10px;
  display: inline-block;
  z-index: 2;
  width: 40px;
  :hover {
    transform: scale(1.1);

    svg {
      color: ${({ theme }) => theme.colors.details};
    }
  }

  ${({ active }) =>
    active &&
    css`
      svg  {
        color: ${({ theme }) => theme.colors.details};
      }
    `}
`
