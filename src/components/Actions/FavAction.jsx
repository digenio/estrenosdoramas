import { FaHeart } from 'react-icons/fa'
import React, { useContext, useEffect, useState } from 'react'
import { FavsContext } from '../../contexts/FavsContext'
import { UserContext } from '../../contexts/UserContext'
import { DropItem } from '../../ui/Buttons/DropDownItem'

export const FavAction = ({
  _id,
  name,
  poster_path,
  __typename,
  slug,
  name_es,
  isTVShow,
  poster
}) => {
  const { loggedUser } = useContext(UserContext) || {}

  const [liked, changeLiked] = useState(false)
  const { favs, toggleFav } = useContext(FavsContext)

  useEffect(() => {
    const index = favs.findIndex(item => item?.serie_id === _id)
    changeLiked(index !== -1)
  }, [_id, favs])

  const handleFav = () => {
    if (loggedUser) {
      if (!liked) {
        toggleFav({
          serie_id: _id,
          name,
          name_es,
          poster_path,
          poster,
          slug,
          type:
            __typename === 'Movie' ? 'movie' : isTVShow ? 'tvshow' : 'dorama'
        })
        changeLiked(true)
      } else {
        toggleFav({
          serie_id: _id
        })
        changeLiked(false)
      }
    }
  }

  return (
    <>
      <DropItem
        buttonList
        href='#'
        icon={<FaHeart />}
        liked={liked}
        eventClick={handleFav}
      >
        Favoritos
      </DropItem>
    </>
  )
}
