import styled, { css } from 'styled-components'
import { useState, useContext } from 'react'
import Link from 'next/link'
import { GoChevronDown } from 'react-icons/go'
import { MdMovieFilter, MdDesktopWindows } from 'react-icons/md'
import {
  BiLockAlt,
  BiSearch,
  BiNews,
  BiPurchaseTagAlt,
  BiCustomize,
  BiMapPin,
  BiStoreAlt
} from 'react-icons/bi'

import { GenreContext } from '../contexts/GenreContext'
import { NetworkContext } from '../contexts/NetworkContext'
import { CountryContext } from '../contexts/CountryContext'
import { LabelContext } from '../contexts/LabelContext'
import { UserContext } from '../contexts/UserContext'
import { HamburguerButton } from '../ui/Buttons/HamburguerButton'
import { LogoIcon } from '../ui/Base/LogoIcon'
import { TextLink } from '../ui/Buttons/TextLink'
import { DropDown } from '../ui/Buttons/DropDown'
import { DropItem } from '../ui/Buttons/DropDownItem'
import { SearchInput } from '../ui/Buttons/SearchInput'
import { Button } from '../ui/Buttons/Button'
import { NavProfile } from '../components/User/NavProfile'
import { Apps } from '../ui/Base/Apps'

export const Header = () => {
  const [viewMenu, setViewMenu] = useState(false)
  const [viewInput, setViewInput] = useState(false)
  const { genres } = useContext(GenreContext)
  const { networks } = useContext(NetworkContext)
  const { countries: cts } = useContext(CountryContext)
  const { labels } = useContext(LabelContext)

  const handleMenuView = e => {
    setViewMenu(!viewMenu)
  }

  const handleSearchInputView = () => {
    setViewInput(!viewInput)
  }

  const { loggedUser, logout } = useContext(UserContext)

  return (
    <HeaderLayout>
      <HeaderContainer>
        <LogoIcon />
        <Navigation viewMenu={viewMenu}>
          <ListNavigation>
            <LogoIcon mobile />
            <TextLink href='/estrenos-doramas'>
              <IconMenu>
                <MdDesktopWindows size={25} />
              </IconMenu>
              Doramas
            </TextLink>

            <TextLink href='/peliculas'>
              <IconMenu>
                <MdMovieFilter size={25} />
              </IconMenu>
              Películas
            </TextLink>
            <TextLink href='/variedades'>
              <IconMenu>
                <BiNews size={25} />
              </IconMenu>
              Variedades
            </TextLink>

            <DropDown
              icon={<GoChevronDown />}
              iconTwo={<BiPurchaseTagAlt size={25} />}
              navegation
              label='Etiquetas'
            >
              {labels.slice(1, 8).map(label => (
                <DropItem
                  as={`/etiquetas/${label.slug}`}
                  href={`/etiquetas/[slug]`}
                  navegationList
                  key={label._id}
                  color
                >
                  {label.name}
                </DropItem>
              ))}
              <DropItem more href={`/etiquetas`} navegationList>
                ver mas
              </DropItem>
            </DropDown>
            <DropDown
              icon={<GoChevronDown />}
              iconTwo={<BiCustomize size={25} />}
              navegation
              label='Generos'
            >
              {genres.slice(1, 8).map(geners => (
                <DropItem
                  as={`/generos/${geners.slug}`}
                  href={`/generos/[slug]`}
                  navegationList
                  key={geners._id}
                  color
                >
                  {geners.name}
                </DropItem>
              ))}
              <DropItem more href={`/generos`} navegationList>
                ver mas
              </DropItem>
            </DropDown>

            <DropDown
              icon={<GoChevronDown />}
              iconTwo={<BiMapPin size={25} />}
              navegation
              label='Paises'
            >
              {cts.map(countrie => (
                <DropItem
                  as={`/paises/${countrie.slug}`}
                  href={`/paises/[slug]`}
                  navegationList
                  key={countrie._id}
                  color
                >
                  {countrie.name}
                </DropItem>
              ))}
            </DropDown>

            <DropDown
              icon={<GoChevronDown />}
              iconTwo={<BiStoreAlt size={25} />}
              navegation
              label='Productora'
            >
              {networks.slice(1, 8).map(producers => (
                <DropItem
                  as={`/productoras/${producers.slug}`}
                  href={`/productoras/[slug]`}
                  navegationList
                  key={producers._id}
                  color
                >
                  {producers.name}
                </DropItem>
              ))}
              <DropItem more href={`/productoras`} navegationList>
                ver mas
              </DropItem>
            </DropDown>
          </ListNavigation>
        </Navigation>
        <Apps />
      </HeaderContainer>

      <SearchInput
        viewInput={viewInput}
        onClick={handleSearchInputView}
        handleSearchInputView={handleSearchInputView}
      />

      <Button
        mobile
        width={70}
        height={35}
        radius={5}
        onClick={handleSearchInputView}
      >
        <BiSearch size={20} />
      </Button>
      {loggedUser ? (
        <NavProfile {...loggedUser} logout={logout} />
      ) : (
        <>
          <Link href={'/autenticacion'}>
            <a>
              <Button desktop detailOne width={110} height={40} radius={5} link>
                Iniciar
              </Button>
            </a>
          </Link>
        </>
      )}

      {loggedUser ? null : (
        <>
          <Link href={'/autenticacion'}>
            <Button
              mobile
              background
              width={70}
              height={35}
              radius={5}
              link
              href={'/autenticacion'}
            >
              <BiLockAlt />
            </Button>
          </Link>
        </>
      )}

      <HamburguerButton onClick={handleMenuView} viewMenu={viewMenu} />
    </HeaderLayout>
  )
}

const HeaderLayout = styled.header`
  z-index: 400;
  display: flex;
  position: fixed;
  height: 80px;
  /* max-width: 1440px; */
  width: 100%;
  margin: 0 auto;
  justify-content: space-between;
  padding: 0 2% 0 2%;
  align-items: center;
  background-color: ${({ theme }) => theme.colors.primary};

  :before {
    position: absolute;
    content: '';
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0, 0, 0, 0.6);
    z-index: -1;
  }
`

const HeaderContainer = styled.article`
  display: flex;
  align-items: center;
  width: 80%;
  height: inherit;
`

const Navigation = styled.nav`
  position: fixed;
  left: 0;
  padding-left: 30px;
  width: 15rem;
  left: -15rem;
  top: 0;
  height: 100vh;
  background-color: ${({ theme }) => theme.colors.primary};
  overflow-y: scroll;

  z-index: 10;
  transition: left 0.3s;
  font-size: 0.8rem;

  ${({ viewMenu }) =>
      viewMenu &&
      css`
        left: 0rem;
        box-shadow: 0 0 3rem #000;
      `}
    :before {
    position: absolute;
    content: '';
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;

    background-color: rgba(0, 0, 0, 0.6);
    z-index: -1;
  }

  @media ${({ theme }) => theme.devices.laptop} {
    overflow: initial;
    display: flex;
    position: initial;
    margin-left: 10px;
    width: 100%;
    justify-content: flex-start;
    background-color: transparent;
    height: auto;
    padding-left: 0;

    :before {
      content: none;
    }
  }
`

const ListNavigation = styled.ul`
  list-style: none;
  font-size: 20px;
  height: 0px;
  padding: 2rem 0;

  @media ${({ theme }) => theme.devices.laptop} {
    width: 100%;
    display: flex;
    text-align: center;
    font-size: .8rem;
    justify-content: space-between;
    align-items: center;
  }
  @media ${({ theme }) => theme.devices.laptopL} {
    width: 90%;
   
  }
  @media ${({ theme }) => theme.devices.desktop} {
    width: 80%;
   
  }
  /* query */

  margin: 10px 10px;
  li {
    width: auto;
    cursor: pointer;
    /* color: ${({ theme }) => theme.colors.black}; */

    font-weight: 400;
    transition: color 0.3s ease;
    :hover {
      color: white;
    }
  }
`

// const LineSeparation = styled.hr`
//   height: 2px;
//   margin: 0;
//   width: 100%;
//   background-color: white;
//   display: none;
// `

const IconMenu = styled.span`
  margin-right: 5px;
  @media ${({ theme }) => theme.devices.laptop} {
    display: none;
  }
`
