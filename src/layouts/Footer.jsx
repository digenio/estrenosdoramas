import styled from 'styled-components'
import { TextLink } from '../ui/Buttons/TextLink'
import { LogoIcon } from '../ui/Base/LogoIcon'

export const Footer = () => {
  return (
    <FooterBar>
      <LogoIcon grayscale='true' />
      <NavFooter>
        <FooterLink href='/dcma'>DCMA</FooterLink>
        <FooterLink href='/politicas'>Politica de Cookies</FooterLink>
        <FooterLink href='/faq'>Preguntas frecuentes</FooterLink>
        <FooterLink href='/terminos'>Terminos y condiciones</FooterLink>
      </NavFooter>
    </FooterBar>
  )
}

const FooterBar = styled.footer`
  width: 100%;
  position: relative;
  min-height: 80px;
  display: flex;
  flex-direction: column;
  align-items: center;
  align-self: flex-end;
  padding: 0 2% 0 2%;
  z-index: 5;
  background-color: ${({ theme }) => theme.colors.primary};
  @media ${({ theme }) => theme.devices.laptop} {
    flex-direction: row;
    justify-content: space-between;
  }

  :before {
    position: absolute;
    content: '';
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0, 0, 0, 0.6);
    z-index: -1;
  }
`

const NavFooter = styled.nav`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  gap: 15px;
  padding: 10px;
`

const FooterLink = styled(TextLink)`
  color: ${({ theme }) => theme.colors.white};
  text-decoration: none;
  list-style: none;
  text-align: center;
  font-size: 16px;
  transition: color 0.3s ease;
  cursor: pointer;

  &:hover {
    color: ${({ theme }) => theme.colors.white};
  }
`
