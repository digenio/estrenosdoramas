import styled from 'styled-components'
import { Button } from '../ui/Buttons/Button'
import { FadeIn } from '../styles/animations'
import Link from 'next/link'
import { useRouter } from 'next/router'
export const CategoriesList = ({ children, categorie, slug }) => {
  const { pathname } = useRouter()

  return (
    <Categorie>
      {categorie && (
        <HeaderCaregorie>
          <Title>{categorie}</Title>
          {pathname == '/' && (
            <>
              <Link href={`/${categorie}`}>
                <a>
                  <ButtonCategorie height={30}>Ver mas</ButtonCategorie>
                </a>
              </Link>
            </>
          )}
        </HeaderCaregorie>
      )}

      <CategoriContainer>{children}</CategoriContainer>
    </Categorie>
  )
}
const HeaderCaregorie = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;

  margin-bottom: 20px;
`
const Title = styled.h3`
  color: ${({ theme }) => theme.colors.black};
  margin-right: 1rem;
  font-size: 30px;
  font-weight: 700;
  text-transform: capitalize;
`

const CategoriContainer = styled.section`
  margin-bottom: 30px;
`

const Categorie = styled.div`
  display: flex;
  flex-direction: column;
  animation: ${FadeIn} 0.4s;
  transition: all 1s;
  animation-fill-mode: both;

  h3 {
    text-transform: capitalize;
  }
`
const ButtonCategorie = styled(Button)`
  font-size: 11px;
  text-transform: uppercase;
  background-color: ${({ theme }) => theme.colors.gray};
  color: white;
  font-weight: 700;
  border-radius: 5px;
  box-shadow: inset 0 -3px 0 rgb(0 0 0 / 20%);

  :hover {
    background-color: ${({ theme }) => theme.colors.primary};
  }
`
