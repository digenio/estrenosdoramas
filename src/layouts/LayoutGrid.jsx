import styled, { css } from 'styled-components'

export const LayoutGrid = ({ children, slug, slugIverse }) => {
  return (
    <DetailWrapper slug={slug} slugIverse={slugIverse}>
      {children}
    </DetailWrapper>
  )
}

const DetailWrapper = styled.section`
  display: grid;
  grid-template-columns: repeat(1, minmax(0, 1fr));
  grid-gap: 1.5rem;

  @media ${({ theme }) => theme.devices.laptop} {
    grid-template-columns: 350px auto;
    grid-template-rows: auto;
    ${({ slug }) =>
      slug &&
      css`
        grid-template-rows: 500px 1fr;
      `}

    ${({ slugIverse }) =>
      slugIverse &&
      css`
        grid-template-columns: auto 350px;
        grid-template-rows: 500px 1fr;
      `}
    grid-gap: 5px;
  }
`
