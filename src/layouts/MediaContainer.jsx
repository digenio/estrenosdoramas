import React, { useRef } from 'react'
import styled, { css } from 'styled-components'
import { FadeIn } from '../styles/animations'

const MediaContainer = React.forwardRef((props, ref) => {
  return (
    <>
      {props.kingOfDrop ? (
        <>
          <TitleMedia>{props.kingOfDrop}</TitleMedia> <Button />
        </>
      ) : null}
      <ContentMediaWrapper className={props.className} ref={ref} id='wrapper'>
        {props.children}
      </ContentMediaWrapper>
    </>
  )
})

export default MediaContainer

const ContentMediaWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(2, minmax(0, 1fr));
  gap: 10px;
 

  animation: ${FadeIn} 0.4s;
  transition: all 1s;
  animation-fill-mode: both;
  @media ${({ theme }) => theme.devices.mobileM} {
    grid-template-columns: repeat(3, minmax(0, 1fr));
    grid-gap: 1rem;
  }
  @media ${({ theme }) => theme.devices.mobileL} {
    grid-template-columns: repeat(4, minmax(0, 1fr));
    grid-gap: 1rem;
  }


  @media ${({ theme }) => theme.devices.tabletS} {
    grid-template-columns: repeat(5, minmax(0, 1fr));
    grid-gap: 1rem;

  }
  @media ${({ theme }) => theme.devices.tablet} {
    grid-template-columns: repeat(5, minmax(0, 1fr));
    grid-gap: 1rem;

  }

  @media ${({ theme }) => theme.devices.laptop} {
    grid-template-columns: repeat(5, minmax(0, 1fr));
    ${({ casts }) =>
    casts &&
    css`
        grid-row: auto / span 1;
        grid-column: 1/-1;
      `}
  }
  /* @media ${({ theme }) => theme.devices.laptopL} {
    grid-template-columns: repeat(6, minmax(0, 1fr));
    grid-gap: 1.5rem;
  } */
`

const TitleMedia = styled.h3`
  color: ${({ theme }) => theme.colors.white};
`
