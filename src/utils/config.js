const config = {
  name: "EstrenosDoramas",
  platform: "estrenosdoramas",
  app: "estrenosdoramas",
  url: "https://estrenosdoramas.io",
  arc: "VH1LTN6V",
  color: "#2E96F7",
  analitics: "G-YK3JMW8HHX",
  appAndroid: "com.gratis.estrenosdoramas",
};

export default config;
