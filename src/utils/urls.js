import config from './config'

export const URL_IMAGE = 'https://image.tmdb.org/t/p/w300_and_h450_bestv2'
export const URL_IMAGE_2 = 'https://image.tmdb.org/t/p/w440_and_h660_face'

/* IMAGE POSTER */
export const URL_IMAGE_POSTER = 'https://image.tmdb.org/t/p/w220_and_h330_face'
export const URL_IMAGE_POSTER_X =
  'https://image.tmdb.org/t/p/w94_and_h141_bestv2'
export const URL_IMAGE_POSTER_1X =
  'https://image.tmdb.org/t/p/w116_and_h174_face'
export const URL_IMAGE_POSTER_2X =
  'https://image.tmdb.org/t/p/w220_and_h330_face'
export const URL_IMAGE_POSTER_3X =
  'https://image.tmdb.org/t/p/w440_and_h660_face'

/* IMAGE BACKDROPS */
export const URL_IMAGE_BACK_DROPS_1X =
  'https://image.tmdb.org/t/p/w1000_and_h450_multi_faces'
export const URL_IMAGE_BACK_DROPS_2X =
  'https://image.tmdb.org/t/p/w1920_and_h800_multi_faces'

/* IMAGE EPISODE */
export const URL_IMAGE_EPISODE_1X =
  'https://image.tmdb.org/t/p/w227_and_h127_bestv2/'

export const URL_IMAGE_EPISODE_2X =
  'https://image.tmdb.org/t/p/w454_and_h254_bestv2/'

/* OTHERS */
export const URL_IMAGE_ORIGINAL = 'https://image.tmdb.org/t/p/original'
export const URL_IMAGE_NETWORK = 'https://image.tmdb.org/t/p/h60'

export const URL_PEOPLE = 'https://image.tmdb.org/t/p/w138_and_h175_face'
export const URL_PERSON = 'https://image.tmdb.org/t/p/w300_and_h450_bestv2'
export const URL_LOGO_NETWORK = 'https://image.tmdb.org/t/p/h30'

export const URL_API_PROD = 'https://fluxcedene.net/api'

export const URL_API_DEV = 'http://localhost:8000/api'
export const URL = config.url

export const URL_API = URL_API_PROD

export const RESOLVER_URL = 'http://64.227.10.130:8080/api/v1'

export const AVATAR_1 = 'https://actiserver.com/wp-content/uploads/avatar-7.png'
export const AVATAR_2 =
  'https://lh3.googleusercontent.com/proxy/3OnIbW261tnOk2NbaLG_7N2YZccOgVD7zmHFNo38OUSSBzAeNesY5EXi_Iv1F5YANVtMtPXEGArFzz3cj6wDkyNzMbq2fxdL3w2fsp0'
