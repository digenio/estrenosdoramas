import fs from 'fs'
import globby from 'fast-glob'
import * as builder from 'xmlbuilder'

import { LIST_LABELS } from '../gql/label'
import { LIST_GENRE_SLUG } from '../gql/genre'
import { LIST_MOVIES_SLUG } from '../gql/movie'
import { LIST_SEASON_SLUG } from '../gql/season'
import { LIST_DORAMAS_SLUG } from '../gql/dorama'
import { LIST_EPISODES_SLUG } from '../gql/episode'
import { LIST_NETWORKS_SLUG } from '../gql/network'

import config from './config'
import { initializeApollo } from '../lib/apolloClient'
import { formatDate, firstDayMonth } from './functions'

const rootUrl = config.url

const buildUrlObject = (path, updatedAt, freq, priority) => {
  return {
    loc: { '#text': `${rootUrl}${path}` },
    lastmod: updatedAt ? { '#text': formatDate(updatedAt) } : undefined,
    changefreq: freq ? { '#text': freq } : undefined,
    priority: priority ? { '#text': priority } : undefined
  }
}

export const generateSiteMap = async () => {
  try {
    const client = initializeApollo()

    const pages = await globby([
      'src/pages/**/*.jsx',
      '!src/pages/_*.jsx',
      '!src/pages/**/[slug].jsx',
      '!src/pages/**/[letter].jsx',
      '!src/pages/**/[year].jsx',
      '!src/pages/api',
      'posts/*.md'
    ])

    const { data: { listEpisodes = [] } = {} } = await client.query({
      query: LIST_EPISODES_SLUG
    })
    const { data: { listDoramas = [] } = {} } = await client.query({
      query: LIST_DORAMAS_SLUG
    })
    const { data: { listGenres = [] } = {} } = await client.query({
      query: LIST_GENRE_SLUG
    })
    const { data: { listMovies = [] } = {} } = await client.query({
      query: LIST_MOVIES_SLUG
    })
    const { data: { listNetworks = [] } = {} } = await client.query({
      query: LIST_NETWORKS_SLUG
    })
    const { data: { listSeasons = [] } = {} } = await client.query({
      query: LIST_SEASON_SLUG
    })
    const { data: { listLabels = [] } = {} } = await client.query({
      query: LIST_LABELS
    })

    generateSubSiteMap(listGenres, 'generos', 'weekly', '0.6')
    generateSubSiteMap(listNetworks, 'productoras', 'weekly', '0.6')
    generateSubSiteMap(listLabels, 'etiquetas', 'weekly', '0.6')
    generateSubSiteMap(listMovies, 'peliculas', 'weekly', '1.0')

    generateSubSiteMap(
      listDoramas.filter(it => !it.isTVShow),
      'doramas',
      'weekly',
      '1.0'
    )
    generateSubSiteMap(
      listDoramas.filter(it => it.isTVShow),
      'variedades',
      'weekly',
      '1.0'
    )

    generateSubSiteMap(listSeasons, 'temporadas', 'weekly', '0.8')
    generateSubSiteMapCapitulos(listEpisodes, 'capitulos', 'weekly', '1.0')
    generateSubSiteMapPages(pages)

    generateSiteMapIndex([
      'capitulos',
      'doramas',
      'peliculas',
      'etiquetas',
      'temporadas',
      'generos',
      'productoras',
      'pages',
      'variedades'
    ])

    return
  } catch (error) {
    return { error: 404 }
  }
}

const generateSiteMapIndex = array => {
  try {
    let feedObject = {
      sitemapindex: {
        '@xmlns': 'http://www.sitemaps.org/schemas/sitemap/0.9',
        sitemap: []
      }
    }

    for (let index = array.length - 1; index >= 0; index--) {
      const item = array[index]
      if (item) {
        feedObject.sitemapindex.sitemap.push(
          buildUrlObject(`/sitemap-${item}.xml`)
        )
      }
    }

    const sitemap = builder.create(feedObject, { encoding: 'utf-8' })
    const xml = sitemap.end({ pretty: true })

    fs.writeFileSync(`public/sitemap.xml`, xml, function (err, data) {
      if (err) {
        console.error(err)
      }
    })
  } catch (error) {}

  return
}

const generateSubSiteMap = (array, root, freq, priority, extra = '') => {
  try {
    let feedObject = {
      urlset: {
        '@xmlns': 'http://www.sitemaps.org/schemas/sitemap/0.9',
        '@xmlns:image': 'http://www.google.com/schemas/sitemap-image/1.1',
        url: []
      }
    }

    for (let index = array.length - 1; index >= 0; index--) {
      const item = array[index]
      if (item.slug) {
        feedObject.urlset.url.push(
          buildUrlObject(
            `/${root}/${item.slug}`,
            item.updatedAt || firstDayMonth(),
            freq,
            priority
          )
        )
      }
    }

    const sitemap = builder.create(feedObject, { encoding: 'utf-8' })
    const xml = sitemap.end({ pretty: true })

    fs.writeFileSync(`public/sitemap-${root}${extra}.xml`, xml, function (
      err,
      data
    ) {
      if (err) {
        console.error(err)
      }
    })
  } catch (error) {}

  return
}

const generateSubSiteMapCapitulos = (array, root, freq, priority) => {
  try {
    let feedObject = {
      sitemapindex: {
        '@xmlns': 'http://www.sitemaps.org/schemas/sitemap/0.9',
        '@xmlns:image': 'http://www.google.com/schemas/sitemap-image/1.1',
        sitemap: []
      }
    }

    const months = []
    array.forEach(item => {
      const date = formatDate(item.updatedAt)
      const month = date
        .split('-')
        .slice(0, 2)
        .join('-')
      if (months.findIndex(m => m === month) === -1) {
        months.push(month)
      }
    })

    months.reverse().forEach(month => {
      feedObject.sitemapindex.sitemap.push({
        loc: { '#text': `${rootUrl}/sitemap-${root}${month}.xml` }
      })

      const caps = array.filter(item => {
        const date = formatDate(item.updatedAt)
        const monthCap = date
          .split('-')
          .slice(0, 2)
          .join('-')
        return monthCap === month
      })

      generateSubSiteMap(caps, root, freq, priority, month)
    })

    const sitemap = builder.create(feedObject, { encoding: 'utf-8' })
    const xml = sitemap.end({ pretty: true })

    fs.writeFileSync(`public/sitemap-${root}.xml`, xml, function (err, data) {
      if (err) {
        console.error(err)
      }
    })
  } catch (error) {}

  return
}

const generateSubSiteMapPages = pages => {
  let feedObject = {
    urlset: {
      '@xmlns': 'http://www.sitemaps.org/schemas/sitemap/0.9',
      '@xmlns:image': 'http://www.google.com/schemas/sitemap-image/1.1',
      url: []
    }
  }

  for (const item of pages) {
    const path = item
      .replace('src/pages', '')
      .replace('.jsx', '')
      .replace('.md', '')
    const route = path.replace('/index', '')
    if (route.includes('[user]')) {
      continue
    }
    feedObject.urlset.url.push(
      buildUrlObject(
        `${route}`,
        firstDayMonth(),
        route ? 'weekly' : 'daily',
        route ? '0.6' : '1.0'
      )
    )
  }

  const sitemap = builder.create(feedObject, { encoding: 'utf-8' })
  const xml = sitemap.end({ pretty: true })

  fs.writeFileSync(`public/sitemap-pages.xml`, xml, function (err, data) {
    if (err) {
      console.error(err)
    }
  })

  return
}
