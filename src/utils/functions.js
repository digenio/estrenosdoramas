import MobileDetect from 'mobile-detect'
import { monthNames } from './constans'
import { URL_IMAGE_EPISODE_1X } from './urls'

export function makeYears () {
  const today = new Date()
  const year = today.getFullYear()
  const years = []
  for (let i = 0; i < 30; i++) {
    years.push((year - i).toString())
  }
  return years
}

export function shuffle (array1, array2) {
  array1 = array1
  array2 = array2

  const array3 = array1.concat(array2)
  const array4 = Array.from(Array(array3.length).keys())
  const array5 = []

  for (let i = array4.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * i)
    const temp = array4[i]
    array4[i] = array4[j]
    array4[j] = temp
  }

  array4.forEach(i => {
    array5.push(copy(array3[i]))
  })

  return array5
}

export function makeid (length) {
  var result = ''
  var characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  var charactersLength = characters.length
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }
  return result
}

export function mixArray (array1, array2, limit) {
  const array = Array.from(Array(limit).keys())

  let idx = 0
  let idx2 = 0
  const list = []
  array.forEach(item => {
    if (item % 2 === 0) {
      if (array1[idx2]) {
        list.push(array1[idx2])
        idx2++
      } else {
        list.push(array2[idx])
        idx++
      }
    } else {
      if (array2[idx]) {
        list.push(array2[idx])
        idx++
      } else {
        list.push(array1[idx2])
        idx2++
      }
    }
  })

  const newList = list.filter(item => item)

  return newList
}

export function copy (obj) {
  return JSON.parse(JSON.stringify(obj))
}

export function getDeviceType (req) {
  let userAgent
  let deviceType
  if (req) {
    userAgent = req.headers['user-agent']
  } else {
    userAgent = navigator.userAgent
  }
  const md = new MobileDetect(userAgent)
  if (md.tablet()) {
    deviceType = 'tablet'
  } else if (md.mobile()) {
    deviceType = 'mobile'
  } else {
    deviceType = 'desktop'
  }
  return { deviceType }
}

export const createArray = n => Array.from(Array(n).keys())

export const filterCast = array => array.filter(item => item.slug)

export const firstDayMonth = () => {
  let d = new Date()
  let month = d.getMonth() + 1
  if (month / 9 <= 1) month = '0' + month
  let year = d.getFullYear()
  let dateStr = year + '-' + month + '-' + '01'
  return dateStr
}

export function formatDate (date) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear()

  if (month.length < 2) month = '0' + month
  if (day.length < 2) day = '0' + day

  return [year, month, day].join('-')
}

export function shuffleArray (array) {
  return array.sort(() => Math.random() - 0.5)
}

export function getDateString (date) {
  var dayOfWeek = [
    'Lunes',
    'Martes',
    'Miércoles',
    'Jueves',
    'Viernes',
    'Sábado',
    'Domingo'
  ]

  const monthName = [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre'
  ]

  return (
    date?.getDate() +
    ' de ' +
    monthName[date?.getMonth()] +
    ' del ' +
    date?.getFullYear()
  )
}

export function getRandomFloat (min, max) {
  return Math.random() * (max - min) + min
}

export function getRandomBool () {
  return Math.random() < 0.5
}

export function minutesToHours (totalMinutes) {
  var hours = Math.floor(totalMinutes / 60)
  var minutes = totalMinutes % 60

  if (hours > 0) {
    return `${hours} h ${minutes} min`
  } else {
    return `${minutes} min`
  }
}

export const toK = number => {
  return number / 1000 < 1
    ? number
    : number % 1000 < 100
    ? parseInt(number / 1000) + 'k'
    : (number / 1000).toFixed(1) + 'k'
}

export function getWeekDates (current) {
  var week = new Array()
  // Starting Monday not Sunday
  current.setDate(current.getDate() - 3)
  for (var i = 0; i < 7; i++) {
    week.push(new Date(current))
    current.setDate(current.getDate() + 1)
  }
  return week
}

export const getHrefByCategory = __typename => {
  if (__typename === 'Dorama' || __typename === 'PeopleDoramas') {
    return `/doramas/[slug]`
  } else if (__typename === 'tvshow') {
    return `/doramas/[slug]`
  } else {
    return `/peliculas/[slug]`
  }
}

export const getSlugByCategory = (__typename, slug) => {
  if (__typename === 'Dorama' || __typename === 'PeopleDoramas') {
    return `/doramas/${slug}`
  } else if (__typename === 'tvshow') {
    return `/doramas/${slug}`
  } else {
    return `/peliculas/${slug}`
  }
}
export const getDateByMonthName = date => {
  const dateNew = new Date(date)
  const year = dateNew.getFullYear()
  const month = monthNames[dateNew.getMonth()]
  const day = dateNew.getDate()

  return `${day} de ${month} del ${year}`
}

export const getImageUrlEpisode = (
  still_path = 'asas',
  backdrop = 'asas',
  chapter,
  imageDescription
) => {
  let still = `${URL_IMAGE_EPISODE_1X}${still_path}`
  let backdropImage = `${URL_IMAGE_EPISODE_1X}${backdrop}`

  let expresionURL = /(?:http|https):\/\/(?:[^\/\r\n]+)(\/[^\r\n]*)?/
  let cadenaStill = still_path
  let cadenaBackdrop = backdrop

  if (backdrop) {
    if (!!cadenaBackdrop.match(expresionURL)) {
      return cadenaBackdrop
    } else {
      return backdropImage
    }
  }
  if (still_path) {
    if (!!cadenaStill.match(expresionURL)) {
      return cadenaStill
    } else {
      return still
    }
  }

  return false
}

export const handleScrollTop = position => {
  if (position > 0) {
    // window.requestAnimationFrame(scrollUp)
    document.documentElement.scrollTo(0, 0)
  }
}

export const makeFistLetterUppercase = str =>
  str.charAt(0).toUpperCase() + str.slice(1)


  export const shortOverviewDots=(string)=>{

    let overviewCut = string?.slice(0, 100);
    
    return `${overviewCut}..`
    }
    
    