export const letters = 'abcdefghijklmnopqrstuvwxyz'.split('')

export const collection = ['Viendo', 'Por ver', 'Vistos', 'Por continuar']

export const monthNames = [
  'Enero',
  'Febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Septiembre',
  'Octubre',
  'Novienbre',
  'Diciembre'
]

export const serversAvalibles = [
  '1113',
  '1239',
  '1237',
  '1245',
  '2671',
  '1238',
  '1240',
  '1241',
  '14463',
  '1233',
  '7286',
  '1235',
  '3889',
  '1234',
  '8309',
  '34',
  '1230',
  '395',
  '8888'
]

export const days = [
  'Domingo',
  'Lunes',
  'Martes',
  'Miércoles',
  'Jueves',
  'Viernes',
  'Sábado'
]
